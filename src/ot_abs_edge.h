/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_EDGE_H_
#define OT_ABS_EDGE_H_

#include "ot_timer.h"
#include "ot_abs_typedef.h"
#include "ot_abs_node.h"

namespace OpenTimer {

// Class: AbsEdge
class AbsEdge{

  public:

    AbsEdge();                                                        // Constructor.
    AbsEdge(const AbsEdge&) = delete;                                 // Disable copy constructor.
    ~AbsEdge();                                                       // Destructor.

    inline int_t idx() const;                                         // Accessor.
    inline unsigned_t num_nonunate() const;  
 
    inline abs_node_pt from_abs_node_ptr() const;                     // Accessor.
    inline abs_node_pt to_abs_node_ptr() const;                       // Accessor.

    inline edge_pt slew_e_ptr() const; 
    inline abs_edge_pt first_abs_e_ptr() const;    

    inline abs_edgelist_iter_t fanin_satellite() const;               // Accessor.
    inline abs_edgelist_iter_t fanout_satellite() const;              // Accessor.
    inline abs_edgeset_pt abs_edgeset_ptr();                          // Accessor.
    inline abs_edge_type_e type() const;                              // Query the edge type.
    inline timing_sense_e timing_sense() const;                       // Query the timing sense.

    inline timing_pt merged_pos(int);                                 // Query the merged timing uptr.
    inline timing_pt merged_neg(int);                                 // Query the merged timing uptr.
    timing_upt merged_non_timing(int);                                 // Query the merged timing uptr.

    inline void_t set_idx(int_ct);                                    // Mutator.
    inline void_t set_num_nonunate(unsigned_ct);
    inline void_t set_from_abs_node_ptr(abs_node_pt);                 // Mutator.
    inline void_t set_to_abs_node_ptr(abs_node_pt);                   // Mutator.
    inline void_t set_slew_e_ptr(edge_pt);
    inline void_t set_first_abs_e_ptr(abs_edge_pt);
    inline void_t set_abs_edgeset_ptr(abs_edgeset_pt);                // Mutator.
    inline void_t set_fanin_satellite(abs_edgelist_iter_t);           // Mutator.
    inline void_t set_fanout_satellite(abs_edgelist_iter_t);          // Mutator.
    inline void_t set_type(abs_edge_type_ce);                         // Set the abs edge type.
//    inline void_t set_edge_ptr(edge_pt);                              // Set the edge ptr.
    inline void_t set_timing_sense(timing_sense_ce);                  // Set the timing sense.

    void_t merge_timing();                                            // Merge the timing.
    void_t merge_timing(int);                                         // Merge the timing.

    timing_pt timing_ptr(int, int, int);                              // Accessor.
    timing_pt insert_timing(int, int, int);                           // Insert a timing.
    timing_pt insert_merged_pos(int);
    timing_pt insert_merged_neg(int);

    node_pt from_node_ptr() const;                                    // Query the from node.
    node_pt to_node_ptr() const;                                      // Query the to node.
//    edge_pt edge_ptr() const;                                         // Query the edge ptr.

    pin_pt from_pin_ptr() const;                                      // Query the from pin.
    pin_pt to_pin_ptr() const;                                        // Query the to pin.

    inline static void_pt operator new(size_t);                       // Operator new.
    inline static void_t operator delete(void_pt);                    // Operator delete.
    

    AbsEdge& operator = (const AbsEdge&) = delete;                    // Disable copy assignment.
    static void_pt operator new [] (size_t) = delete;                 // Disable array new.
    static void_t operator delete [] (void_pt) = delete;              // Disable array delete.

  private:

    int_t _idx;                                                       // Index.

    unsigned_t _num_nonunate;
    abs_edgeset_pt _abs_edgeset_ptr;                                  // Satellitei.

    abs_edge_type_e _type;                                            // Abs Edge Type
    timing_sense_e _timing_sense;                                     // Edge sense.

    timing_upt _merged_pos[2];                                        // Positively merged timing uptr.
    timing_upt _merged_neg[2];                                        // Nagatively merged timing uptr.

    abs_node_pt _from_abs_node_ptr;                                   // From abs_node ptr.
    abs_node_pt _to_abs_node_ptr;                                     // To abs_node ptr.
    edge_pt _slew_e_ptr;
    abs_edge_pt _first_abs_e_ptr;
  
    //edge_pt _edge_ptr;                                                // Edge ptr.
   
    abs_edgelist_iter_t _fanin_satellite;                             // Fanin satellite.
    abs_edgelist_iter_t _fanout_satellite;                            // Fanout satellite.   

    static abs_edge_allocator_t _allocator;                           // Allocator.
    
    timing_upt _merge_pos_neg_timing(timing_pt, timing_pt);           // Merge the timing.
    timing_upt _merge_timing(timing_pt, timing_pt);                   // Merge the timing.

    bool_t _is_lut_identical(timing_lut_rt, timing_lut_rt);           // Compare two luts.
};

// Operator: new
inline void_pt AbsEdge::operator new(size_t size) {
  return _allocator.allocate();
}

// Operator: delete
inline void_t AbsEdge::operator delete(void_pt ptr) {
  _allocator.deallocate((abs_edge_pt)ptr);
}

// Function: idx
inline int_t AbsEdge::idx() const {
  return _idx;
}

// Function: num_nonunate
inline unsigned_t AbsEdge::num_nonunate() const {
  return _num_nonunate;
}


// Function: timing_sense
inline timing_sense_e AbsEdge::timing_sense() const {
  return _timing_sense;
}

// Procedure: set_idx
inline void_t AbsEdge::set_num_nonunate(unsigned_ct num_nonunate) {
  _num_nonunate=num_nonunate;
}
// Procedure: set_idx
inline void_t AbsEdge::set_idx(int_ct idx) {
  _idx = idx;
}

// Procedure: set_timing_sense
inline void_t AbsEdge::set_timing_sense(timing_sense_ce sense) {
  _timing_sense = sense;
}

// Procedure: set_abs_edgset_ptr
inline void_t AbsEdge::set_abs_edgeset_ptr(abs_edgeset_pt ptr) {
  _abs_edgeset_ptr = ptr;
}

// Function: edgeset_ptr
inline abs_edgeset_pt AbsEdge::abs_edgeset_ptr() {
  return _abs_edgeset_ptr;
}

// Function: timing_ptr
//inline timing_pt AbsEdge::timing_ptr(int el, int from_rf, int to_rf) {
//  return _timing_uptr[el][from_rf][to_rf].get();
//}

// Function: merged_pos
inline timing_pt AbsEdge::merged_pos(int el) {
  return _merged_pos[el].get();
}

// Function: merged_neg
inline timing_pt AbsEdge::merged_neg(int el) {
  return _merged_neg[el].get();
}

// Function: merged_non
//inline timing_pt AbsEdge::merged_non(int el) {
//  return _merged_non[el].get();
//}

// Procedure: set_from_abs_node_ptr    
inline void_t AbsEdge::set_from_abs_node_ptr(abs_node_pt ptr) {
  _from_abs_node_ptr = ptr;
} 

// Procedure: set_to_abs_node_ptr
inline void_t AbsEdge::set_to_abs_node_ptr(abs_node_pt ptr) {
  _to_abs_node_ptr = ptr;
}

// Procedure: set_slew_e_ptr
inline void_t AbsEdge::set_slew_e_ptr(edge_pt ptr) {
  _slew_e_ptr = ptr;
}

// Procedure: set_slew_e_ptr
inline void_t AbsEdge::set_first_abs_e_ptr(abs_edge_pt ptr) {
  _first_abs_e_ptr = ptr;
}

// Function: from_abs_node_ptr
inline abs_node_pt AbsEdge::from_abs_node_ptr() const {
  return _from_abs_node_ptr;
}

// Function: to_abs_node_ptr
inline abs_node_pt AbsEdge::to_abs_node_ptr() const {
  return _to_abs_node_ptr;
}
// Function: slew_e_ptr
inline edge_pt AbsEdge::slew_e_ptr() const {
  return _slew_e_ptr;
}
// Function: slew_e_ptr
inline abs_edge_pt AbsEdge::first_abs_e_ptr() const {
  return _first_abs_e_ptr;
}

// Procedure: set_fanin_satellite
inline void_t AbsEdge::set_fanin_satellite(abs_edgelist_iter_t ptr) {
  _fanin_satellite = ptr;
}

// Procedure: set_fanout_satellite
inline void_t AbsEdge::set_fanout_satellite(abs_edgelist_iter_t ptr) {
  _fanout_satellite = ptr;
}

// Function: fanin_satellite
inline abs_edgelist_iter_t AbsEdge::fanin_satellite() const {
  return _fanin_satellite;
}

// Function: fanout_satellite
inline abs_edgelist_iter_t AbsEdge::fanout_satellite() const {
  return _fanout_satellite;
}

// Function: type
inline abs_edge_type_e AbsEdge::type() const{
  return _type;
}

// Procedure: set_type
inline void_t AbsEdge::set_type(abs_edge_type_ce type) {
  _type = type;
}
/*
// Function: edge_ptr
inline edge_pt AbsEdge::edge_ptr() const {
  return _edge_ptr;
}

// Procedure: set_edge_ptr
inline void_t AbsEdge::set_edge_ptr(edge_pt ptr) {
  _edge_ptr = ptr;
}
*/

};  // End of namespace OpenTimer. ----------------------------------------------------------------

#endif


