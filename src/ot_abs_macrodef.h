/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_MACRODEF_H_
#define OT_ABS_MACRODEF_H_

#include "ot_macrodef.h"

#define TAU2016_FLT_MAX 987654.0f
#define TAU2016_FLT_MIN -987654.0f

#define OT_ABS_NODE_DEFAULT_MAX_SLEW TAU2016_FLT_MAX
#define OT_ABS_NODE_DEFAULT_MIN_SLEW TAU2016_FLT_MIN
#define OT_ABS_NODE_DEFAULT_MAX_CAPACITANCE TAU2016_FLT_MAX
#define OT_ABS_NODE_DEFAULT_MIN_CAPACITANCE TAU2016_FLT_MIN

#define OT_ABS_LUT_TEMPLATE_SLEW_DIMENTION_SIZE 8
#define OT_ABS_LUT_TEMPLATE_LOAD_DIMENTION_SIZE 8

#define OT_ABS_MIN_INPUT_SLEW 1.0f
#define OT_ABS_MAX_INPUT_SLEW 250.0f
#define OT_ABS_MIN_OUTPUT_LOAD 1.0f
#define OT_ABS_MAX_OUTPUT_LOAD 250.0f
#define OT_ABS_TABLE_SIZE 8

#define OT_ABS_SCALAR_TH 1.0f

#endif
