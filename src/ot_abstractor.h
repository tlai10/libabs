/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/


#ifndef OT_ABSTRACTOR_H_
#define OT_ABSTRACTOR_H_

#include "ot_timer.h"
#include "ot_abs_typedef.h"
#include "ot_abs_node.h"
#include "ot_abs_edge.h"
#include "ot_abs_iterator.h"
#include "ot_abs_macrodef.h"
#include "ot_abs_enumdef.h"

namespace OpenTimer {

// Class: Abstractor
class Abstractor {

  public:

    Abstractor();                                                                 // Constructor
    Abstractor(const Abstractor&) = delete;                                       // Disable copy constructor.
    ~Abstractor();                                                                // Destructor

    inline string_crt macro_name() const;                                         // Query the macro name.

    inline void_t set_timer_ptr(timer_pt);                                        // Set the timer ptr.
    inline void_t set_macro_name(string_crt);                                     // Set the macro name.

    inline timer_pt timer_ptr() const;                                            // Query the timer ptr.
    
    inline abs_node_dict_pt abs_node_dict_ptr() const;                            // Query the node dict ptr type.
    inline abs_node_dict_rt abs_node_dict() const;                                // Query the node dict ref type.
    inline abs_edgeset_crt abs_edgeset() const;                                   // Query the edgeset ref.
    inline abs_edgeset_pt abs_edgeset_ptr();                                      // Query the edgeset ptr.
    inline abs_edgeset_rt abs_edgeset();                                          // Query the edgeset ref.

    inline size_t num_abs_nodes() const;                                          // Query the absnode count.
    inline size_t num_abs_edges() const;                                          // Query the absedge count.

    inline lut_template_dict_pt lut_template_dict_ptr() const;                    // Query the lut template dict ptr.
    inline lut_template_dict_rt lut_template_dict() const;                        // Query the lut template dict ref.
    
    inline string_t abs_node_name(pin_pt) const;                                  // Query the abs node name.

    void_t write_celllib(string_crt, string_crt);                                 // Write the celllib.
    void_t abstraction();                                                         // Abstraction.

    Abstractor& operator = (const Abstractor&) = delete;                          // Disable copy assignment.

  private:

    string_t _macro_name;                                                         // Macro name.

    timer_pt _timer_ptr;                                                          // Timer ptr.

    abs_node_dict_upt _abs_node_dict_uptr;                                        // Absnode dict uptr.
    abs_edgeset_upt _abs_edgeset_uptr;                                            // Edgeset uptr.
    
    lut_template_dict_upt _lut_template_dict_uptr;                                // LUT template dict uptr.
    
    abs_edge_pt _find_abs_edge(abs_node_pt, abs_node_pt);                         // Find the abs edge.
    abs_edge_pt _find_abs_edge(pin_pt, pin_pt);                                   // Find the abs edge.
    abs_node_pt _insert_abs_node(pin_pt);                                         // Insert abs node.
    abs_edge_pt _insert_abs_edge(abs_node_pt, abs_node_pt, edge_pt, abs_edge_type_ce, timing_sense_ce,unsigned_ct);     // Insert an abs edge.
    abs_edge_pt _insert_abs_edge(abs_node_pt, abs_node_pt, abs_edge_type_ce, timing_sense_ce,unsigned_ct, abs_edge_pt);     // Insert an abs edge.

    abs_edge_pt _insert_abs_edge(pin_pt, pin_pt, edge_pt, abs_edge_type_ce, timing_sense_ce, unsigned_ct);               // Insert an abs edge.
    abs_edge_pt _insert_abs_edge(pin_pt, pin_pt, abs_edge_type_ce, timing_sense_ce, unsigned_ct, abs_edge_pt);

    //abs_edge_pt _insert_abs_edge(edge_pt);                                        // Insert an abs edge.

    void_t _split_jump_to_abs_edges(jump_pt);                                     // Split jump to abs edges.
    void_t _initiate_graph();                                                     // Initiate the abs graph.
    void_t _initiate_graph_to_min();
    void_t _initiate_graph_to_io();
    void_t _abstract_on_abs_edges(); 
    void_t _abstract_forward_on_abs_edges();
    void_t _abstract_on_list_abs_edge();
    void_t _abstract_on_orphan_edges();
    void_t _abstract_cross_not_sameslew_sameload_edges();
    void_t _abstract_cross_abs_edges(size_t, size_t);

    void_t _abstract_timing();                                                    // Abstract the timing.
    void_t _abstract_timing(abs_edge_pt);                                         // Abstract the timing.
    void_t _abstract_back_timing(list<abs_edge_pt>&);
    void_t _abstract_back_timing(abs_edge_pt);

    void_t _abstract_boundary_timing();                                           // Abstract the boundary timing.
    void_t _abstract_constraint_timing(abs_edge_pt);                              // Abstract the constraint timing.
    void_t _abstract_combinational_timing(abs_edge_pt);                           // Abstract the combinational timing.
    void_t _abstract_combinational_timing_with_decided(abs_edge_pt);                           // Abstract the combinational timing with decieded point
    //void_t _abstract_jump_timing(abs_edge_pt);                                    // Abstract the jump timing.
    //void_t _abstract_comb_timing(abs_edge_pt, int, int, int);
    void_t _abstract_rctree_path_timing(abs_edge_pt);                             // Abstract the rctree path timing.
    void_t _abstract_clock_tree_path_timing(abs_edge_pt);                         // Abstract the clock tree path timing.
    void_t _abstract_clock_constraint_timing(abs_edge_pt );
    void_t _abstract_clock_combinational_timing(abs_edge_pt );
    void_t _abstract_back_comb_timing(abs_edge_pt);
    void_t _abstract_forward_comb_timing(abs_edge_pt);
    void_t _abstract_list_timing(abs_edge_pt, list<abs_edge_pt> &);

    void_t _abstract_po_seg_timing(abs_edge_pt);
    void_t _abstract_po_seg_timing(pin_pt, rctree_pt);
    void_t _abstract_po_seg_timing(abs_edge_pt, pin_pt, rctree_pt);                            // Abstract the PO seg timing.
    void_t _abstract_po_seg_timing_v1(abs_edge_pt, pin_pt, rctree_pt);                            // Abstract the PO seg timing.
   
    
    void_t _abstract_load_slew_timing(abs_edge_pt, int, int, int);                // Abstract the load-slew timing.
    void_t _abstract_slew_slew_timing(abs_edge_pt, int, int, int);                // Abstract the slew-slew timing.
    void_t _abstract_list_timing(abs_edge_pt, int, int, int, list<abs_edge_pt> &);
    void_t _remove_abs_edge(abs_edge_pt);                                         // Remove abs node.
    void_t _remove_abs_node(abs_node_pt);                                         // Remove abs node.
    //void_t _initiate_jump_timing(abs_edge_pt, int, int, int);
    void_t _initiate_comb_timing(abs_edge_pt, edge_pt, edge_pt, int, int, int);
    void_t _initiate_back_comb_timing(abs_edge_pt, int, int, int);
    void_t _initiate_po_seg_timing(abs_edge_pt, edge_pt, int, int, int);
    void_t _initiate_clock_constraint_timing(abs_edge_pt, int, int, int);
    void_t _initiate_clock_combinational_timing(abs_edge_pt, int, int, int);
    void_t _initiate_cross_timing(abs_edge_pt, int, int, int, abs_edge_pt, abs_edge_pt);
    //void_t _initiate_constraint_timing(abs_edge_pt, int, int, int);
    //void_t _initiate_combinational_timing(abs_edge_pt, int, int, int);
    void_t _initiate_load_slew_timing_log(abs_edge_pt, int, int, int);            // Initiate the comb timing.
    void_t _initiate_timing_from_next_edge(abs_edge_pt, int, int, int);            // Initiate from next edge timing. (only rctree type)
    //void_t _initiate_slew_slew_timing(abs_edge_pt, int, int, int);                // Initiate the constr timing.
    void_t _write_celllib(int, string_crt);                                       // Write celllib.
    void_t _shrink_lut(timing_lut_rt);
    void_t _update_lut_template();
    void_t _update_lut_template(timing_pt);

    void_t _annotate_pi_d(bool_t* pi, bool_t* po);
    void_t _annotate_po_q(bool_t* po); 

    timing_upt _merge_timing(timing_pt, timing_pt);                               // Merge the timing.
    timing_upt _merge_pos_neg_timing(timing_pt, timing_pt);                       // Merge the timing.

    float_vt _infer_indices(float_t, float_t);                                    // Infer the indices.
    float_vt _infer_range_indices(float_vpt, float_t , float_t) ;
    float_t _same_max_min(float_t minv, float_t maxv);
    //float_pair_t _infer_timing(abs_edge_pt, int, int, float_ct, float_ct);        // Infer the timing.
    float_pair_t _infer_timing(abs_edge_pt, edge_pt, int, int, int , float_ct,float_ct); // Infer the timing.
    float_t      _infer_abs_timing(abs_edge_pt, abs_edge_pt, int, int, int, float_ct , float_ct); // Infer abs timing.
    float_t      _infer_abs_timing(abs_edge_pt, abs_edge_pt, int, int, int);
    float_t      _infer_abs_slew_timing(abs_edge_pt, abs_edge_pt, int, int, int);
    float_t      _infer_abs_slew_timing(abs_edge_pt, abs_edge_pt, int, int, int, float_ct , float_ct); 
//    float_pair_t _infer_ctiming(abs_edge_pt, int, int, int, float_ct, float_ct);  // Infer the timing.
    float_pair_t _infer_rc_timing(abs_edge_pt, int, int, float_ct, float_ct);     // Infer the timing.
    float_pair_t _infer_comb_timing(abs_edge_pt, int , int, int, float_ct , float_ct);
    float_pair_t _infer_comb_non_timing_delay(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_pair_t _infer_comb_non_timing_slew(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_pair_t _infer_clock_comb_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_pair_t _infer_clock_comb_non_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_back_comb_non_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_back_comb_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_forward_slew_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_forward_slew_non_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_forward_comb_non_timing(abs_edge_pt, int , int, int, float_ct, float_ct);
    float_t      _infer_forward_comb_timing(abs_edge_pt, int , int, int,float_ct ,float_ct);
    float_t      _infer_list_timing(abs_edge_pt, int , int, int, list<abs_edge_pt> & );
    float_t      _infer_cross_timing(abs_edge_pt, int , int, int, float_ct ,float_ct, abs_edge_pt, abs_edge_pt);
    float_t      _infer_cross_slew_timing(abs_edge_pt, int , int, int, float_ct ,float_ct, abs_edge_pt, abs_edge_pt);


    float_pair_t _infer_po_seg_timing(abs_edge_pt, int , int, int,float_ct , float_ct);
    float_pair_t _infer_po_seg_timing(abs_edge_pt, edge_pt, int , int, int,float_ct , float_ct);

    float_t _infer_clock_const_timing(abs_edge_pt, int , int, int,float_ct ,float_ct);
    //float_pair_t _infer_rtiming(abs_edge_pt, int, int, float_ct, float_ct);       // Infer the timing.
    //float_pair_t _infer_rtiming(abs_edge_pt, edge_pt, int, int&, float_ct);       // Infer the timing.
    
    lut_template_pt _insert_lut_template(lut_template_type_e, size_t, size_t);    // Insert a lut template.
    timing_pt _initiate_lut_template(abs_edge_pt, int, int, int);                 // Insert a lut template.

    bool_t _infer_clock_const_indices(abs_edge_pt, edge_pt, edge_pt, int, int, int);
    bool_t _infer_indices(abs_edge_pt, edge_pt, edge_pt, int, int, int);
    bool_t _infer_po_seg_indices(abs_edge_pt, edge_pt, int, int, int);  
    bool_t _infer_back_comb_indices(abs_edge_pt, int, int, int);
    bool_t _infer_cross_indices(abs_edge_pt, int, int, int, abs_edge_pt, abs_edge_pt);
   
    bool_t _is_lut_identical(timing_lut_rt lut1, timing_lut_rt lut2);

    edge_pt _find_load_edge( abs_edge_pt );

    bool_t _repeated(vector<node_pt> &, node_pt );
 
 
    rctree_upt _initiate_rctree(rctree_pt);
    size_t _cut_orphan_edges();
    size_t _cut_forward_orphan_edges();
    void_t _abstract_clock();
    void_t _prepared_test(bool_t *);
    void_t _topological_order(size_t* order,size_t & idx);

    float_t _fixed_creddit(abs_edge_pt, int , int , int);
    node_pt _find_common_point(abs_node_pt, abs_node_pt, bool_t*);
    void_t _merge_abs_edge(list<abs_edge_pt>&, abs_edge_type_ce);
    void_t _merge_forward_abs_edge(list<abs_edge_pt>&);
    bool_t _merge_abs_edge(abs_edge_pt a, abs_edge_pt b);
    bool_t _merge_abs_edge(abs_edge_pt a, abs_edge_pt b, int , int, int);
    void_t _merge_abs_edge_rise_delay(timing_pt, timing_pt, int , int, int, size_t, size_t);
    void_t _merge_abs_edge_fall_delay(timing_pt, timing_pt, int , int, int, size_t, size_t);
    void_t _merge_abs_edge_rise_slew(timing_pt, timing_pt, int , int, int, size_t, size_t);
    void_t _merge_abs_edge_fall_slew(timing_pt, timing_pt, int , int, int, size_t, size_t);
    void_t _build_on_abs_edge_graph(queue<abs_node_pt> &, list<abs_edge_pt> &, unordered_set<abs_edge_pt> &, bool_t*);
    void_t _build_forward_on_abs_edges(queue<abs_node_pt> &, list<abs_edge_pt> &, unordered_set<abs_edge_pt> &, bool_t*); 
    void_t _abstract_list_abs_timing(abs_edge_pt, list<abs_edge_pt> &); // TODO
    void_t _abstract_cross_timing(abs_edge_pt, abs_edge_pt, abs_edge_pt); 
    void_t _abstract_cross_timing(abs_edge_pt, int, int, int , abs_edge_pt, abs_edge_pt);
    
};  

// Procedure: set_macro_name
inline void_t Abstractor::set_macro_name(string_crt name) {
  _macro_name = name;
}

// Function: macro_name
inline string_crt Abstractor::macro_name() const {
  return _macro_name;
}

// Function: timer_ptr
inline timer_pt Abstractor::timer_ptr() const {
  return _timer_ptr;
}

// Procedure: set_timer_ptr
inline void_t Abstractor::set_timer_ptr(timer_pt ptr) {
  _timer_ptr = ptr;
}

// Function: lut_template_dict_ptr
inline lut_template_dict_pt Abstractor::lut_template_dict_ptr() const {
  return _lut_template_dict_uptr.get();
}

// Function: lut_template_dict
inline lut_template_dict_rt Abstractor::lut_template_dict() const {
  return *(lut_template_dict_ptr());
}

// Function: abs_node_dict_ptr
inline abs_node_dict_pt Abstractor::abs_node_dict_ptr() const {
  return _abs_node_dict_uptr.get();
}

// Function: abs_node_dict
inline abs_node_dict_rt Abstractor::abs_node_dict() const {
  return *(abs_node_dict_ptr());
}

// Function: abs_edgeset_ptr
inline abs_edgeset_pt Abstractor::abs_edgeset_ptr() {
  return _abs_edgeset_uptr.get();
}

// Function: abs_edgeset
inline abs_edgeset_crt Abstractor::abs_edgeset() const{
  return *(_abs_edgeset_uptr.get());
}

// Function: abs_edgeset
inline abs_edgeset_rt Abstractor::abs_edgeset() {
  return *(abs_edgeset_ptr());
}

// Function: abs_node_name
inline string_t Abstractor::abs_node_name(pin_pt pin_ptr) const {
  if(pin_ptr == nullptr) return "";
  string name = pin_ptr->name();
  replace(name.begin(), name.end(), ':', '_');
  return name;
}

// Function: num_abs_nodes
inline size_t Abstractor::num_abs_nodes() const{
  return abs_node_dict().size();
}

// Function: num_abs_edges
inline size_t Abstractor::num_abs_edges() const{
  return abs_edgeset().size();
}

}; // End of namespace OpenTimer. -----------------------------------------------------------------


#endif


