/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_ENUM_H_
#define OT_ABS_ENUM_H_

namespace OpenTimer {

enum class TimingLUTType{
  CONSTRAINT_TABLE,
  DELAY_TABLE,
  TRANSITION_TABLE
};

enum class AbsEdgeType {
  RCTREE_PATH,
  CONSTRAINT,
  COMBINATIONAL,
  REQUIRED_CONSTRAINT,
  BACK_TRACE_COMB,
  FORWARD_TRACE_COMB,
  PO_SEGMENT,
  CLOCK_CONSTRAINT,
  CLOCK_COMBINATIONAL,
  LIST_ABS_EDGE,
  CROSS_ABS_EDGE,
  UNDEFINED
};

};// End of OpenTimer namespace. ----------------------------------------------------------------

#endif

