/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#include "ot_abs_edge.h"

namespace OpenTimer {

// Static member declaration.
abs_edge_allocator_t AbsEdge::_allocator;                       

// Constructor.
AbsEdge::AbsEdge():
  _idx(OT_UNDEFINED_IDX),
  _num_nonunate(0),
  _abs_edgeset_ptr(nullptr), 
  _type(AbsEdgeType::UNDEFINED),
  _timing_sense(UNDEFINED_TIMING_SENSE),
  _from_abs_node_ptr(nullptr),
  _to_abs_node_ptr(nullptr),
  _slew_e_ptr(nullptr),
  _first_abs_e_ptr(nullptr),
  _fanin_satellite(nullptr),
  _fanout_satellite(nullptr) {

}

// Destructor.
AbsEdge::~AbsEdge() {
}

// Function: from_pin_ptr
pin_pt AbsEdge::from_pin_ptr() const {
  return from_abs_node_ptr()->pin_ptr();
}    

// Function: to_pin_ptr
pin_pt AbsEdge::to_pin_ptr() const {
  return to_abs_node_ptr()->pin_ptr();
}    

// Function: from_node_ptr
node_pt AbsEdge::from_node_ptr() const {
  return from_pin_ptr()->node_ptr();
}

// Function: to_node_ptr
node_pt AbsEdge::to_node_ptr() const {
  return to_pin_ptr()->node_ptr();
}

// Function: insert_merged_pos
timing_pt AbsEdge::insert_merged_pos(int el) {
  if(_merged_pos[el] == nullptr) {
    _merged_pos[el].reset(new timing_t());
    _merged_pos[el].get()->set_from_cellpin_name(from_abs_node_ptr()->name());   
    _merged_pos[el].get()->set_to_cellpin_name(to_abs_node_ptr()->name()); 
  }
  return _merged_pos[el].get();
}

// Function: insert_merged_neg
timing_pt AbsEdge::insert_merged_neg(int el) {
  if(_merged_neg[el] == nullptr) {
    _merged_neg[el].reset(new timing_t());
    _merged_neg[el].get()->set_from_cellpin_name(from_abs_node_ptr()->name());   
    _merged_neg[el].get()->set_to_cellpin_name(to_abs_node_ptr()->name()); 
  }
  return _merged_neg[el].get();
}

// Function: timing_ptr
timing_pt AbsEdge::timing_ptr(int el, int from_rf, int to_rf) {

  switch(type()) {
    // Constraint arc.
    case AbsEdgeType::CLOCK_CONSTRAINT:
      if(from_rf == RISE) return _merged_pos[el].get();
      else return _merged_neg[el].get();
    break;

    // Combinational arc.
    case AbsEdgeType::CROSS_ABS_EDGE: 
    case AbsEdgeType::FORWARD_TRACE_COMB:
    case AbsEdgeType::BACK_TRACE_COMB:
    case AbsEdgeType::CLOCK_COMBINATIONAL:
    case AbsEdgeType::COMBINATIONAL:
    case AbsEdgeType::RCTREE_PATH:
    case AbsEdgeType::PO_SEGMENT:
      if(from_rf == to_rf) return _merged_pos[el].get();     
      else return _merged_neg[el].get();
    break;

    // Undefined.
    default:
      return nullptr;
    break;
  }
}

// Function: insert_timing
timing_pt AbsEdge::insert_timing(int el, int from_rf, int to_rf) {

  switch(type()) {

    // Constraint arc.
    case AbsEdgeType::CLOCK_CONSTRAINT:
      if(from_rf == RISE) return insert_merged_pos(el);
      else return insert_merged_neg(el);
    break;

    // Combinational arc.
    case AbsEdgeType::CROSS_ABS_EDGE:
    case AbsEdgeType::FORWARD_TRACE_COMB:
    case AbsEdgeType::BACK_TRACE_COMB:
    case AbsEdgeType::CLOCK_COMBINATIONAL:
    case AbsEdgeType::COMBINATIONAL:
    case AbsEdgeType::RCTREE_PATH:
    case AbsEdgeType::PO_SEGMENT:
      if(from_rf == to_rf) return insert_merged_pos(el);     
      else return insert_merged_neg(el);
    break;

    // Undefined.
    default:
      return nullptr;
    break;
  }
}
timing_upt AbsEdge::merged_non_timing(int el){
  timing_pt pos=merged_pos(el);
  timing_pt neg=merged_neg(el);
  if( pos == nullptr || neg == nullptr){
    return nullptr;
  }
  timing_upt merged_uptr(new timing_t);
  auto& merged = *(merged_uptr.get());
  merged.set_from_cellpin_name(from_abs_node_ptr()->name());
  merged.set_to_cellpin_name(to_abs_node_ptr()->name());

  merged.set_timing_sense(NON_UNATE);

  // Merge the timing type
  if(pos->timing_type() != neg->timing_type()) {
    //LOG(ERROR) << "Fail to merge timings (timing type exception)";
    return nullptr;
  }
  merged.set_timing_type(pos->timing_type());

  // Merge the lut (exclusively)
  if(!pos->cell_rise().empty() && !neg->cell_rise().empty()) return nullptr;
  if(!pos->cell_fall().empty() && !neg->cell_fall().empty()) return nullptr;
  if(!pos->rise_transition().empty() && !neg->rise_transition().empty()) return nullptr;
  if(!pos->fall_transition().empty() && !neg->fall_transition().empty()) return nullptr;
  if(!pos->rise_constraint().empty() && !neg->rise_constraint().empty()) return nullptr;
  if(!pos->fall_constraint().empty() && !neg->fall_constraint().empty()) return nullptr;


  merged.cell_rise() =       pos->cell_rise().empty() ?       neg->cell_rise() :       pos->cell_rise();
  merged.cell_fall() =       pos->cell_fall().empty() ?       neg->cell_fall() :       pos->cell_fall();
  merged.rise_transition() = pos->rise_transition().empty() ? neg->rise_transition() : pos->rise_transition();
  merged.fall_transition() = pos->fall_transition().empty() ? neg->fall_transition() : pos->fall_transition();
  merged.rise_constraint() = pos->rise_constraint().empty() ? neg->rise_constraint() : pos->rise_constraint();
  merged.fall_constraint() = pos->fall_constraint().empty() ? neg->fall_constraint() : pos->fall_constraint();

  return merged_uptr; 
}


/*// Procedure: merge_timing
void_t AbsEdge::merge_timing() {
  EL_ITER(el) {
    merge_timing(el);
  }
}

// Procedure: merge_timing
void_t AbsEdge::merge_timing(int el) {

  switch(type()) {
    // Constraint arc.
    case AbsEdgeType::CONSTRAINT:
      //_merged_pos[el] = _merge_timing(timing_ptr(el, RISE, RISE), timing_ptr(el, RISE, FALL));
      //_merged_neg[el] = _merge_timing(timing_ptr(el, FALL, RISE), timing_ptr(el, FALL, FALL));
    break;
    
    // Combinational arc.
    case AbsEdgeType::COMBINATIONAL:
    case AbsEdgeType::RCTREE_PATH:
    case AbsEdgeType::JUMP:
    case AbsEdgeType::PO_SEGMENT:
      //_merged_pos[el] = _merge_timing(timing_ptr(el, RISE, RISE), timing_ptr(el, FALL, FALL));
      //_merged_neg[el] = _merge_timing(timing_ptr(el, RISE, FALL), timing_ptr(el, FALL, RISE));
      _merged_non[el] = _merge_pos_neg_timing(_merged_pos[el].get(), _merged_neg[el].get());
    break;

    // Undefined.
    default:
      LOG(ERROR) << "Failed to merge timing (AbsEdgeType exception)";
    break;
  }
}

// Function: _merge_timing
// The function takes a pair of timing ptrs and merge them into the new timing. The data 
// structure partitions a non-unate arc into two timings, one for positive-unate and another
// one for negative-unate. Every positive-unate timing or negative-unate timing is further
// partitioned into two timings per ending transition uniquely defined.
timing_upt AbsEdge::_merge_timing(timing_pt t1, timing_pt t2) {

  if(t1 == nullptr && t2 == nullptr) return nullptr;

  timing_upt merged_uptr(new timing_t);
  auto& merged = *(merged_uptr.get());
  
  // Single arc.
  if(t1 == nullptr || t2 == nullptr || t1 == t2) {
    merged = (t1 == nullptr) ? *t2 : *t1;
    return merged_uptr;
  }

  // Merge the cellpin name.
  if(t1->from_cellpin_name() != t2->from_cellpin_name() ||
     t1->to_cellpin_name() != t2->to_cellpin_name()) {
    LOG(ERROR) << "Fail to merge timings (cellpin name exception)";
    return nullptr;
  }
  merged.set_from_cellpin_name(t1->from_cellpin_name());
  merged.set_to_cellpin_name(t1->to_cellpin_name());

  // Merge the timing sense
  if(t1->timing_sense() != t2->timing_sense()) {
    merged.set_timing_sense(NON_UNATE);
  }
  else merged.set_timing_sense(t1->timing_sense());
  
  //if(t1->timing_sense() != t2->timing_sense()) {
  //  LOG(ERROR) << "Fail to merge timings (timing sense exception)";
  //  return nullptr;
  //}
  //merged.set_timing_sense(t1->timing_sense());
  
  // Merge the timing type
  if(t1->timing_type() != t2->timing_type()) {
    LOG(ERROR) << "Fail to merge timings (timing type exception)";
    return nullptr;
  }
  merged.set_timing_type(t1->timing_type());

  // Merge the lut (exclusively)
  merged.cell_rise() = t1->cell_rise().empty() ? t2->cell_rise() : t1->cell_rise();
  merged.cell_fall() = t1->cell_fall().empty() ? t2->cell_fall() : t1->cell_fall();
  merged.rise_transition() = t1->rise_transition().empty() ? t2->rise_transition() : t1->rise_transition();
  merged.fall_transition() = t1->fall_transition().empty() ? t2->fall_transition() : t1->fall_transition();
  merged.rise_constraint() = t1->rise_constraint().empty() ? t2->rise_constraint() : t1->rise_constraint();
  merged.fall_constraint() = t1->fall_constraint().empty() ? t2->fall_constraint() : t1->fall_constraint();

  return merged_uptr;
}

// Function: _merge_pos_neg_timing
// The function takes a pair of timing ptrs and merge them into the new timing. The data 
// structure partitions a non-unate arc into two timings, one for positive-unate and another
// one for negative-unate. Every positive-unate timing or negative-unate timing is further
// partitioned into two timings per ending transition uniquely defined.
timing_upt AbsEdge::_merge_pos_neg_timing(timing_pt t1, timing_pt t2) {

  if(t1 == nullptr && t2 == nullptr) return nullptr;

  timing_upt merged_uptr(new timing_t);
  auto& merged = *(merged_uptr.get());
  
  // Single arc.
  if(t1 == nullptr || t2 == nullptr || t1 == t2) {
    merged = (t1 == nullptr) ? *t2 : *t1;
    return merged_uptr;
  }

  // Merge the cellpin name.
  if(t1->from_cellpin_name() != t2->from_cellpin_name() ||
     t1->to_cellpin_name() != t2->to_cellpin_name()) {
    LOG(ERROR) << "Fail to merge timings (cellpin name exception)";
    return nullptr;
  }
  merged.set_from_cellpin_name(t1->from_cellpin_name());
  merged.set_to_cellpin_name(t1->to_cellpin_name());

  // Merge the timing sense
  if(t1->timing_sense() == t2->timing_sense()) {
    return nullptr;
  }
  merged.set_timing_sense(NON_UNATE);
  
  // Merge the timing type
  if(t1->timing_type() != t2->timing_type()) {
    LOG(ERROR) << "Fail to merge timings (timing type exception)";
    return nullptr;
  }
  merged.set_timing_type(t1->timing_type());
  
  

  // Merge the lut (exclusively)
  if (t1->cell_rise().empty()){}
  else if (_is_lut_identical(t1->cell_rise(), t2->cell_rise()))
    merged.cell_rise() =t1->cell_rise();
  else return nullptr;

  if(t1->cell_fall().empty()){}
  else if (_is_lut_identical(t1->cell_fall(), t2->cell_fall()))
    merged.cell_fall() =t1->cell_fall();
  else return nullptr;

  if(t1->rise_transition().empty() ){}
  else if (_is_lut_identical(t1->rise_transition(),t2->rise_transition())){
    merged.rise_transition()=t1->rise_transition();
  }
  else return nullptr;

  if (t1->fall_transition().empty() ){}
  else if (_is_lut_identical(t1->fall_transition(), t2->fall_transition()))
    merged.fall_transition() =t1->fall_transition();
  else return nullptr;

  if (t1->rise_constraint().empty()){}
  else if (_is_lut_identical(t1->rise_constraint(), t2->rise_constraint()))
    merged.rise_constraint()=t1->rise_constraint();
  else return nullptr;

  if (t1->fall_constraint().empty()){}
  else if (_is_lut_identical(t1->fall_constraint(),t2->fall_constraint())){
    merged.fall_constraint() = t1->fall_constraint();
  }
  else return nullptr;

  return merged_uptr;
}

bool_t AbsEdge::_is_lut_identical(timing_lut_rt lut1, timing_lut_rt lut2){
  
  float_cmrt t1 =lut1.table();
  float_cmrt t2 =lut2.table();

  if (t1.size()!=t2.size() || t1[0].size()!= t2[0].size() ){
    return false;
  }

    
  size_t idx1= t1.size();
  size_t idx2=t1[0].size();
  if(idx1!=1 && idx2!=1){// not scala
    if(lut1.indices1()!=lut2.indices1())
      return false;
    if(lut1.indices2()!=lut2.indices2()){
      return false;
    }
  }

  for(unsigned_t i =0;i<idx1;++i){
    for(unsigned_t j=0;j<idx2;++j){
      if(t1[i][j] != t2[i][j]){
        return false;
      }
    }
  }
  return true;
}*/

};  // End of namespace OpenTimer. ----------------------------------------------------------------


