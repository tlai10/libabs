/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#include "ot_abstractor.h"

namespace OpenTimer {

// Constructor.
Abstractor::Abstractor() :
  _timer_ptr(nullptr),
  _abs_node_dict_uptr(new abs_node_dict_t()),
  _abs_edgeset_uptr(new abs_edgeset_t()),
  _lut_template_dict_uptr(new lut_template_dict_t()) {
}

// Destructor.
Abstractor::~Abstractor() {
}
void dump(OpenTimer::abstractor_t& abstractor) {
  ofstream minmax("minmax");
  ofstream coutf("edgeinfo");
  ofstream gra("edge_graph.dot");
  size_t clkedge = 0;
  size_t scalar = 0;
  size_t var = 0; 
  
  for(const auto& e : abstractor.abs_edgeset()) {
    if(e!=nullptr){
      abs_node_pt from = e->from_abs_node_ptr();
      abs_node_pt to = e->to_abs_node_ptr();  
      if(to->pin_ptr()->node_ptr()->is_in_clock_tree()){
        clkedge++;
      }else if(from->same_slew() && to->same_load()){
        scalar++;
      }else {
        var++;  
      }
      
    }
  }
  cout<<"clkedge = "<<clkedge<<endl;
  cout<<"scalar = "<<scalar<<endl;
  cout<<"var = "<<var<<endl;


  int fanout_0 = 0;
  int fanout_1 = 0; 
  int fanin_1 = 0;
  int faninout_1 = 0;
  int faninout_2 = 0;
  int test_fanin = 0;
  auto cant_removed = new bool_t[abstractor.timer_ptr()->nodeset().num_indices()];
  memset(cant_removed, 0, sizeof(bool_t)*abstractor.timer_ptr()->nodeset().num_indices());
  int conunt_cant_removed = 0;
  int conunt_cant_removed_edge = 0;
  int count_const_slew_start = 0;
  int self_test = 0;
  
  auto const_slew_start = new bool_t[abstractor.timer_ptr()->nodeset().num_indices()];
  memset(const_slew_start,0, sizeof(bool_t)*abstractor.timer_ptr()->nodeset().num_indices());

  gra<<"digraph { "<<endl; 
  gra<<"rankdir=LR;"<<endl;
  gra<<"graph [pad=\"0.5\", nodesep=\"0.5\", ranksep=\"4\"];"<<endl;
  gra<<"{"<<endl;

  for (const auto& item : abstractor.abs_node_dict()) {
    if(item.second->num_fanouts()==0 && !(item.second->pin_ptr()->is_primary_output()) && item.second->num_fanins() == 1)
      ++fanout_0;

    if(item.second->num_fanouts()==1&& !item.second->pin_ptr()->node_ptr()->is_in_clock_tree())    
      fanout_1++;
    if(item.second->num_fanins()==1 && !item.second->pin_ptr()->node_ptr()->is_in_clock_tree())
      fanin_1++;
    if(item.second->num_fanins()==2 && item.second->num_fanouts()==2){
      faninout_2++;
    }
    if(!item.second->pin_ptr()->node_ptr()->is_in_clock_tree()){
      for(const auto & out: item.second->fanout()){
        for(const auto & in: item.second->fanin()){
          if(out->to_abs_node_ptr()->pin_ptr()->is_constrained() && out->to_abs_node_ptr()->pin_ptr()->test_ptr()->related_pin_ptr() == in->from_abs_node_ptr()->pin_ptr()){
            gra<<item.second->name()<<" [style=filled, fillcolor=red]"<<endl;
          }
        }
      }
    } 
    if(item.second->is_clock()){
      gra<<item.second->name()<<" [style=filled, fillcolor=green]"<<endl;
    } 
    else if(item.second->pin_ptr()->is_primary_input()){
      gra<<item.second->name()<<" [style=filled, fillcolor=blue]"<<endl;
    } 
    else if(item.second->pin_ptr()->is_primary_output()){
      gra<<item.second->name()<<" [style=filled, fillcolor=orange]"<<endl;
    } 
    else if(item.second->pin_ptr()->is_constrained() && item.second->pin_ptr()->test_ptr()->related_pin_ptr() != nullptr){
      gra<<item.second->name()<<" [style=filled, fillcolor=yellow]"<<endl;
    }else if(!item.second->same_slew()){
      gra<<item.second->name()<<" [style=filled, fillcolor=aliceblue]"<<endl;
    }
    else if(!item.second->same_load()){
      gra<<item.second->name()<<" [style=filled, fillcolor=darkseagreen1]"<<endl;
    }
    

    
    


    if(item.second->num_fanouts()==1&& item.second->num_fanins()==1 &&!item.second->pin_ptr()->node_ptr()->is_in_clock_tree()){
      abs_edge_pt out = item.second->fanout().head()->item();
      abs_edge_pt in = item.second->fanin().head()->item();
      
      if(out->to_abs_node_ptr()->pin_ptr()->is_constrained() && out->to_abs_node_ptr()->pin_ptr()->test_ptr()->related_pin_ptr() == in->from_abs_node_ptr()->pin_ptr()){

        self_test++;
      }else{
        faninout_1++;
      }
    }
    if(item.second->pin_ptr()->node_ptr()->is_in_clock_tree()){
      if(cant_removed[item.second->pin_ptr()->node_ptr()->idx()]==false){
        cant_removed[item.second->pin_ptr()->node_ptr()->idx()]=true;
        conunt_cant_removed++;
      }
    }
    if(!item.second->same_slew() || !item.second->same_load()){
      if(cant_removed[item.second->pin_ptr()->node_ptr()->idx()]==false){
        cant_removed[item.second->pin_ptr()->node_ptr()->idx()]=true;
        conunt_cant_removed++;  
      }
      if(!item.second->same_slew()){  
        for(const auto & e:item.second->fanout()){
          abs_node_pt to = e->to_abs_node_ptr();
          //if(to->same_slew() && to->same_load()){
            if(cant_removed[to->pin_ptr()->node_ptr()->idx()]==false){
              cant_removed[to->pin_ptr()->node_ptr()->idx()]=true;
              conunt_cant_removed++;
            }
            if(const_slew_start[to->pin_ptr()->node_ptr()->idx()]==false && !to->pin_ptr()->node_ptr()->is_in_clock_tree() ){
              const_slew_start[to->pin_ptr()->node_ptr()->idx()]=true;
              count_const_slew_start++;
            }
      
          //}else
        }
      }
      if(!item.second->same_load()){
        for(const auto & e:item.second->fanin()){
          abs_node_pt from = e->from_abs_node_ptr();
          if(cant_removed[from->pin_ptr()->node_ptr()->idx()]==false){
            cant_removed[from->pin_ptr()->node_ptr()->idx()]=true;
            conunt_cant_removed++;
          }
        }
      }
    }
    if(item.second->pin_ptr()->is_primary_output() || item.second->pin_ptr()->is_primary_input()){
      if(cant_removed[item.second->pin_ptr()->node_ptr()->idx()]==false){
        cant_removed[item.second->pin_ptr()->node_ptr()->idx()]=true;
        conunt_cant_removed++;
      }
    }
  
    if(item.second->pin_ptr()->is_constrained() && item.second->pin_ptr()->test_ptr()->related_pin_ptr()!=nullptr){
      test_fanin++;
    }


    minmax<< item.second->pin_ptr()->name() <<endl;
    EL_RF_ITER(el, rf) {
      
      minmax<< item.second->min_load(el,rf)<<" "<< item.second->max_load(el,rf)<<"  "<<el<<rf<<endl;  
    }
    minmax<<endl;
  }
  minmax<<endl;
  gra<<"}"<<endl;  

  int rc=0,po_seg=0,clk_comb=0,clk_const=0;
  int comb=0,back_comb = 0;

  ofstream test("test");
  set<int> pre_test;
  set<int> pre_pre_test;
  set<int> pre_pre_pre_test;
  coutf<<"dump..."<<endl;
  for(auto & iter:abstractor.abs_edgeset()) {
    if(iter == nullptr ) continue; 
    if( !iter->to_abs_node_ptr()->pin_ptr()->node_ptr()->is_in_clock_tree()){
      gra<< iter->from_abs_node_ptr()->name().c_str() <<" -> "<< iter->to_abs_node_ptr()->name().c_str()<<";"<<endl;
    }
    if(iter->to_abs_node_ptr()->pin_ptr()->test_ptr()!=nullptr && iter->to_abs_node_ptr()->pin_ptr()->test_ptr()->related_pin_ptr()!= nullptr){
      test<< iter->from_abs_node_ptr()->name().c_str() <<" -> "<< iter->to_abs_node_ptr()->name().c_str()<<" "
          <<iter->from_abs_node_ptr()->num_fanins()<<" "<<iter->from_abs_node_ptr()->num_fanouts()<<"     "
          <<iter->to_abs_node_ptr()->num_fanins()<<" "<<iter->to_abs_node_ptr()->num_fanouts()<<endl;
      pre_test.insert(iter->from_abs_node_ptr()->pin_ptr()->node_ptr()->idx());
      //if(iter->from_abs_node_ptr()->num_fanouts()<10){
        for(const auto & e: iter->from_abs_node_ptr()->fanin()){
          abs_node_pt from = e->from_abs_node_ptr();
          pre_pre_test.insert(from->pin_ptr()->node_ptr()->idx());
          //for(const auto & ee: from->fanin()){
          //  abs_node_pt f = ee->from_abs_node_ptr();
          //  pre_pre_pre_test.insert(f->pin_ptr()->node_ptr()->idx());
          //} 
        }
      //}
    }
    coutf << iter->from_abs_node_ptr()->name().c_str() <<" -> "<< iter->to_abs_node_ptr()->name().c_str()<<"  ";
    //coutf << iter->from_abs_node_ptr()->is_clock()<<" ";
    switch(iter->timing_sense()){
      case POSITIVE_UNATE:
        coutf<<"\tpos";
      break;
      case NEGATIVE_UNATE:
        coutf<<"\tneg";
      break;
      case NON_UNATE:
        coutf<<"\tnon";
      break;
      default:
        coutf<<"\tundefine";
      break;
    
    }
    coutf<<" non: "<<iter->num_nonunate()<<" ";
    switch (iter->type()) {
    case OpenTimer::AbsEdgeType::COMBINATIONAL:
      if(iter->slew_e_ptr()!=nullptr)
        coutf<<iter->slew_e_ptr()->from_node_ptr()->pin_ptr()->name(); 
      coutf<<" comb";
      comb++;
      break;
    case OpenTimer::AbsEdgeType::RCTREE_PATH:
      coutf<<"rctree";
      ++rc;
      break;
    case OpenTimer::AbsEdgeType::PO_SEGMENT:
      coutf<<"po_seg";
      ++po_seg;
      break;
    case OpenTimer::AbsEdgeType::CLOCK_COMBINATIONAL:
     coutf<<"clk_comb";
      ++clk_comb;
      break;
    case OpenTimer::AbsEdgeType::CLOCK_CONSTRAINT:
      coutf<<"clk_const";
      ++clk_const;
      break;
    case OpenTimer::AbsEdgeType::BACK_TRACE_COMB:
      coutf<<"back_comb";
      ++back_comb;
      break;
    case OpenTimer::AbsEdgeType::CROSS_ABS_EDGE:
      coutf<<"cross";
      break;
    default:
      break;
    }
    coutf<<" "<<iter->to_abs_node_ptr()->num_fanins()<<" "<<iter->to_abs_node_ptr()->num_fanouts();
    if(iter->from_abs_node_ptr()->same_slew() && !iter->to_abs_node_ptr()->pin_ptr()->node_ptr()->is_in_clock_tree())
      coutf<<" *";
    if(iter->to_abs_node_ptr()->same_slew() && iter->to_abs_node_ptr()->same_load() && !iter->to_abs_node_ptr()->pin_ptr()->node_ptr()->is_in_clock_tree())
      coutf<<"-";
    if(iter->num_nonunate()>1){
      coutf<<"  +++++++++++++++++++++++++++++++++++++++++++++";
    }
    coutf<<endl;
    if(cant_removed[iter->to_abs_node_ptr()->pin_ptr()->node_ptr()->idx()] == true || cant_removed[iter->from_abs_node_ptr()->pin_ptr()->node_ptr()->idx()] == true){
      conunt_cant_removed_edge++;
    }
  }
  gra<<"}"<<endl;
  cout<< "Num of comb = "<<comb<<endl;
  cout<< "Num of rc = "<<rc<<endl;
  cout<< "Num of po_seg = "<<po_seg<<endl;
  cout<< "Num of clk_comb = "<<clk_comb<<endl;
  cout<< "Num of clk_const = "<<clk_const<<endl;
  cout<< "Num of back_comb = "<<back_comb<<endl;
  cout<< "Num of fanout is 0 = "<<fanout_0<<endl;
  cout<< "Num of fanout is 1 = "<<fanout_1<<endl;
  cout<< "Num of fanin is 1 = "<<fanin_1<<endl;
  cout<< "Num of fanin fanout is 1 = "<<faninout_1<<endl;
  cout<< "Num of test fanins = "<<test_fanin<<endl;
  cout<< "Num of cannot removed node = "<<conunt_cant_removed<<endl;
  cout<< "Num of cannot removed edge = "<<conunt_cant_removed_edge<<endl;
  cout<< "Num of const same slew start = "<<count_const_slew_start<<endl;
  cout<< "Num of self test = "<<self_test<<endl;
  cout<< "Num of pre test = "<<pre_test.size()<<endl;
  cout<< "Num of pre pre test = "<<pre_pre_test.size()<<endl;
  cout<< "Num of pre pre pre test = "<<pre_pre_test.size()<<endl;
  cout<< "Num of fanin fanout is 2 = "<<faninout_2<<endl; 
  delete [] cant_removed;
  ofstream non("nonu");
  for(const auto& e : abstractor.abs_edgeset()){ 
    if(e!=nullptr && e->timing_sense()==NON_UNATE){
        EL_RF_RF_ITER(el, irf, orf){
          non<<e->from_abs_node_ptr()->pin_ptr()->name() <<" -> "
                <<e->to_abs_node_ptr()->pin_ptr()->name()<<" ";
          timing_pt timing_ptr = e->timing_ptr(el, irf, orf);   
          if(timing_ptr!=nullptr){
            auto lut = (orf==RISE)? timing_ptr->cell_rise(): timing_ptr->cell_fall();
            if(!lut.empty()){ 
              non<< (el==EARLY?"EARLY ":"LATE  ")<<(irf==RISE?"R":"F")<<(orf==RISE?"R ":"F ")<<lut.table(0,0)<<" "<<e->num_nonunate();
              if(e->type() == AbsEdgeType::BACK_TRACE_COMB)
                non<<" back_comb ";
              if(e->slew_e_ptr()!=nullptr)
                non<<e->slew_e_ptr()->from_node_ptr()->pin_ptr()->name()<<" -> "<<e->slew_e_ptr()->to_node_ptr()->pin_ptr()->name();
            }
          } 

          non<<endl;    
        }
      }
  }
  ofstream none("enonu");
  for(const auto& e : *(abstractor.timer_ptr()->edgelist_ptr())){
    if(e!= nullptr&& e->timing_sense()==NON_UNATE){
      EL_RF_RF_ITER(el, irf, orf){
        timing_pt timing_ptr = e->timing_ptr(el, irf, orf);
        
        none<<e->from_node_ptr()->pin_ptr()->name() <<" -> "
              <<e->to_node_ptr()->pin_ptr()->name()<<" ";
        
        if(timing_ptr!=nullptr){
          auto lut = (orf==RISE)? timing_ptr->cell_rise(): timing_ptr->cell_fall();
          
          if(!lut.empty())
            none<<(el==EARLY?"EARLY ":"LATE  ")<<(irf==RISE?"R":"F")<<(orf==RISE?"R ":"F ")
                <<e->timing_arc_ptr(el)->delay(irf, orf,e->from_node_ptr()->slew(el,irf), e->to_node_ptr()->pin_ptr()->load(el,orf) );
        }
        none<<endl;

      }
    }
  }
  delete [] const_slew_start;
}


// Procedure: abstraction
// The main procedure of the abstraction which consists of several steps as follows:
//
// Step 1: Initiate the abstraction graph (associated with abs_node and abs_edge)
// Step 2: Initialize the boundary timing to find out the max/min possible slew and load.
// Step 3: Initiate the timing lut structures (template & indices).
// Step 4: Iterate over the abs edge set and perform the timing abstraction per abs edge.

void_t Abstractor::abstraction() {

  LOG(INFO) << "Abstracting macro " + macro_name() + " ...";

  // Initiate the abstract graph through abs edge connections.
  {
    size_t size=1;
    
    if (timer_ptr()->circuit_ptr()->clock_tree_ptr()->level()<0){
   
      _initiate_graph_to_min();
      // Abstract the boundary timing.
      _abstract_boundary_timing();
      
    }else{
      _initiate_graph_to_io();
      while(size>0){
        size = _cut_orphan_edges();
      } 
      LOG(INFO)<< "Successfully cut orphans";
      // Abstract the boundary timing.
      _abstract_boundary_timing();

      _abstract_clock();
    }

  }
  // After the initiation, we perform abstraction to generate the timing lut for each abs edge.
  _abstract_timing();
  

  


  _abstract_on_abs_edges(); 

  _abstract_forward_on_abs_edges();  

  _abstract_on_orphan_edges();
  _abstract_on_orphan_edges();

  _abstract_cross_abs_edges(2, 2); 

  

  _abstract_cross_abs_edges(2, 3); 
  
  //_abstract_cross_abs_edges(3, 2);
  //_abstract_cross_abs_edges(3, 2);
  _abstract_cross_abs_edges(2, 2);
  _abstract_cross_abs_edges(2, 3);

  _abstract_on_abs_edges();
  _abstract_cross_not_sameslew_sameload_edges();
  _abstract_forward_on_abs_edges();
  //for DEGUB

  // Insert the lut templates.
  _update_lut_template();

  LOG(INFO) << "Successfully abstracted macro " + macro_name();

  //timer_ptr()->report_timer();
  //TODO For DEBUG
  //dump(*this);
  //cout << "Num abs nodes = " << num_abs_nodes() << "\n";
  //cout << "Num abs edges = " << num_abs_edges() << "\n";

}

// Procedure: _initiate_rctree
rctree_upt Abstractor::_initiate_rctree(rctree_pt rctree_ptr) {

  if(rctree_ptr == nullptr) {
    return nullptr;
  }

  rctree_upt new_rctree_uptr(new rctree_t());
  auto new_rctree_ptr = new_rctree_uptr.get();

  // Iterate over the edgelist in the given rctree and insert each to the new rctree.
  for (auto & rcnode_iter : *(rctree_ptr->rctree_node_dict_ptr())) {
    //if(_rctree_ptr->rctree_node_ptr(rcnode_iter.first)!=nullptr) return;

    //auto rctree_node_ptr = _rctree_ptr->rctree_node_ptr(rcnode_iter.first);
    new_rctree_ptr->insert_rctree_node(rcnode_iter.first);

    for( auto & fanout_iter : *(rcnode_iter.second->fanout_ptr())) {
      auto to_rctree_node_ptr = new_rctree_ptr->rctree_node_ptr(fanout_iter->to_rctree_node_ptr()->name());
      if(to_rctree_node_ptr==nullptr) new_rctree_ptr->insert_rctree_node(fanout_iter->to_rctree_node_ptr()->name());

      new_rctree_ptr->insert_rctree_edge(fanout_iter->from_rctree_node_ptr()->name(),fanout_iter->to_rctree_node_ptr()->name(), fanout_iter->res());
    }
  }

  // Iterate over the nodes in the given rctree and copy the cap to the new rctree node.
  for(auto & item : rctree_ptr->rctree_node_dict() ) {
    EL_RF_ITER(el,rf) {
      new_rctree_ptr->set_cap(item.second->name(), el, rf, item.second->cap(el, rf));
    }
  }
  new_rctree_ptr->set_root_ptr(new_rctree_ptr->rctree_node_ptr(rctree_ptr->root_ptr()->name()));

  return new_rctree_uptr;
}

// Function: _insert_abs_node
abs_node_pt Abstractor::_insert_abs_node (pin_pt pin_ptr) {

  if(pin_ptr == nullptr) return nullptr;

  auto abs_node_ptr = abs_node_dict().insert(abs_node_name(pin_ptr));
  abs_node_ptr->set_pin_ptr(pin_ptr);

  return abs_node_ptr;
}

// Procedure: _remove_abs_node
void_t Abstractor::_remove_abs_node(abs_node_pt abs_node_ptr) {
  if(abs_node_ptr == nullptr) return;
  auto name = abs_node_ptr->name();
  abs_node_dict().remove(name);
}

// Function: _find_abs_edge
// The function finds the edge that connects the given two endpoints and returns the pointer
// pointing to this edge.
abs_edge_pt Abstractor::_find_abs_edge(pin_pt from_pin_ptr, pin_pt to_pin_ptr) {
  auto from_name = abs_node_name(from_pin_ptr);
  auto to_name = abs_node_name(to_pin_ptr);
  return _find_abs_edge(abs_node_dict()[from_name], abs_node_dict()[to_name]);
}

// Function: _find_abs_edge
// The function finds the edge that connects the given two endpoints and returns the pointer
// pointing to this edge.
abs_edge_pt Abstractor::_find_abs_edge(abs_node_pt from_abs_node_ptr, abs_node_pt to_abs_node_ptr) {

  if(!from_abs_node_ptr || !to_abs_node_ptr) return nullptr;

  abs_edgelist_pt abs_edgelist_ptr;

  // Obtain the edgelist with smaller size, which could be better for later search.
  if(from_abs_node_ptr->num_fanouts() > to_abs_node_ptr->num_fanins()) {
    abs_edgelist_ptr = from_abs_node_ptr->fanout_ptr();
  } else {
    abs_edgelist_ptr = to_abs_node_ptr->fanin_ptr();
  }

  // Search the edge.
  for(const auto& e : *abs_edgelist_ptr) {
    if(e->from_abs_node_ptr() == from_abs_node_ptr && e->to_abs_node_ptr() == to_abs_node_ptr)  {
      return e;
    }
  }

  return nullptr;
}

// Function: _insert_abs_edge
abs_edge_pt Abstractor::_insert_abs_edge(
  abs_node_pt from_abs_node_ptr,
  abs_node_pt to_abs_node_ptr,
  abs_edge_type_ce type,
  timing_sense_ce timing_sense,
  unsigned_ct num_nonunate,
  abs_edge_pt first_e
) {

  if(from_abs_node_ptr == nullptr || to_abs_node_ptr == nullptr) return nullptr;

  //CHECK(type == AbsEdgeType::BACK_TRACE_COMB || type == AbsEdgeType::FORWARD_TRACE_COMB || type == AbsEdgeType::LIST_ABS_EDGE);

  int_t idx = abs_edgeset_ptr()->insert();
  abs_edge_pt abs_edge_ptr = abs_edgeset()[idx];
  abs_edge_ptr->set_idx(idx);
  abs_edge_ptr->set_type(type);
  abs_edge_ptr->set_abs_edgeset_ptr(abs_edgeset_ptr());
  abs_edge_ptr->set_from_abs_node_ptr(from_abs_node_ptr);
  abs_edge_ptr->set_to_abs_node_ptr(to_abs_node_ptr);
  abs_edge_ptr->set_timing_sense(timing_sense);
  abs_edge_ptr->set_num_nonunate(num_nonunate);
  abs_edge_ptr->set_first_abs_e_ptr(first_e);
  from_abs_node_ptr->insert_fanout(abs_edge_ptr);
  to_abs_node_ptr->insert_fanin(abs_edge_ptr);

  return abs_edge_ptr;
}

// Function: _insert_abs_edge
abs_edge_pt Abstractor::_insert_abs_edge(
pin_pt from_pin, 
pin_pt to_pin, 
abs_edge_type_ce type, 
timing_sense_ce timing_sense, 
unsigned_ct num_nonunate,
abs_edge_pt first_e
) {
  CHECK(from_pin!= to_pin);
  if(from_pin == nullptr || to_pin == nullptr) return nullptr;

  auto from_abs_node = _insert_abs_node(from_pin);
  auto to_abs_node = _insert_abs_node(to_pin);


  return _insert_abs_edge(from_abs_node, to_abs_node, type, timing_sense, num_nonunate, first_e);
}
// Function: _insert_abs_edge
abs_edge_pt Abstractor::_insert_abs_edge(
  abs_node_pt from_abs_node_ptr,
  abs_node_pt to_abs_node_ptr,
  edge_pt slew_e_ptr,
  abs_edge_type_ce type,
  timing_sense_ce timing_sense,
  unsigned_ct num_nonunate
) {

  if(from_abs_node_ptr == nullptr || to_abs_node_ptr == nullptr) return nullptr;

  int_t idx = abs_edgeset_ptr()->insert();
  abs_edge_pt abs_edge_ptr = abs_edgeset()[idx];
  abs_edge_ptr->set_idx(idx);
  abs_edge_ptr->set_type(type);
  abs_edge_ptr->set_abs_edgeset_ptr(abs_edgeset_ptr());
  abs_edge_ptr->set_from_abs_node_ptr(from_abs_node_ptr);
  abs_edge_ptr->set_to_abs_node_ptr(to_abs_node_ptr);
  abs_edge_ptr->set_slew_e_ptr(slew_e_ptr);
  abs_edge_ptr->set_timing_sense(timing_sense);
  abs_edge_ptr->set_num_nonunate(num_nonunate);
  from_abs_node_ptr->insert_fanout(abs_edge_ptr);
  to_abs_node_ptr->insert_fanin(abs_edge_ptr);

  return abs_edge_ptr;
}

// Function: _insert_abs_edge
abs_edge_pt Abstractor::_insert_abs_edge(pin_pt from_pin, pin_pt to_pin, edge_pt slew_e , abs_edge_type_ce type, timing_sense_ce timing_sense, unsigned_ct num_nonunate) {
  CHECK(from_pin!= to_pin)<< from_pin->name();
  if(from_pin == nullptr || to_pin == nullptr) return nullptr;

  auto from_abs_node = _insert_abs_node(from_pin);
  auto to_abs_node = _insert_abs_node(to_pin);
//  auto decided_abs_node = _insert_abs_node(decided_pin);

  return _insert_abs_edge(from_abs_node, to_abs_node, slew_e, type, timing_sense,num_nonunate);
}

// Procedure: _remove_abs_edge
void_t Abstractor::_remove_abs_edge (abs_edge_pt abs_edge_ptr) {
  abs_edge_ptr->from_abs_node_ptr()->remove_fanout(abs_edge_ptr);
  abs_edge_ptr->to_abs_node_ptr()->remove_fanin(abs_edge_ptr);
  abs_edgeset_ptr()->remove(abs_edge_ptr->idx());
}
/*
  abs_edge_ptr     edge_ptr
  (el,irf,orf)    (el,orf,eorf)
 o ----------- o ------------- o
  new_timing_ptr old_timing_ptr
*/
void_t Abstractor::_initiate_timing_from_next_edge(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {
  if(abs_edge_ptr->type()!=AbsEdgeType::RCTREE_PATH) return ;
  auto new_timing_ptr = abs_edge_ptr -> insert_timing(el, irf, orf);
  auto& new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise() : new_timing_ptr->cell_fall();
  auto& new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition() : new_timing_ptr->fall_transition();

  edge_pt edge_ptr;
  if (abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->fanout().head()==nullptr) return;

  edge_ptr = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->fanout().head()->item();

  int eorf = (edge_ptr->timing_sense()==POSITIVE_UNATE)? orf : !orf;


  if (edge_ptr->timing_ptr(el,orf,eorf)==nullptr) return;
  timing_pt old_timing_ptr = edge_ptr->timing_ptr(el,orf,eorf);
  auto & old_dlut = (eorf == RISE) ? old_timing_ptr->cell_rise() : old_timing_ptr->cell_fall();
  auto & old_tlut = (eorf == RISE) ? old_timing_ptr->rise_transition() : old_timing_ptr->fall_transition();


  float_vt* old_dslew_indices_ptr(nullptr);
  float_vt* old_tslew_indices_ptr(nullptr);


  // set up slew indices
  float_t idx2_min = abs_edge_ptr->to_abs_node_ptr()->min_slew(el,orf);
  float_t idx2_max = abs_edge_ptr->to_abs_node_ptr()->max_slew(el,orf);

  if(old_dlut.lut_template_ptr() != nullptr) {
    old_dslew_indices_ptr = (old_dlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                            &old_dlut.indices2(): & old_dlut.indices1();
  }
  if(old_tlut.lut_template_ptr() != nullptr) {
    old_tslew_indices_ptr = (old_tlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                            &old_tlut.indices2(): & old_tlut.indices1();
  }

  // Declare: infer_range_indices_from_next
  auto infer_range_indices_from_next = [] (float_vpt ptr, float_t minv, float_t maxv) -> float_vt {
    if(ptr == nullptr) return float_vt(1, (minv + maxv) / OT_FLT_TWO);
    maxv = max(minv + OT_FLT_ONE, maxv);
    float_vt out;
    out.push_back(minv);
    for(auto i = upper_bound(ptr->begin(), ptr->end(), minv); i != ptr->end() && *i < maxv; ++i) {
      if(*i > out.back() + OT_FLT_ONE) {
        out.push_back(*i);
      }
    }
    if(maxv > out.back() + OT_FLT_ONE) out.push_back(maxv);
    return out;
  };

  auto dslew_indices = infer_range_indices_from_next((old_dslew_indices_ptr),idx2_min,idx2_max);
  auto tslew_indices = infer_range_indices_from_next((old_tslew_indices_ptr),idx2_min,idx2_max);

  new_dlut.resize(1, dslew_indices.size());
  new_tlut.resize(1, tslew_indices.size());

  new_dlut.indices1()[0] = (abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->load(el,orf));
  new_tlut.indices1()[0] = (abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->load(el,orf));

  new_dlut.indices2() = dslew_indices;
  new_tlut.indices2() = tslew_indices;

}


// Procedure: _initiate_load_slew_timing
// The procedure initiates the load-slew timing structurs, including the lut indices and lut
// template. By default, the lut template adheres the following structure:
//
// variable1 (index1): total_output_net_capacitance
// variable2 (index2): input_net_transition
///
void_t Abstractor::_initiate_load_slew_timing_log(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  auto timing_ptr = abs_edge_ptr -> insert_timing(el, irf, orf);
  auto& dlut = (orf == RISE) ? timing_ptr->cell_rise() : timing_ptr->cell_fall();
  auto& tlut = (orf == RISE) ? timing_ptr->rise_transition() : timing_ptr->fall_transition();

  float_vt load_indices = _infer_indices(abs_edge_ptr->  to_abs_node_ptr()->min_load(el, orf),abs_edge_ptr->  to_abs_node_ptr()->max_load(el, orf));
  float_vt slew_indices = _infer_indices(abs_edge_ptr->from_abs_node_ptr()->min_slew(el, irf),abs_edge_ptr->from_abs_node_ptr()->max_slew(el, irf));

  dlut.resize(load_indices.size(), slew_indices.size());
  tlut.resize(load_indices.size(), slew_indices.size());

  dlut.indices1() = load_indices;
  tlut.indices1() = load_indices;
  dlut.indices2() = slew_indices;
  tlut.indices2() = slew_indices;

  // Set the timing related field.
  timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);

  switch(abs_edge_ptr->type()) {
/*  case AbsEdgeType::COMBINATIONAL:
    timing_ptr->set_timing_type(abs_edge_ptr->edge_ptr()->timing_ptr(el, irf, orf)->timing_type());
    break;
*/
  case AbsEdgeType::RCTREE_PATH:
    timing_ptr->set_timing_type(TimingType::COMBINATIONAL);
    break;
  default:
    //CHECK(false);
    break;
  }
}
/*
// Procedure: _initiate_slew_slew_timing
// The procedure initiates the slew-slew timing structurs, including the lut indices and lut
// template. By default, the lut template adheres the following structure:
//
// variable1 (index1): constraint_pin_transition
// variable2 (index2): related_pin_transition
//
void_t Abstractor::_initiate_slew_slew_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  auto timing_ptr = abs_edge_ptr -> insert_timing(el, irf, orf);
  auto& clut = (orf == RISE) ? timing_ptr->rise_constraint() : timing_ptr->fall_constraint();
  float_vt slew1_indices = _infer_indices(abs_edge_ptr->to_abs_node_ptr()->min_slew(el, orf),abs_edge_ptr->to_abs_node_ptr()->max_slew(el, orf));
  float_vt slew2_indices = _infer_indices(abs_edge_ptr->from_abs_node_ptr()->min_slew(el, irf),abs_edge_ptr->from_abs_node_ptr()->max_slew(el, irf));
  clut.resize(slew1_indices.size(), slew2_indices.size());

  clut.indices1() = slew1_indices;
  clut.indices2() = slew2_indices;

  // Set the timing sense and timing type.
  timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
  timing_ptr->set_timing_type(abs_edge_ptr->edge_ptr()->timing_ptr(el, irf, orf)->timing_type());
}
*/

// given a comb find load_e
edge_pt Abstractor::_find_load_edge(abs_edge_pt abs_edge_ptr) {


  edge_pt e = abs_edge_ptr->slew_e_ptr();
  edge_pt load_e = e;
  node_pt v = e->to_node_ptr();
  if(e->to_node_ptr() !=nullptr && e->to_node_ptr()->num_fanouts()!=1) return load_e;
 
  while(e->to_node_ptr() !=nullptr  && v != abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr() ) {

    //CHECK( v -> num_fanouts()==1)<< v -> num_fanouts()<<" "<< abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name()<< " -> "<<
    //abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name();
    
    e = v -> fanout().head()->item();
 
    //CHECK(e->type() != UNDEFINED_EDGE_TYPE);
    if(e->type() ==CONSTRAINT_EDGE_TYPE || e->type() == COMBINATIONAL_EDGE_TYPE) {
      load_e=e;
    }

    

    v = e->to_node_ptr();

  }

  return load_e;

}

// Declare: _infer_range_indices
float_vt Abstractor::_infer_range_indices(float_vpt ptr, float_t minv, float_t maxv) {

  if(ptr == nullptr) return float_vt(1, (minv + maxv) / OT_FLT_TWO);
  maxv = max(minv + OT_FLT_ONE, maxv);
  float_vt out;
  out.push_back(minv);
  for(auto i = upper_bound(ptr->begin(), ptr->end(), minv); i != ptr->end() && *i < maxv; ++i) {
    if(*i > out.back() + OT_FLT_ONE) {
      out.push_back(*i);
    }
  }
  if(maxv > out.back() + OT_FLT_ONE) out.push_back(maxv);
  return out;
}


bool_t Abstractor::_infer_po_seg_indices(abs_edge_pt abs_edge_ptr, edge_pt slew_e, int el, int irf, int orf) {

  if (abs_edge_ptr ==nullptr || slew_e == nullptr) {
    LOG(WARNING)<<" null abs_edge_ptr || null= slew_e";
    return false ;
  }


  timing_pt slew_timing_ptr = slew_e->timing_ptr(el,irf,orf);

  if (slew_timing_ptr == nullptr) {
    //CHECK(slew_timing_ptr != nullptr);
    //CHECK(load_timing_ptr!=nullptr);
    
//        if(slew_timing_ptr == nullptr)
//          <<" no timing on slew:"<<slew_e->from_node_ptr()->pin_ptr()->name()<<" -> "<< slew_e->to_node_ptr()->pin_ptr()->name()<<" irf:"<<irf<<" orf:"<<orf<<endl;
    
    return false ;

  }


  // Initiate a new timing ptr.
  timing_pt new_timing_ptr= abs_edge_ptr->insert_timing(el,irf,orf);

  //LOG(INFO)<<"          Start infer indeces";
  auto & new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise() : new_timing_ptr->cell_fall();
  auto & new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition() : new_timing_ptr->fall_transition();

  auto & old_s_dlut = (orf == RISE) ? slew_timing_ptr->cell_rise() : slew_timing_ptr->cell_fall();
  auto & old_s_tlut = (orf == RISE) ? slew_timing_ptr->rise_transition() : slew_timing_ptr->fall_transition();


  float_t idx1_min;   // Could be load or slew
  float_t idx1_max;

  float_t idx2_min;   // slew
  float_t idx2_max;

  float_vt* old_didx1_ptr(nullptr);
  float_vt* old_didx2_ptr(nullptr);

  float_vt* old_tidx1_ptr(nullptr);
  float_vt* old_tidx2_ptr(nullptr);



  //set up load indces
  idx1_min = abs_edge_ptr->to_abs_node_ptr()->min_load(el,orf);
  idx1_max = abs_edge_ptr->to_abs_node_ptr()->max_load(el,orf);

  if(old_s_dlut.lut_template_ptr() != nullptr) {
    old_didx1_ptr = (old_s_dlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_dlut.indices1(): & old_s_dlut.indices2();
  }
  if(old_s_tlut.lut_template_ptr() != nullptr) {
    old_tidx1_ptr = (old_s_tlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_tlut.indices1(): & old_s_tlut.indices2();
  }

  // set up slew indices
  idx2_min = abs_edge_ptr->from_abs_node_ptr()->min_slew(el,irf);
  idx2_max = abs_edge_ptr->from_abs_node_ptr()->max_slew(el,irf);

  if(old_s_dlut.lut_template_ptr() != nullptr) {
    old_didx2_ptr = (old_s_dlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_dlut.indices2(): & old_s_dlut.indices1();
  }
  if(old_s_tlut.lut_template_ptr() != nullptr) {
    old_tidx2_ptr = (old_s_tlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_tlut.indices2(): & old_s_tlut.indices1();
  }

  // assign load indices
  // slew_e != last edge
  net_pt net_ptr= slew_e->to_node_ptr()->pin_ptr()->net_ptr();
 
  CHECK(slew_e->type()!=RCTREE_EDGE_TYPE);
  if(slew_e->to_node_ptr() != abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr() &&  net_ptr!=nullptr && net_ptr->rctree_ptr()!=nullptr ) {
    rctree_pt rctree_ptr = net_ptr->rctree_ptr();
    
    if (fabs(idx1_max - idx1_min) <= OT_FLT_ONE) {
      new_dlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
      new_tlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
    } 
    else{
      float_t rcl=0;
      //CHECK(rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name())!=nullptr)<< rctree_ptr->root_ptr()->name()<<" "<< abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name()<<endl;
      if(rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name()) != nullptr )
        rcl= rctree_ptr->load(el,orf) - rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name())->load(el,orf);
      else 
        rcl= rctree_ptr->load(el,orf);
      auto indices = _infer_range_indices((old_didx1_ptr),idx1_min+rcl ,idx1_max+rcl);
      
      for(unsigned_t i=0;i< indices.size();++i ){
        indices[i] = indices[i]-rctree_ptr->load(el,orf);
      }
      
      new_dlut.indices1() = indices;
      new_tlut.indices1() = indices;
    }

  }
  //rc_tree ==nullptr
  else {
    if (fabs(idx1_max - idx1_min) <= OT_FLT_ONE) {
      new_dlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
      new_tlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);

    } else {
      new_dlut.indices1() = _infer_range_indices((old_didx1_ptr),idx1_min,idx1_max);
      new_tlut.indices1() = _infer_range_indices((old_tidx1_ptr),idx1_min,idx1_max);
    }

  }


  // slew indices
  // decided != from abs node
  net_ptr= abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->net_ptr();
  if (abs_edge_ptr->slew_e_ptr()->from_node_ptr()!= abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr() && net_ptr!=nullptr && net_ptr->rctree_ptr()!=nullptr ) {


    rctree_pt rctree_ptr = net_ptr->rctree_ptr();
    rctree_node_pt rctree_node_ptr = rctree_ptr->rctree_node_ptr(abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->name());

    if (fabs(idx2_max - idx2_min) <= OT_FLT_ONE) {

      //idx2 min max is abs_edge from node slew min mas
      // because rctree is always positive unate => leave node rf = irf

      float_t slew_in = (idx2_min + idx2_max)/OT_FLT_TWO;
      new_dlut.indices2().push_back(slew_in);
      new_tlut.indices2().push_back(slew_in);

      CHECK(std::isnan(slew_in)==0);
    } 
    else{
      idx2_min=rctree_ptr->slew(rctree_node_ptr,el,irf,idx2_min);
      idx2_max=rctree_ptr->slew(rctree_node_ptr,el,irf,idx2_max);

      auto indeces = _infer_range_indices((old_didx2_ptr),idx2_min,idx2_max);



      for(unsigned_t i=0; i< indeces.size(); ++i ) {
       indeces[i] = sqrt(fabs(indeces[i]*indeces[i]- rctree_node_ptr->impulse(el,irf)));
      }
      new_dlut.indices2() = indeces;
      new_tlut.indices2() = indeces;
    }
  }
  // decided = from abs node
  else {
    if (fabs(idx2_max - idx2_min) <= OT_FLT_ONE) {
      new_dlut.indices2().push_back((idx2_min + idx2_max) / OT_FLT_TWO);
      new_tlut.indices2().push_back((idx2_min + idx2_max) / OT_FLT_TWO);
    } else {
      new_dlut.indices2() = _infer_range_indices((old_didx2_ptr),idx2_min,idx2_max);
      new_tlut.indices2() = _infer_range_indices((old_tidx2_ptr),idx2_min,idx2_max);
    }
  }
  //CHECK(!new_dlut.indices1().empty() && !new_dlut.indices2().empty());
  //CHECK(!new_tlut.indices1().empty() && !new_tlut.indices2().empty());


  new_dlut.resize(new_dlut.size1(),new_dlut.size2());
  new_tlut.resize(new_tlut.size1(),new_tlut.size2());
 
  return true;

}
//------------------------------------------------------------------------------------------------
bool_t Abstractor::_infer_cross_indices(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, abs_edge_pt a, abs_edge_pt b){
  if (abs_edge_ptr ==nullptr){
    LOG(WARNING)<<" null abs_edge_ptr";
    return false;
  }

  // Initiate a new timing ptr.
  timing_pt new_timing_ptr= abs_edge_ptr->insert_timing(el,irf,orf);

  auto & new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise() : new_timing_ptr->cell_fall();
  auto & new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition() : new_timing_ptr->fall_transition();

  float_t idx1_min = abs_edge_ptr->to_abs_node_ptr()->min_load(el, orf);
  float_t idx1_max = abs_edge_ptr->to_abs_node_ptr()->max_load(el, orf);

  float_t idx2_min = abs_edge_ptr->from_abs_node_ptr()->min_slew(el, irf);
  float_t idx2_max = abs_edge_ptr->from_abs_node_ptr()->max_slew(el, irf);

  abs_edge_pt load_edge = b;
  abs_edge_pt slew_edge = a;

  if( abs_edge_ptr->to_abs_node_ptr()->same_load()){
    float_t load = (idx1_min + idx1_max) / OT_FLT_TWO;
    new_dlut.indices1().push_back(load);
    new_tlut.indices1().push_back(load);
  }else{
    if(load_edge == nullptr){
      LOG(WARNING)<< "load_edge == nullptr";
      return false;
    }
    
    int olrf = (load_edge->timing_sense() == POSITIVE_UNATE)? orf : !orf;
    
    timing_pt old_timing_ptr = load_edge->timing_ptr(el, olrf,orf);
    CHECK(old_timing_ptr != nullptr)<< el<<olrf<<orf<<" "<<load_edge->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<< load_edge->to_abs_node_ptr()->pin_ptr()->name()<<" "
    <<((load_edge->type() == AbsEdgeType::PO_SEGMENT)?"is pos seg":"not po seg");
    //if(old_timing_ptr == nullptr )
    //  old_timing_ptr = load_edge->timing_ptr(el, !olrf,orf);
    auto & old_dlut = (orf == RISE) ? old_timing_ptr->cell_rise() : old_timing_ptr->cell_fall();
    auto & old_tlut = (orf == RISE) ? old_timing_ptr->rise_transition() : old_timing_ptr->fall_transition();

    new_dlut.indices1() = old_dlut.indices1() ;
    new_tlut.indices1() = old_tlut.indices1() ;
  }
  
  if( abs_edge_ptr->from_abs_node_ptr()->same_slew()){
    float_t slew_in = (idx2_min + idx2_max)/OT_FLT_TWO;
    
    new_dlut.indices2().push_back(slew_in);
    new_tlut.indices2().push_back(slew_in);
  }else{
    if(slew_edge == nullptr){
      LOG(WARNING)<< "slew_edge == nullptr";
      return false;
    }
    int isrf = (slew_edge->timing_sense() == POSITIVE_UNATE)? irf : !irf;
    timing_pt old_timing_ptr = slew_edge->timing_ptr(el,irf,isrf);
    auto & old_dlut = (isrf == RISE) ? old_timing_ptr->cell_rise() : old_timing_ptr->cell_fall();
    auto & old_tlut = (isrf == RISE) ? old_timing_ptr->rise_transition() : old_timing_ptr->fall_transition();
    new_dlut.indices2() = old_dlut.indices2() ;
    new_tlut.indices2() = old_tlut.indices2() ;
  }
  
  new_dlut.resize(new_dlut.size1(),new_dlut.size2());
  new_tlut.resize(new_tlut.size1(),new_tlut.size2());

  return true;

}
//--------------------------------------------------------------------------------------------------
bool_t Abstractor::_infer_back_comb_indices(abs_edge_pt abs_edge_ptr, int el, int irf, int orf){
  if (abs_edge_ptr ==nullptr){
    //CHECK(false)<<" null abs_edge_ptr";
    return false;
  }

  // Initiate a new timing ptr.
  timing_pt new_timing_ptr= abs_edge_ptr->insert_timing(el,irf,orf);

  auto & new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise() : new_timing_ptr->cell_fall();
  auto & new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition() : new_timing_ptr->fall_transition();

  float_t idx1_min = abs_edge_ptr->to_abs_node_ptr()->min_load(el, orf);
  float_t idx1_max = abs_edge_ptr->to_abs_node_ptr()->max_load(el, orf);

  float_t idx2_min = abs_edge_ptr->from_abs_node_ptr()->min_slew(el, irf);
  float_t idx2_max = abs_edge_ptr->from_abs_node_ptr()->max_slew(el, orf);

  abs_edge_pt load_edge = nullptr;
  abs_edge_pt slew_edge = nullptr;
  abs_edge_pt tmp = nullptr;
  if(!abs_edge_ptr->to_abs_node_ptr()->same_load()){
    switch(abs_edge_ptr->type()){
      case AbsEdgeType::BACK_TRACE_COMB:
        load_edge = abs_edge_ptr->first_abs_e_ptr();
      
        break;
      case AbsEdgeType::FORWARD_TRACE_COMB:
        tmp = abs_edge_ptr->first_abs_e_ptr();
        while(tmp->to_abs_node_ptr() != abs_edge_ptr->to_abs_node_ptr()){
          //CHECK(tmp->to_abs_node_ptr()->num_fanouts() == 1);
          tmp = tmp->to_abs_node_ptr()->fanout().head()->item();
        }
        load_edge = tmp;
        break;
      default:
        //CHECK(false);
        break;
      }
  }

  if(!abs_edge_ptr->to_abs_node_ptr()->same_slew()){
    switch(abs_edge_ptr->type()){
      case AbsEdgeType::BACK_TRACE_COMB:
        tmp = abs_edge_ptr->first_abs_e_ptr();
        while(tmp->from_abs_node_ptr() != abs_edge_ptr->from_abs_node_ptr()){
          //CHECK(tmp->from_abs_node_ptr()->num_fanins() == 1);
          tmp = tmp->from_abs_node_ptr()->fanin().head()->item();
        }
        slew_edge = tmp; 
        break;
      case AbsEdgeType::FORWARD_TRACE_COMB:
        slew_edge = abs_edge_ptr->first_abs_e_ptr();
        break;
      default:
        //CHECK(false);
        break;
    }
  }

  if( abs_edge_ptr->to_abs_node_ptr()->same_load()){
    float_t load = (idx1_min + idx1_max) / OT_FLT_TWO;
    new_dlut.indices1().push_back(load);
    new_tlut.indices1().push_back(load);
  }else{
    if(load_edge == nullptr){
      LOG(WARNING)<<"load_edge == nullptr";
      return false;
    }

    int olrf = (load_edge->timing_sense() == POSITIVE_UNATE)? orf : !orf;
    timing_pt old_timing_ptr = load_edge->timing_ptr(el,irf,olrf);
    auto & old_dlut = (olrf == RISE) ? old_timing_ptr->cell_rise() : old_timing_ptr->cell_fall();
    auto & old_tlut = (olrf == RISE) ? old_timing_ptr->rise_transition() : old_timing_ptr->fall_transition();

    new_dlut.indices1() = old_dlut.indices1() ;
    new_tlut.indices1() = old_tlut.indices1() ;
  }
  
  if( abs_edge_ptr->from_abs_node_ptr()->same_slew()){
    float_t slew_in = (idx2_min + idx2_max)/OT_FLT_TWO;
    new_dlut.indices2().push_back(slew_in);
    new_tlut.indices2().push_back(slew_in);
  }else{
    //CHECK(slew_edge != nullptr);
   
    int isrf = (slew_edge->timing_sense() == POSITIVE_UNATE)? irf : !irf;
    timing_pt old_timing_ptr = slew_edge->timing_ptr(el,isrf,orf);
    auto & old_dlut = (isrf == RISE) ? old_timing_ptr->cell_rise() : old_timing_ptr->cell_fall();
    auto & old_tlut = (isrf == RISE) ? old_timing_ptr->rise_transition() : old_timing_ptr->fall_transition();
    new_dlut.indices2() = old_dlut.indices2() ;
    new_tlut.indices2() = old_tlut.indices2() ;

  }
  
  new_dlut.resize(new_dlut.size1(),new_dlut.size2());
  new_tlut.resize(new_tlut.size1(),new_tlut.size2());

  return true;

}
// Function: _infer_indices
// case 1
//   rc      cellarc                 cell |  po(rc)
// o ------ o --------- o ------- o --------- o
// from     slew_e                          to
//
// slew_e: first cell arc
// load_e: last cell arc
//
// case 2: only RCTREE
//
// o --------- o
// from        to
//             
//                 slew
//                 load

bool_t Abstractor::_infer_indices(abs_edge_pt abs_edge_ptr, edge_pt slew_e, edge_pt load_e, int el, int irf, int orf) {

  if (abs_edge_ptr ==nullptr || slew_e == nullptr|| load_e==nullptr) {
    LOG(WARNING)<<" null abs_edge_ptr || null= slew_e ||  null load_e";
    return false ;
  }

  int leirf = (load_e->timing_sense() == POSITIVE_UNATE) ? orf : !orf;
  int seorf = (slew_e->timing_sense() == POSITIVE_UNATE) ? irf : !irf;

  timing_pt slew_timing_ptr = slew_e->timing_ptr(el,irf,seorf);
  timing_pt load_timing_ptr = load_e->timing_ptr(el,leirf,orf);
  if(load_e->timing_sense()==NON_UNATE && load_timing_ptr==nullptr){
    
    load_timing_ptr = load_e->timing_ptr(el,!leirf,orf);
  }
  if(slew_e->timing_sense() ==NON_UNATE && slew_timing_ptr ==nullptr){
    slew_timing_ptr = slew_e->timing_ptr(el,!irf,seorf);
    if(load_e==slew_e && slew_timing_ptr ==nullptr ){
      slew_timing_ptr = load_timing_ptr;
    }
  }


  if (slew_timing_ptr == nullptr|| load_timing_ptr==nullptr) {
   
    
        if(slew_timing_ptr == nullptr)
          LOG(INFO)<<" slew_timing_ptr == nullptr"<<endl
          <<" no timing on slew:"<<slew_e->from_node_ptr()->pin_ptr()->name()<<" -> "<< slew_e->to_node_ptr()->pin_ptr()->name()<<" irf:"<<irf<<" seorf:"<<seorf<<endl;

        if (load_timing_ptr==nullptr )
          LOG(INFO)<< "load_timing_ptr==nullptr"<<endl
          <<" no timing on load:"<<load_e->from_node_ptr()->pin_ptr()->name()<<" -> "<< load_e->to_node_ptr()->pin_ptr()->name()<<" leirf:"<<leirf<<" orf:"<<orf<<endl;
    
    return false ;

  }


  // Initiate a new timing ptr.
  timing_pt new_timing_ptr= abs_edge_ptr->insert_timing(el,irf,orf);

  //LOG(INFO)<<"          Start infer indeces";
  auto & new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise() : new_timing_ptr->cell_fall();
  auto & new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition() : new_timing_ptr->fall_transition();

  auto & old_s_dlut = (seorf == RISE) ? slew_timing_ptr->cell_rise() : slew_timing_ptr->cell_fall();
  auto & old_s_tlut = (seorf == RISE) ? slew_timing_ptr->rise_transition() : slew_timing_ptr->fall_transition();

  auto & old_l_dlut = (orf == RISE) ? load_timing_ptr->cell_rise() : load_timing_ptr->cell_fall();
  auto & old_l_tlut = (orf == RISE) ? load_timing_ptr->rise_transition() : load_timing_ptr->fall_transition();

  float_t idx1_min;   // Could be load or slew
  float_t idx1_max;

  float_t idx2_min;   // slew
  float_t idx2_max;

  float_vt* old_didx1_ptr(nullptr);
  float_vt* old_didx2_ptr(nullptr);

  float_vt* old_tidx1_ptr(nullptr);
  float_vt* old_tidx2_ptr(nullptr);



  //set up load indces
  idx1_min = abs_edge_ptr->to_abs_node_ptr()->min_load(el,orf);
  idx1_max = abs_edge_ptr->to_abs_node_ptr()->max_load(el,orf);

  if(old_l_dlut.lut_template_ptr() != nullptr) {
    old_didx1_ptr = (old_l_dlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_l_dlut.indices1(): & old_l_dlut.indices2();
  }
  if(old_l_tlut.lut_template_ptr() != nullptr) {
    old_tidx1_ptr = (old_l_tlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_l_tlut.indices1(): & old_l_tlut.indices2();
  }

  // set up slew indices
  idx2_min = abs_edge_ptr->from_abs_node_ptr()->min_slew(el,irf);
  idx2_max = abs_edge_ptr->from_abs_node_ptr()->max_slew(el,irf);

  if(old_s_dlut.lut_template_ptr() != nullptr) {
    old_didx2_ptr = (old_s_dlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_dlut.indices2(): & old_s_dlut.indices1();
  }
  if(old_s_tlut.lut_template_ptr() != nullptr) {
    old_tidx2_ptr = (old_s_tlut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_tlut.indices2(): & old_s_tlut.indices1();
  }


  // assign load indices

  // load_e != last edge
  net_pt net_ptr= load_e->to_node_ptr()->pin_ptr()->net_ptr();
 
  //CHECK(load_e->type()!=RCTREE_EDGE_TYPE);
  if(load_e->to_node_ptr() != abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr() &&  net_ptr!=nullptr && net_ptr->rctree_ptr()!=nullptr ) {
    rctree_pt rctree_ptr = net_ptr->rctree_ptr();
    
    if (fabs(idx1_max - idx1_min) <= OT_FLT_ONE) {
      new_dlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
      new_tlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
    } 
    else{
      float_t rcl=0;
      //CHECK(rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name())!=nullptr)<< rctree_ptr->root_ptr()->name()<<" "<< abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name();
      //if( rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name())!=nullptr) {
        rcl= rctree_ptr->load(el,orf) - rctree_ptr->rctree_node_ptr(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name())->load(el,orf);
      //}
      auto indices = _infer_range_indices((old_didx1_ptr),idx1_min+rcl ,idx1_max+rcl);
      
        for(unsigned_t i=0;i< indices.size();++i ){
          indices[i] = indices[i]-rctree_ptr->load(el,orf);
        }
      
      new_dlut.indices1() = indices;
      new_tlut.indices1() = indices;
    }

  }
  //load_e == last edge
  else {
    if (fabs(idx1_max - idx1_min) <= OT_FLT_ONE) {
      new_dlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
      new_tlut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);

    } else {
      new_dlut.indices1() = _infer_range_indices((old_didx1_ptr),idx1_min,idx1_max);
      new_tlut.indices1() = _infer_range_indices((old_tidx1_ptr),idx1_min,idx1_max);
    }

  }


  // slew indices
  // decided != from abs node
  net_ptr= abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->net_ptr();
  if (abs_edge_ptr->slew_e_ptr()->from_node_ptr()!= abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr() && net_ptr!=nullptr && net_ptr->rctree_ptr()!=nullptr ) {


    rctree_pt rctree_ptr = net_ptr->rctree_ptr();
    rctree_node_pt rctree_node_ptr = rctree_ptr->rctree_node_ptr(abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->name());

    if (fabs(idx2_max - idx2_min) <= OT_FLT_ONE) {

      //idx2 min max is abs_edge from node slew min mas
      // because rctree is always positive unate => leave node rf = irf

      float_t slew_in = (idx2_min + idx2_max)/OT_FLT_TWO;
      new_dlut.indices2().push_back(slew_in);
      new_tlut.indices2().push_back(slew_in);

      CHECK(std::isnan(slew_in)==0);
    } 
    else{
      idx2_min=rctree_ptr->slew(rctree_node_ptr,el,irf,idx2_min);
      idx2_max=rctree_ptr->slew(rctree_node_ptr,el,irf,idx2_max);

      auto indeces = _infer_range_indices((old_didx2_ptr),idx2_min,idx2_max);



      for(unsigned_t i=0; i< indeces.size(); ++i ) {
        //float_t slew_out = indeces[i];
        //float_t slew_in = sqrt(indeces[i]*indeces[i]- rctree_node_ptr->impulse(el,irf));
        //CHECK(fabs(slew_out - rctree_ptr->slew(rctree_node_ptr,el,irf,slew_in))<0.0001f)
        //    <<"slew_out ori "<< slew_out<<" der "<< rctree_ptr->slew(rctree_node_ptr,el,irf,slew_in);
        indeces[i] = sqrt(fabs(indeces[i]*indeces[i]- rctree_node_ptr->impulse(el,irf)));
        CHECK(std::isnan(indeces[i])==0);
      }
      new_dlut.indices2() = indeces;
      new_tlut.indices2() = indeces;
    }
  }
  // decided = from abs node
  else {
    if (fabs(idx2_max - idx2_min) <= OT_FLT_ONE) {
      new_dlut.indices2().push_back((idx2_min + idx2_max) / OT_FLT_TWO);
      new_tlut.indices2().push_back((idx2_min + idx2_max) / OT_FLT_TWO);
    } else {
      new_dlut.indices2() = _infer_range_indices((old_didx2_ptr),idx2_min,idx2_max);
      new_tlut.indices2() = _infer_range_indices((old_tidx2_ptr),idx2_min,idx2_max);
    }
  }
  //CHECK(!new_dlut.indices1().empty() && !new_dlut.indices2().empty());
  //CHECK(!new_tlut.indices1().empty() && !new_tlut.indices2().empty());


  new_dlut.resize(new_dlut.size1(),new_dlut.size2());
  new_tlut.resize(new_tlut.size1(),new_tlut.size2());



  return true;

}



// Procedure: _initiate_clock_constraint_timing
// irf: new timing  irf
// orf: new timing orf
void_t Abstractor::_initiate_clock_constraint_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to initiate constraint timing (nullptr exception)";
    return;
  }

  
  
  edge_pt slew_e=nullptr;
  edge_pt const_e=nullptr;
  //node_pt to_node_ptr = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  bool_t success=false;

  slew_e =  abs_edge_ptr->slew_e_ptr();
  for(auto const & item:abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->fanin()){
    if(item->type() == CONSTRAINT_EDGE_TYPE){
      const_e=item;
    }
  }
  // infer timing
  success=_infer_clock_const_indices(abs_edge_ptr, slew_e, const_e, el, irf, orf);



  if (success){
    auto new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);
    //CHECK(new_timing_ptr != nullptr);


    new_timing_ptr->set_timing_sense(NON_UNATE);


    //new_timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
    if(abs_edge_ptr->timing_sense()==POSITIVE_UNATE){
      auto old_timing_ptr = const_e->timing_arc_ptr(el)->timing_ptr(irf,orf);
      if(old_timing_ptr==nullptr){
        //LOG(WANRING)<< "old_timing_ptr==nullptr";
        return;
      }
      new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
    }
    else{
      auto old_timing_ptr = const_e->timing_arc_ptr(el)->timing_ptr(!irf,orf);
      if(old_timing_ptr==nullptr){
        //LOG(WANRING)<< "old_timing_ptr==nullptr";
        return;
      }
      //CHECK(old_timing_ptr!=nullptr);

      switch(old_timing_ptr->timing_type()){
        case TimingType::HOLD_RISING:
          new_timing_ptr->set_timing_type(TimingType::HOLD_FALLING);
        break;
        case TimingType::HOLD_FALLING:
          new_timing_ptr->set_timing_type(TimingType::HOLD_RISING);
        break;
        case TimingType::SETUP_RISING:
          new_timing_ptr->set_timing_type(TimingType::SETUP_FALLING);
        break;
        case TimingType::SETUP_FALLING:
          new_timing_ptr->set_timing_type(TimingType::SETUP_RISING);
        break;
        default:
          CHECK(false);
        break; 
      }
      //new_timing_ptr->set_timing_sense(timing_type);
    } 
    //CHECK(old_timing_ptr->timing_type() != TimingType::UNDEFINED);
  }
  else {
    LOG(ERROR)<<"QQ" << abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name() <<" ->"<<abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name()<<"  "
    <<abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->name()<<" "<<el<<irf<<orf;

  }
}

bool_t Abstractor::_infer_clock_const_indices(abs_edge_pt abs_edge_ptr,edge_pt slew_e, edge_pt const_e, int el, int irf, int orf) {

  if(abs_edge_ptr == nullptr|| slew_e==nullptr|| const_e==nullptr){
    LOG(ERROR) << "Fail to initiate constraint timing (nullptr exception)";
    return false ;
  }

  int ceirf = (abs_edge_ptr->timing_sense() == POSITIVE_UNATE) ? irf : !irf;
  int seorf = (slew_e->timing_sense() == POSITIVE_UNATE) ? irf : !irf;
 
  timing_pt slew_timing_ptr = slew_e->timing_ptr(el,irf,seorf);
  timing_pt const_timing_ptr = const_e->timing_ptr(el,ceirf,orf);

  if(slew_timing_ptr==nullptr){
    LOG(WARNING)<<"slew_timing_ptr==nullptr";
    return false ;
  }
  if(const_timing_ptr==nullptr){
    LOG(WARNING)<<"const_timing_ptr==nullptr";
    return false ;
  }

 
  timing_pt new_timing_ptr= abs_edge_ptr->insert_timing(el,irf,orf);
  
  
  auto & new_clut = (orf == RISE) ? new_timing_ptr->rise_constraint() : new_timing_ptr->fall_constraint();
  
  auto & old_s_clut =(slew_e == const_e)? ((seorf == RISE) ? slew_timing_ptr->rise_constraint() : slew_timing_ptr->fall_constraint()):((seorf == RISE) ? slew_timing_ptr->rise_transition() : slew_timing_ptr->fall_transition());
  auto & old_c_clut = ( orf== RISE) ? const_timing_ptr->rise_constraint() : const_timing_ptr->fall_constraint();
  
  float_t idx1_min;
  float_t idx1_max;

  float_t idx2_min; 
  float_t idx2_max;

  float_vt* old_sidx2_ptr(nullptr);
  
  float_vt* old_cidx1_ptr(nullptr);


  // constraint clk(related) -> d
  // constraint_slew_slew
  // variable_1 : constrained_pin_transition;  (c) to
  // variable_2 : related_pin_transition; clk  (s) from


  idx1_min = abs_edge_ptr->to_abs_node_ptr()->min_slew(el,orf);
  idx1_max = abs_edge_ptr->to_abs_node_ptr()->max_slew(el,orf);

  idx2_min = abs_edge_ptr->from_abs_node_ptr()->min_slew(el,irf);
  idx2_max = abs_edge_ptr->from_abs_node_ptr()->max_slew(el,irf);

  if(old_c_clut.lut_template_ptr() != nullptr) {
    old_cidx1_ptr = (old_c_clut.lut_template_ptr()->variable1()==CONSTRAINED_PIN_TRANSITION)?
                    &old_c_clut.indices1(): & old_c_clut.indices2();
  }

  if(old_s_clut.lut_template_ptr() != nullptr) {
    old_sidx2_ptr = (old_s_clut.lut_template_ptr()->variable1()==CONSTRAINED_PIN_TRANSITION||old_s_clut.lut_template_ptr()->variable1()==TOTAL_OUTPUT_NET_CAPACITANCE)?
                    &old_s_clut.indices2(): & old_s_clut.indices1();
  }
   
  // assign index 1 (d pin)
  if(fabs(idx1_max - idx1_min) <= OT_FLT_ONE){
    new_clut.indices1().push_back((idx1_min + idx1_max) / OT_FLT_TWO);
    //CHECK((idx1_min + idx1_max) / OT_FLT_TWO>0)<< idx1_min<<" "<< idx1_max;

  }
  else {
    new_clut.indices1() = _infer_range_indices((old_cidx1_ptr),idx1_min,idx1_max);
  }

  //assign index 2 clk pin
  net_pt net_ptr= slew_e->from_node_ptr()->pin_ptr()->net_ptr();
  if (fabs(idx2_max - idx2_min) <= OT_FLT_ONE){
    new_clut.indices2().push_back((idx2_min + idx2_max) / OT_FLT_TWO);
    //CHECK((idx2_min + idx2_max) / OT_FLT_TWO>0);
  }
  else {
    rctree_pt rctree_ptr = net_ptr->rctree_ptr();


    if(slew_e->from_node_ptr() != abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr() && net_ptr!=nullptr &&  net_ptr->rctree_ptr()!=nullptr ){
    
      rctree_node_pt rctree_node_ptr = rctree_ptr->rctree_node_ptr(slew_e->from_node_ptr()->pin_ptr()->name());

      idx2_max=idx2_max*idx2_max-rctree_node_ptr->impulse(el,irf);
      idx2_min=idx2_min*idx2_min-rctree_node_ptr->impulse(el,irf);

      auto indices2 = _infer_range_indices((old_sidx2_ptr),idx2_min,idx2_max);
      
      for(unsigned_t i=0;i< indices2.size();++i ){
        indices2[i] = indices2[i]* indices2[i]-rctree_node_ptr->impulse(el,irf);
      }
      new_clut.indices2()=indices2;

    }
    else{
      new_clut.indices2()= _infer_range_indices((old_sidx2_ptr),idx2_min,idx2_max);
    }
  }

  new_clut.resize(new_clut.size1(),new_clut.size2());

  return true;
  
}
/*
// Procedure: _initiate_constraint_timing
void_t Abstractor::_initiate_constraint_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  if(abs_edge_ptr == nullptr || abs_edge_ptr->edge_ptr() == nullptr) {
    LOG(ERROR) << "Fail to initiate constraint timing (nullptr exception)";
    return;
  }

  auto old_timing_ptr = abs_edge_ptr->edge_ptr()->timing_ptr(el, irf, orf);

  if(old_timing_ptr == nullptr) return;

  _infer_indices(abs_edge_ptr, abs_edge_ptr->edge_ptr(), abs_edge_ptr->edge_ptr(), el, irf, orf);


  auto new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);

  //CHECK(new_timing_ptr != nullptr);

  new_timing_ptr->set_timing_sense(abs_edge_ptr->edge_ptr()->timing_arc_ptr(el)->timing_sense());

  //CHECK(old_timing_ptr->timing_type() != TimingType::UNDEFINED);
  new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
}
*/
void_t Abstractor::_initiate_po_seg_timing(abs_edge_pt abs_edge_ptr, edge_pt slew_e, int el, int irf , int orf) {
  if(abs_edge_ptr == nullptr ) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return;
  }
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  bool_t success = false ;

  success=_infer_po_seg_indices(abs_edge_ptr, slew_e, el, irf, orf);

  if(success) {
    auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);
    timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
    timing_ptr->set_timing_type(TimingType::COMBINATIONAL);

  } else {
    LOG(ERROR)<<"QQ " << tail->pin_ptr()->name() <<" ->"<<head->pin_ptr()->name()<<"  "<<slew_e->from_node_ptr()->pin_ptr()->name()<<" "<<el<<irf<<orf;

  }
}
void_t Abstractor::_initiate_cross_timing(abs_edge_pt abs_edge_ptr, int el, int irf , int orf, abs_edge_pt a, abs_edge_pt b) {
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return;
  }
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();

  //const auto decided = abs_edge_ptr->slew_e_ptr()->from_abs_node_pt->pin_ptr()->node_ptr();
  //if (decided == nullptr) CHECK(false);
  bool_t success = false ;
  success = _infer_cross_indices(abs_edge_ptr, el, irf, orf, a, b);

  
  //LOG(INFO)<<"         End infer indecies (decided == tail)";

  if(success) {
    auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);
    timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
   
    timing_ptr->set_timing_type(TimingType::COMBINATIONAL);

  } else {
    LOG(ERROR)<<"QQ " << tail->pin_ptr()->name() <<" ->"<<head->pin_ptr()->name()<<" "<<el<<irf<<orf;

  }
}
void_t Abstractor::_initiate_back_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf , int orf) {
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return;
  }
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();

  //const auto decided = abs_edge_ptr->slew_e_ptr()->from_abs_node_pt->pin_ptr()->node_ptr();
  //if (decided == nullptr) CHECK(false);
  bool_t success = false ;

  
  success=_infer_back_comb_indices(abs_edge_ptr, el, irf, orf);
  //LOG(INFO)<<"         End infer indecies (decided == tail)";

  if(success) {
    auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);
    timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
   
    timing_ptr->set_timing_type(TimingType::COMBINATIONAL);

  } else {
    LOG(ERROR)<<"QQ " << tail->pin_ptr()->name() <<" ->"<<head->pin_ptr()->name()<<" "<<el<<irf<<orf;

  }
}
void_t Abstractor::_initiate_comb_timing(abs_edge_pt abs_edge_ptr, edge_pt slew_e, edge_pt comb_e, int el, int irf , int orf) {
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return;
  }
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  //const auto decided = abs_edge_ptr->slew_e_ptr()->from_abs_node_pt->pin_ptr()->node_ptr();
  //if (decided == nullptr) CHECK(false);
  bool_t success = false ;

  success=_infer_indices(abs_edge_ptr, slew_e, comb_e, el, irf, orf);
  //LOG(INFO)<<"         End infer indecies (decided == tail)";

  if(success) {


    auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);

    timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);

    timing_ptr->set_timing_type(TimingType::COMBINATIONAL);

  } else {
    LOG(ERROR)<<"QQ " << tail->pin_ptr()->name() <<" ->"<<head->pin_ptr()->name()<<"  "<<slew_e->from_node_ptr()->pin_ptr()->name()<<" "<<el<<irf<<orf;

  }
}
/*
// Procedure: _initiate_jump_timing
void_t Abstractor::_initiate_jump_timing(abs_edge_pt abs_edge_ptr, int el, int irf , int orf) {

  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return;
  }

  // Get the tail and head of the path.
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  auto from_node_ptr = tail;
  auto from_rf = irf;
  bool_t success = false ;


  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != head && success == false) {

    // Sanity check. The path propagation can't meet a branching pin.
    if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      break;
    }

    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.
    auto e = from_node_ptr->fanout().head()->item();
    success = _infer_indices(abs_edge_ptr, e, e, el, from_rf, orf);


    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
  }

  if(!success) {
    _initiate_load_slew_timing_log(abs_edge_ptr, el, irf, orf);
  }

  auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);

  //CHECK(timing_ptr != nullptr);

  timing_ptr->set_timing_sense(irf == orf ? POSITIVE_UNATE : NEGATIVE_UNATE);
  timing_ptr->set_timing_type(TimingType::COMBINATIONAL);
}
*/

bool_t Abstractor::_repeated(vector<node_pt> & record, node_pt v){
 
  for(const auto & item :record ){
    if(v == item){
      return true;
    }
  }
  return false;
}
void_t Abstractor::_annotate_po_q(bool_t* po ){
  queue <node_pt> que;
  node_pt u;
  node_pt v;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  for(const auto& item : timer_ptr()->circuit_ptr()->primary_output_dict()) {
    _insert_abs_node(item.second->pin_ptr());
    que.push(item.second->pin_ptr()->node_ptr());
    visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
  }
  while (!que.empty()) {
    u = que.front();
    que.pop();
    for(const auto& e : u->fanin()) {
      v = e->from_node_ptr();
      if(v->is_clock_sink()){
        po[v->idx()] = true;
        

        visited[v->idx()] = true;
      }
      else if(visited[v->idx()] == false ) {   
        //CHECK(!v->is_clock_sink());
        que.push(v);
        visited[v->idx()] = true;
      } 
      
    }
  }
  delete [] visited;
}

void_t Abstractor::_annotate_pi_d(bool_t* pi, bool_t* po){

  queue <node_pt> que;
  unordered_set<node_pt> backlist;
  node_pt u;
  node_pt v;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  auto back_visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(back_visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  for(const auto& item : timer_ptr()->circuit_ptr()->primary_input_dict()) {
    if(item.second->pin_ptr() != timer_ptr()->circuit_ptr()->clock_tree_root_pin_ptr()){
      _insert_abs_node(item.second->pin_ptr());
      que.push(item.second->pin_ptr()->node_ptr());
      visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
      back_visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
      
    }
  }
  while (!que.empty()) {
    u = que.front();
    que.pop();
    if(u->is_constrained() ){
      pi[u->idx()]=true;
      backlist.insert(u);
      back_visited[u->idx()]=true;

      

      //CHECK(u->num_fanouts() == 0);
    }
    bool_t back_tr = true; 
    for(const auto& e : u->fanin()){
      if(back_visited[e->from_node_ptr()->idx()]==false)
        back_tr = false; 
    }
    if( back_tr ){
      back_visited[u->idx()]=true;

    }
    
    for(const auto& e : u->fanout()) {
      v = e->to_node_ptr();
      if(visited[v->idx()] == false) {   
        que.push(v);
        visited[v->idx()] = true;
      } 
    }
  }
  delete [] visited ;  

  size_t po_size = 0;
  for(auto it = backlist.begin(); it !=  backlist.end(); ++it){

    queue <node_pt> back;
    back.push(*(it));
    while (!back.empty()) {
      u = back.front();
      back.pop();
      for(const auto& e : u->fanin()) {
       
        v = e->from_node_ptr();
        if(e->type() !=CONSTRAINT_EDGE_TYPE){
          if(u->is_clock_sink()){
            po[u->idx()]=true;
            ++po_size;
          } 
          else if(back_visited[v->idx()] == false) {
            back.push(v);
            back_visited[v->idx()] = true;
          }
        }
      }
    }
  }
  cout<<"num of d (test) = "<< backlist.size()<<endl;
  cout<<"num of q = "<<po_size<<endl;
  delete [] back_visited;

}

void_t Abstractor::_initiate_graph_to_io() {

  LOG(INFO) << "Initiating the abstraction graph ...";

  timer_ptr()->update_timing();
  
  // Declare the data storage.
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  auto po_q = new bool_t[timer_ptr()->nodeset().num_indices()];
  auto pi_d = new bool_t[timer_ptr()->nodeset().num_indices()];

  queue<node_pt> que;
  node_pt u;
  node_pt v;
  // Insert an abs node for every node in the nodeset and push the node with zero
  // fanins into the frontier queue.

  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  memset(po_q, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  memset(pi_d, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
 
  _annotate_pi_d(pi_d, po_q);
  _annotate_po_q(po_q);
  // Insert an abs node for every primary input and primary output.


  for(const auto& item : timer_ptr()->circuit_ptr()->primary_input_dict()) {
    //if(item.second->pin_ptr() != timer_ptr()->circuit_ptr()->clock_tree_root_pin_ptr()){
      _insert_abs_node(item.second->pin_ptr());
      que.push(item.second->pin_ptr()->node_ptr());
      visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
    //}
  }
  for(const auto& item : timer_ptr()->circuit_ptr()->primary_output_dict()) {
    _insert_abs_node(item.second->pin_ptr());
  }

/*
  for (auto& ptr : timer_ptr()->nodeset()) {
    if(ptr->num_fanins() == 0) {
      que.push(ptr);
      visited[ptr->idx()] = true;
    }
  }
*/


  while (!que.empty()) {
    u = que.front();
    que.pop();
    vector<node_pt> record;
    for(const auto& e : u->fanout()) {
      int rf_po_v = RISE;      
      int rf_v = RISE;
      node_pt v_po = u;
      edge_pt slew_e = nullptr;
      int num_non = 0;
      v = e->to_node_ptr();
      rf_v = (e->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
      if (e->type() ==COMBINATIONAL_EDGE_TYPE||e->type() ==CONSTRAINT_EDGE_TYPE){
        slew_e=e;
      }

      if(e->timing_sense()==NON_UNATE){
        ++num_non;
      }

      while(v->num_fanouts()==1) {
        v_po = v;
        if(slew_e==nullptr && (v->fanout().head()->item()->type()== COMBINATIONAL_EDGE_TYPE || v->fanout().head()->item()->type()==CONSTRAINT_EDGE_TYPE) ) {
          slew_e = v->fanout().head()->item();
        }
        rf_v = (v->fanout().head()->item()->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
        if( v->fanout().head()->item()!=nullptr && v->fanout().head()->item()->timing_sense()==NON_UNATE){
          ++num_non;
        }
        v = v->fanout().head()->item()->to_node_ptr();
      }
      node_pt decided_node_ptr =nullptr;
      if(slew_e!=nullptr) {
        decided_node_ptr = slew_e->from_node_ptr();
      }
      // CLOCK
      if(v->is_clock_sink()) {
         // COMBINATIONAL in clock tree
        if(decided_node_ptr!=nullptr) {
          timing_sense_ce timing_sence=(rf_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;
          switch (num_non){
            case 0: // unate
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
              break;
            default: // non unate
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
              break;
          }
        }
        // RCTREE PATH in clock tree
        else {
          _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
        }
        // for all ends nodes of clock tree push into queue
        if (visited[v->idx()]==false) {
           que.push(v);
           visited[v->idx()]=true;
        }
      } // clock tree end

      // Constraints
      else if(slew_e!=nullptr && slew_e->type() == CONSTRAINT_EDGE_TYPE ){
        if(pi_d[ v->idx() ] == true){
         _insert_abs_edge(u->pin_ptr(),v->pin_ptr() , slew_e ,AbsEdgeType::CLOCK_CONSTRAINT, POSITIVE_UNATE, 1);    
        }

      }
      //PO segment u -------> v (PO) and v's num of fanout > 1
      else if (v->num_fanouts()> 1 && v->pin_ptr()->net_ptr() && v->pin_ptr()->net_ptr()->has_primary_output() ) {
        timing_sense_ce timing_sence=(rf_po_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;
        if(slew_e!=nullptr && u != v_po && slew_e->from_node_ptr() != v_po){
          // NON UNATE 
          if(v_po->fanin().head()->item()->timing_sense()==NON_UNATE){
            
            if(_repeated(record, v_po)){
              switch (num_non){
                case 1:
                case 0:
                  _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(),nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                  _insert_abs_edge(decided_node_ptr->pin_ptr(),v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non-1);
                  break;
                default:
                  //CHECK(num_non>1);
                  _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                  _insert_abs_edge( decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non-1);
                  break;
              }
            }
            else {
              switch (num_non){
                case 1:
                case 0:
                  _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non-1);
                  record.push_back(v_po);
                  break;
                default:
                  //CHECK(num_non>1);
                  _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non-1);
                  record.push_back(v_po);
                  break;
              }
            }
          }
          else{ 
            
            if(_repeated(record, v_po)){
              switch (num_non){
                case 0:
                  _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                  _insert_abs_edge(decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
                  break;
                default:
                 // CHECK(num_non>0);
                  _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                  _insert_abs_edge(decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
                  break;
              }
            }
            else {
              switch (num_non){
                case 0:
                  _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
                  record.push_back(v_po);
                  break;
                default:
                  //CHECK(num_non>0);
                  _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
                  record.push_back(v_po);
                  break;
              }
            }
          }
        }
        else if (u!=v_po ) {
          _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
        }  
        for (const auto & edge : v->fanout()) {
          auto to_node_ptr = edge->to_node_ptr();

          timing_sense_ce timing_sence=(rf_v==rf_po_v)? POSITIVE_UNATE : NEGATIVE_UNATE;
//          CHECK(v_po->num_fanouts()==1);
          edge_pt cell_edge =nullptr;
          for(const auto ed: v_po->fanout()  ){
            if(ed->to_node_ptr() ==v){
              cell_edge =ed;
            }
          }
          //CHECK(cell_edge!=nullptr);
          
          //CHECK(cell_edge->type() == COMBINATIONAL_EDGE_TYPE||cell_edge->type() ==CONSTRAINT_EDGE_TYPE );

          if(cell_edge->timing_sense() == NON_UNATE){
            //_insert_abs_edge(v_po->pin_ptr(),v->pin_ptr() , cell_edge ,AbsEdgeType::COMBINATIONAL, NON_UNATE,1);
            //_insert_abs_edge(v->pin_ptr() , to_node_ptr->pin_ptr() ,nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
            _insert_abs_edge(v_po->pin_ptr(),to_node_ptr->pin_ptr() , cell_edge ,AbsEdgeType::PO_SEGMENT, NON_UNATE,1);
          
          }
          else {
            //_insert_abs_edge(v_po->pin_ptr(),v->pin_ptr() , cell_edge ,AbsEdgeType::COMBINATIONAL, timing_sence,0);
            //_insert_abs_edge(v->pin_ptr() , to_node_ptr->pin_ptr() ,nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
            _insert_abs_edge(v_po->pin_ptr(),to_node_ptr->pin_ptr() , cell_edge,AbsEdgeType::PO_SEGMENT, timing_sence,0);
          }

          if(visited[to_node_ptr->idx()]==false) {
            que.push(to_node_ptr);
            visited[to_node_ptr->idx()]=true;
          }
        }
      } // PO SEG end

      // others
      else {
        //COMBINATIONAL
        if(decided_node_ptr!=nullptr) {
          timing_sense_ce timing_sence=(rf_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;
          if(_repeated(record, v)){

            switch (num_non){
              case 0:
                _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(),nullptr,  AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
                _insert_abs_edge(decided_node_ptr->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
                break;
              default:
                //CHECK(num_non>0);
                _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(),nullptr,  AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
                _insert_abs_edge(decided_node_ptr->pin_ptr(),v->pin_ptr(),slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
                break;
            }
          }
          else {
            switch (num_non){
              case 0:
                _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
                record.push_back(v);
                break;
              default:
                _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
                record.push_back(v);
                //CHECK(num_non>0);
                break;
            }
          }

        }
        // RCTREE PATH
        else {
          _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
          record.push_back(v);
          //CHECK(false);
        }
        if(visited[v->idx()] == false && v->num_fanouts() > 0) {
           
          if( !u->is_clock_sink() || po_q[u->idx()]){
            que.push(v);
            visited[v->idx()] = true;
          }
          /* else if (po_q[u->idx()]){
            que.push(v);
            visited[v->idx()] = true;
          }*/
          //else{
            //_insert_abs_edge(timer_ptr()->circuit_ptr()->clock_tree_ptr()->root_pin_ptr() , v->pin_ptr(), nullptr , AbsEdgeType::REQUIRED_CONSTRAINT, NON_UNATE,1);
          //}
        }
      }
    }
  }
  delete [] visited;
  delete [] po_q;
  delete [] pi_d;
  /*
  for(const auto &  ptr: abs_node_dict()){
    if(ptr.second->pin_ptr()->is_primary_output()){
      if( ptr.second->num_fanins() == 0 ){ 
        //LOG(ERROR)<< ptr.second->name();
        CHECK(false)<< ptr.second->name();

      }
    }
  }
*/
}

// cut forward orphan
size_t Abstractor::_cut_forward_orphan_edges(){
  
  abs_node_pt a;
  abs_node_pt b;
  queue<abs_node_pt> queabs;
  size_t si=0;

  for(const auto& item : abs_node_dict()) {
    if(item.second->num_fanins()==0 &&  !(item.second->pin_ptr()->is_primary_input()) && (item.second->num_fanouts()!=0)){
      queabs.push(item.second);
    }
    if(item.second->num_fanins()==0 && item.second->num_fanouts()==0 && !(item.second->pin_ptr()->is_primary_input()) && !(item.second->pin_ptr()->is_primary_output())){
      _remove_abs_node(item.second);
    }
  }
  si = queabs.size();
  while(!queabs.empty()){
    a = queabs.front();
    queabs.pop();
    vector<abs_edge_pt> remove;
    for( const auto & e: a->fanout()){
      b = e->to_abs_node_ptr();
      //CHECK(e->type() != AbsEdgeType::CLOCK_CONSTRAINT  );
      remove.push_back(e);
      if(b->num_fanins()==0 &&  !(b->pin_ptr()->is_primary_input())){
        queabs.push(b);
      }
    }

    for(size_t i=0;i<remove.size();++i){
      _remove_abs_edge(remove[i]);
    }
    if(a->num_fanouts()==0 && a->num_fanins()==0 && !a->pin_ptr()->is_primary_input() && !a->pin_ptr()->is_primary_output())
      _remove_abs_node(a);
  }
  return si;
}
size_t Abstractor::_cut_orphan_edges(){
//  LOG(INFO)<< "Start cut orphans";
  abs_node_pt a;
  abs_node_pt b;
  queue<abs_node_pt> queabs;
  
  size_t si=0;

  for(const auto& item : abs_node_dict()) {
    if(item.second->num_fanouts()==0 && !(item.second->pin_ptr()->is_primary_output()) && !(item.second->pin_ptr()->is_primary_input())
      
       ){
      if(item.second->num_fanins() == 1){
/*
        bool_t has_po = false;
        for(const auto & e :item.second->fanout()){
          if(e->to_abs_node_ptr()->pin_ptr()->net_ptr()!=nullptr){
            if(e->to_abs_node_ptr()->pin_ptr()->net_ptr()->has_primary_output()){
              has_po = true;
            }
          }
        }*/
//        if(has_po == false)
          queabs.push(item.second);    
      }else {
        bool_t has_const = false;
        for(const auto & e :item.second->fanin()){
          if(e->type() == AbsEdgeType::CLOCK_CONSTRAINT){
            has_const = true;
          }
        }
        /*
        for(const auto & e :item.second->fanout()){
          if(e->to_abs_node_ptr()->pin_ptr()->net_ptr()!=nullptr){
            if(e->to_abs_node_ptr()->pin_ptr()->net_ptr()->has_primary_output()){
              has_const = true;
            }
          }
        }
        */
        if(has_const == false ){
          queabs.push(item.second);
        }
      }
    }
  }
  si = queabs.size();
  while(!queabs.empty()){
    a = queabs.front();
    queabs.pop();
    //CHECK(a->num_fanins() == 1);
    vector<abs_edge_pt> remove;
    for(const auto & e :a->fanin()){
      //abs_edge_pt e = a->fanin().head()->item();
      b = e->from_abs_node_ptr();

      //CHECK(e->type() != AbsEdgeType::CLOCK_CONSTRAINT );

      remove.push_back(e);
      //_remove_abs_edge(e);
      //if(a->num_fanouts()==0 && a->num_fanins()==0)
      //  _remove_abs_node(a);
      if(b->num_fanouts()==0 && !(b->pin_ptr()->is_primary_output()) ){
        if(b->num_fanins() == 1)
          queabs.push(b);
        else{
          bool_t has_const = false;
          for(const auto & ee :b->fanin()){
            if(ee->type() == AbsEdgeType::CLOCK_CONSTRAINT){
              has_const = true;
            }
          }
          if(has_const == false ){
            queabs.push(b);
          }

        }
      }
      
    }
    for(size_t i=0;i<remove.size();++i){
      _remove_abs_edge(remove[i]);
    }
    if(a->num_fanouts()==0 && a->num_fanins()==0 && !(a->pin_ptr()->is_primary_input()))
      _remove_abs_node(a);
  }
  return si;

  

//  LOG(INFO)<< "Successfully cut orphans";
}
void_t Abstractor::_initiate_graph_to_min() {

  LOG(INFO) << "Initiating the abstraction graph ...";

  timer_ptr()->update_timing();
  // Insert an abs node for every primary input and primary output.
  for(const auto& item : timer_ptr()->circuit_ptr()->primary_input_dict()) {
    _insert_abs_node(item.second->pin_ptr());
  }
  for(const auto& item : timer_ptr()->circuit_ptr()->primary_output_dict()) {
    _insert_abs_node(item.second->pin_ptr());
  }

  // Declare the data storage.
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  queue <node_pt> que;
  node_pt u;
  node_pt v;
  // Insert an abs node for every node in the nodeset and push the node with zero
  // fanins into the frontier queue.
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  for (auto& ptr : timer_ptr()->nodeset()) {
    if(ptr->num_fanins() == 0) {
      que.push(ptr);
      visited[ptr->idx()] = true;
    }
  }

  while (!que.empty()) {
    u = que.front();
    que.pop();
 
    vector<node_pt> record;
    for(const auto& e : u->fanout()) {
      int rf_po_v = RISE;      
      int rf_v=RISE;
      edge_pt slew_e=nullptr;
      node_pt v_po=u;
      int num_non=0;
      v = e->to_node_ptr();
      rf_v = (e->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
      if (e->type() ==COMBINATIONAL_EDGE_TYPE||e->type() ==CONSTRAINT_EDGE_TYPE){
        slew_e=e;
      }

      if(e->timing_sense()==NON_UNATE){
        ++num_non;
      }

      while(v->num_fanouts()==1) {
        v_po = v;
        rf_po_v = rf_v;

        if(slew_e==nullptr && (v->fanout().head()->item()->type()== COMBINATIONAL_EDGE_TYPE || v->fanout().head()->item()->type()==CONSTRAINT_EDGE_TYPE) ) {
          slew_e = v->fanout().head()->item();
        }
        
        rf_v = (v->fanout().head()->item()->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;

        if( v->fanout().head()->item()!=nullptr && v->fanout().head()->item()->timing_sense()==NON_UNATE){
          ++num_non;
        }

        v = v->fanout().head()->item()->to_node_ptr();

      }





      node_pt decided_node_ptr =nullptr;

      if(slew_e!=nullptr) decided_node_ptr = slew_e->from_node_ptr();
      




      //CLOCK
      if(v->is_clock_sink()) {
         //COMBINATIONAL
        if(decided_node_ptr!=nullptr) {
          timing_sense_ce timing_sence=(rf_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;
          if (num_non>0){
            if(_repeated(record, v)){
              _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0 );             
              _insert_abs_edge(decided_node_ptr->pin_ptr(), v->pin_ptr(), slew_e, AbsEdgeType:: COMBINATIONAL, NON_UNATE,num_non ); 
            } 
            else{
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
              record.push_back(v);
            }
          }      
          else {
            if(_repeated(record, v)){
              _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
              _insert_abs_edge(decided_node_ptr->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
            }
            else{
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
              record.push_back(v);
            }

          }
        }
        // RCTREE PATH
        else {
          _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
        }
        if (visited[v->idx()]==false) {
           que.push(v);
           visited[v->idx()]=true;

         }

      }
      else if(slew_e!=nullptr && slew_e->type() == CONSTRAINT_EDGE_TYPE){
         _insert_abs_edge(u->pin_ptr(),v->pin_ptr() , slew_e ,AbsEdgeType::CLOCK_CONSTRAINT, POSITIVE_UNATE, 1);    
      } 
      //PO segment
      else if (v->num_fanouts()> 1 && v->pin_ptr()->net_ptr() && v->pin_ptr()->net_ptr()->has_primary_output() ) {

        timing_sense_ce timing_sence=(rf_po_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;

        if(slew_e!=nullptr && u != v_po && slew_e->from_node_ptr() != v_po){


          if(v_po->fanin().head()->item()->timing_sense()==NON_UNATE){
            if (num_non>1){
              if(_repeated(record, v_po)){
                _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                _insert_abs_edge( decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non-1);
              }
              else{
                _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non-1);
                record.push_back(v_po);
              }
            }
            else {
              if(_repeated(record, v_po)){
                _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(),nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                _insert_abs_edge(decided_node_ptr->pin_ptr(),v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non-1);

              }
              else {
                _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non-1);
                record.push_back(v_po);
              }
            }
          }
          else{ 
            if (num_non>0){
              if(_repeated(record, v_po)){
                _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                _insert_abs_edge(decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
            
              }
              else {
                _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
                record.push_back(v_po);
              }
            }
            else {
              if(_repeated(record, v_po)){
                _insert_abs_edge(u->pin_ptr(), decided_node_ptr->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE,0);
                _insert_abs_edge(decided_node_ptr->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
              }
              else {
                _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
                record.push_back(v_po);
              }
            }
          }
        }
        else if (u!=v_po ) {
          _insert_abs_edge(u->pin_ptr(), v_po->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
        }  
        for (const auto & edge : v->fanout()) {
          auto to_node_ptr = edge->to_node_ptr();

          timing_sense_ce timing_sence=(rf_v==rf_po_v)? POSITIVE_UNATE : NEGATIVE_UNATE;

//          CHECK(v_po->num_fanouts()==1);
          edge_pt cell_edge =nullptr;
          for(const auto eee: v_po->fanout()  ){
            if(eee->to_node_ptr() ==v){
              cell_edge =eee;
            }
          }
          //CHECK(cell_edge!=nullptr);
//          edge_pt cell_edge = v_po->fanout().head()->item();

          //CHECK(cell_edge->type() == COMBINATIONAL_EDGE_TYPE||cell_edge->type() ==CONSTRAINT_EDGE_TYPE );

          if(cell_edge->timing_sense() == NON_UNATE){
            _insert_abs_edge(v_po->pin_ptr(),to_node_ptr->pin_ptr() , cell_edge ,AbsEdgeType::PO_SEGMENT, NON_UNATE,1);
          
          }
          else {
            _insert_abs_edge(v_po->pin_ptr(),to_node_ptr->pin_ptr() , cell_edge,AbsEdgeType::PO_SEGMENT, timing_sence,0);
          }

          if(visited[to_node_ptr->idx()]==false) {
            que.push(to_node_ptr);
            visited[to_node_ptr->idx()]=true;
          }

        }

      }
      // others
      else {
        //COMBINATIONAL
        if(decided_node_ptr!=nullptr) {
          timing_sense_ce timing_sence=(rf_v==RISE)? POSITIVE_UNATE : NEGATIVE_UNATE;
          if (num_non>0){
            if (_repeated(record, v)){
              _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(),nullptr,  AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
              _insert_abs_edge(decided_node_ptr->pin_ptr(),v->pin_ptr(),slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
            }
            else {
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, NON_UNATE, num_non);
              record.push_back(v);
            }
          }      
          else {
            if(_repeated(record, v)){
              _insert_abs_edge(u->pin_ptr(),decided_node_ptr->pin_ptr(),nullptr,  AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
              _insert_abs_edge(decided_node_ptr->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
            }
            else {
              _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), slew_e, AbsEdgeType::COMBINATIONAL, timing_sence, num_non);
              record.push_back(v);
            }
          }
        }
        // RCTREE PATH
        else {
          _insert_abs_edge(u->pin_ptr(),v->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH, POSITIVE_UNATE, 0);
          record.push_back(v);
          //CHECK(false);
        }
        if(visited[v->idx()]==false && v->num_fanouts()>0) {
          que.push(v);
          visited[v->idx()]=true;
        }
      }
    }

  }
  delete [] visited;



}
/*
// Function: _infer_ctiming
float_pair_t Abstractor::_infer_ctiming(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct slew, float_ct load) {

  if (abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the ctiming (nullptr exception)";
    return float_pair_t(slew, OT_FLT_ZERO);
  }

  auto e = abs_edge_ptr->edge_ptr();
  auto dout = e->timing_arc_ptr(el)->delay(irf, orf, slew, load);
  auto sout = e->timing_arc_ptr(el)->slew(irf, orf, slew, load);

  return float_pair_t(sout, dout);
}

*/

// Function: _infer_comb_non_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_pair_t Abstractor::_infer_comb_non_timing_slew(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);
  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  

  

  // Get the tail and head of the path.
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto decided = abs_edge_ptr->slew_e_ptr()->from_node_ptr();

  auto from_node_ptr = tail;
  auto from_rf = irf;

  edge_pt first_non=nullptr;
  edge_pt last_non=nullptr;
  node_pt temp= decided;
  int trf=irf;
  //int first_from_rf;
  int last_to_rf=RISE;

  while(temp!=head){
    edge_pt e =nullptr ;
    if (temp == decided){
      e =  abs_edge_ptr->slew_e_ptr();
    }
    else {
      //CHECK(temp->num_fanouts()==1);
      e =temp->fanout().head()->item();
    }
    if( e->timing_sense()==NON_UNATE){
      last_non = e;  
      last_to_rf=RISE;
      if(first_non == nullptr){
        first_non = e;
//        first_from_rf=trf;
      }
    }
    trf =  (e->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
    if(e!=last_non)
      last_to_rf = (e->timing_sense() == POSITIVE_UNATE) ? last_to_rf : !last_to_rf ;
    temp = e->to_node_ptr();
  }
  
  last_to_rf=(last_to_rf==RISE)? orf: !orf;
  
  // 1st edge is rctree ----> therefore, will not be non_unate edge 
  if (from_node_ptr!=decided) {
    edge_pt e=nullptr;
    if(from_node_ptr == abs_edge_ptr->slew_e_ptr()->from_node_ptr()){
      e= abs_edge_ptr->slew_e_ptr();
    }
    else if( decided->num_fanins() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }
    else {
      e = decided->fanin().head()->item();
    }
    if(e->from_node_ptr()!=tail) {
      LOG(ERROR) << "Fail to infer timing on from abs node is 1 level earlier than decided abs node" +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }
    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    P = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);
    from_node_ptr=decided;
    from_rf = to_rf;
  }


  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != first_non->from_node_ptr()) {
    edge_pt e = nullptr;
    if( from_node_ptr == abs_edge_ptr->slew_e_ptr()->from_node_ptr()){
      e = abs_edge_ptr->slew_e_ptr();
    }
    // Sanity check. The path propagation can't meet a branching pin.
    else if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name()<< " slew_e "<<abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->name()
                << " -> "<< abs_edge_ptr->slew_e_ptr()->to_node_ptr()->pin_ptr()->name();
      break;

    }
    else {
      //CHECK(from_node_ptr->num_fanouts() == 1);
      e = from_node_ptr->fanout().head()->item();

    }
    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);


    P.first = t.first; //slew
    P.second += t.second; //delay

    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = to_rf;
  }


  // start non unate part
  float_pair_t P1(P.first, P.second);// first no unate-> rise
  float_pair_t P2(P.first, P.second);// first no unate-> fall
    
  timing_pt tr1 = first_non->timing_ptr(el, from_rf, RISE);
  timing_pt tr2 = first_non->timing_ptr(el, from_rf, FALL);
  float_pair_t t1(P.first,0);
  float_pair_t t2(P.first, 0); 
  
  if(tr1!=nullptr){
    t1 = _infer_timing(abs_edge_ptr, first_non, el, from_rf, RISE, P1.first, load);
  }          
  if(tr2 !=nullptr){
    t2 = _infer_timing(abs_edge_ptr, first_non, el, from_rf, FALL, P2.first, load);
  }
  P1.first = t1.first; //slew
  P1.second += t1.second; //delay
  P2.first = t2.first; //slew
  P2.second += t2.second; //delay
 
  from_node_ptr = first_non->to_node_ptr();
  int irf1=RISE;
  int irf2=FALL;
  
  while(from_node_ptr != last_non->to_node_ptr()) {

    auto e = from_node_ptr->fanout().head()->item();
    if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    
    int orf1;
    int orf2;
    float_pair_t t1f,t1r;
    float_pair_t t2f,t2r;

    switch(e->timing_sense()){
      case NON_UNATE:

        t1r = _infer_timing(abs_edge_ptr, e, el, irf1, RISE, P1.first, load);

        t1r.second =  t1r.second + P1.second;

        t1f= _infer_timing(abs_edge_ptr, e, el, irf1, FALL, P1.first, load);

        t1f.second =  t1f.second + P1.second;
              

        t2r = _infer_timing(abs_edge_ptr, e, el, irf2, RISE, P2.first, load);

        t2r.second =  t2r.second + P2.second;

        t2f= _infer_timing(abs_edge_ptr, e, el, irf2, FALL, P2.first, load);

        t2f.second =  t2f.second + P2.second;
   


        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r.second>t2r.second)?t2r:t1r;
            P2=(t1f.second>t2f.second)?t2f:t1f;
          break;
          case LATE:// max
            P1=(t1r.second<t2r.second)?t2r:t1r;
            P2=(t1f.second<t2f.second)?t2f:t1f;
          break;
          default:
            //CHECK(false);
          break;
        }
        orf1=RISE;
        orf2=FALL;

      break;
    
      case POSITIVE_UNATE:
        orf1=irf1;
        orf2=irf2;

        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, P1.first, load);

        P1.first = t1.first;
        P1.second += t1.second;

        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, P2.first, load);

        P2.first = t2.first;
        P2.second += t2.second;

      
      break;
      case NEGATIVE_UNATE:

        orf1=!irf1;
        orf2=!irf2;

        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, P1.first, load);
        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, P2.first, load);

        P1.first = t1.first;
        P1.second += t1.second;

        
        P2.first = t2.first;
        P2.second += t2.second;


      break;
      default:
        //CHECK(false);
      break;
    }

    irf1=orf1;
    irf2=orf2;

    from_node_ptr = e->to_node_ptr();    

  }


  from_rf = last_to_rf;
  P=(last_to_rf==irf1)?P1:P2;
  //CHECK(irf1!=irf2);



 
  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != head) {

    // Sanity check. The path propagation can't meet a branching pin.
    if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      break;
    }


    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.
    auto e = from_node_ptr->fanout().head()->item();
    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);

    P.first = t.first; //slew
    P.second += t.second; //delay

    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //from_rf = to_rf;
  }
  //CHECK(from_rf == orf);

  return P;
 
  


  //CHECK(from_rf == orf);

  return P;
}


// Function: _infer_comb_non_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_pair_t Abstractor::_infer_comb_non_timing_delay(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);
  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  // Get the tail and head of the path.
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto decided = abs_edge_ptr->slew_e_ptr()->from_node_ptr();

  auto from_node_ptr = tail;
  auto from_rf = irf;

  edge_pt first_non=nullptr;
  edge_pt last_non=nullptr;
  node_pt temp= decided;
  int trf=irf;
  //int first_from_rf;
  int last_to_rf=RISE;

  
  // find last non and first non 
  while(temp!=head){
    edge_pt e =nullptr;
    if(temp == decided){
      e = abs_edge_ptr->slew_e_ptr();
    }
    else {
      //CHECK(temp->num_fanouts()==1);
      e =temp->fanout().head()->item();
    }
    if( e->timing_sense()==NON_UNATE){
      last_non = e;  
      last_to_rf=RISE;
      if(first_non == nullptr){
        first_non = e;
      //  first_from_rf=trf;
      }
    }
    trf =  (e->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
    if(e!=last_non)
      last_to_rf = (e->timing_sense() == POSITIVE_UNATE) ? last_to_rf : !last_to_rf ;
    temp = e->to_node_ptr();
  }

  last_to_rf=(last_to_rf==RISE)? orf: !orf;

  //SLEW possible




  // 1st edge is rctree ----> therefore, will not be non_unate edge 
  if (from_node_ptr!=decided) {
    if( decided->num_fanins() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }
    auto e = decided->fanin().head()->item();
    if(e->from_node_ptr()!=tail) {
      LOG(ERROR) << "Fail to infer timing on from abs node is 1 level earlier than decided abs node" +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }
    P = _infer_timing(abs_edge_ptr, e, el, from_rf, irf, P.first, load);
    from_node_ptr = decided;
    from_rf = irf;

    //if(head->pin_ptr()->name()=="inst_174_ZN" && abs(load-12)<2 && el==LATE && irf==RISE && orf==RISE){ 
    //}

  }


  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != first_non->from_node_ptr() ) {
    edge_pt e=nullptr;

    if(from_node_ptr == abs_edge_ptr->slew_e_ptr()->from_node_ptr()){
      e = abs_edge_ptr->slew_e_ptr();
    }
    // Sanity check. The path propagation can't meet a branching pin. 
    else if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name()<< " slew_e "<<abs_edge_ptr->slew_e_ptr()->from_node_ptr()->pin_ptr()->name()
                << " -> "<< abs_edge_ptr->slew_e_ptr()->to_node_ptr()->pin_ptr()->name()<<endl<<
                abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<
                abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name();
      break;
    }
    else {
      e = from_node_ptr->fanout().head()->item();

    }
    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);


    P.first = t.first; //slew
    P.second += t.second; //delay

    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = to_rf;
 


  }



  // start non unate part
  float_pair_t P1(P.first, P.second);// first no unate-> rise
  float_pair_t P2(P.first, P.second);// first no unate-> fall
  timing_pt tptr1 = first_non->timing_ptr(el, from_rf, RISE);
  timing_pt tptr2 = first_non->timing_ptr(el, from_rf, FALL);

  float_pair_t t1(P.first,P.second);
  if(tptr1!=nullptr)
    t1 = _infer_timing(abs_edge_ptr, first_non, el, from_rf, RISE, P1.first, load);

  float_pair_t t2(P.first,P.second);
  if(tptr2!=nullptr)
    t2 = _infer_timing(abs_edge_ptr, first_non, el, from_rf, FALL, P2.first, load);

  
  P1.first = t1.first; //slew
  P1.second += t1.second; //delay
  P2.first = t2.first; //slew
  P2.second += t2.second; //delay
 
  from_node_ptr = first_non->to_node_ptr();
  int irf1=RISE;
  int irf2=FALL;


 
  while(from_node_ptr != last_non->to_node_ptr()) {

    auto e = from_node_ptr->fanout().head()->item();
    if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    
    int orf1=irf1;
    int orf2=irf2;
    float_pair_t t1f,t1r;
    float_pair_t t2f,t2r;
    float_pair_t det1r;
    float_pair_t det2r;
    float_pair_t  det1f;
    float_pair_t det2f;

    switch(e->timing_sense()){
      case NON_UNATE:

        t1r = _infer_timing(abs_edge_ptr, e, el, irf1, RISE, P1.first, load);
        
        det1r = t1r;

        t1r.second =  t1r.second + P1.second;


        t1f= _infer_timing(abs_edge_ptr, e, el, irf1, FALL, P1.first, load);

        det1f=t1f;

        t1f.second =  t1f.second + P1.second;

              

        t2r = _infer_timing(abs_edge_ptr, e, el, irf2, RISE, P2.first, load);

        det2r = t2r;

        t2r.second =  t2r.second + P2.second;

        t2f= _infer_timing(abs_edge_ptr, e, el, irf2, FALL, P2.first, load);

        det2f=t2f;

        t2f.second =  t2f.second + P2.second;
   
        //DEBUG 


        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r.second>t2r.second)?t2r:t1r;
            //CHECK(t2r.second!=0 && t1r.second!=0);
            P2=(t1f.second>t2f.second)?t2f:t1f;
            //CHECK(t2r.second!=0 && t1r.second!=0);
          break;
          case LATE:// max
            P1=(t1r.second<t2r.second)?t2r:t1r;
            P2=(t1f.second<t2f.second)?t2f:t1f;
          break;
          default:
            //CHECK(false);
          break;
        }
        orf1=RISE;
        orf2=FALL;

      break;
    
      case POSITIVE_UNATE:
        orf1=irf1;
        orf2=irf2;

        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, P1.first, load);
        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, P2.first, load);

        P1.first = t1.first;
        P1.second += t1.second;
        P2.first = t2.first;
        P2.second += t2.second;

      break;
      case NEGATIVE_UNATE:

        orf1=!irf1;
        orf2=!irf2;

        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, P1.first, load);
        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, P2.first, load);

        P1.first = t1.first;
        P1.second += t1.second;
        P2.first = t2.first;
        P2.second += t2.second;
      break;
      default:
        //CHECK(false);
      break;
    }

    irf1=orf1;
    irf2=orf2;

    from_node_ptr = e->to_node_ptr();    




  }


  from_rf = last_to_rf;
  
  P=(last_to_rf==irf1)?P1:P2;
//  CHECK(irf1!=irf2);



 
  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != head) {

    // Sanity check. The path propagation can't meet a branching pin.
    if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      break;
    }


    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.
    auto e = from_node_ptr->fanout().head()->item();
    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);

    P.first = t.first; //slew
    P.second += t.second; //delay

    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;

  //  CHECK(e->timing_sense() !=NON_UNATE );

  }
//  CHECK(from_rf == orf);




  return P;
}


// Function: _infer_clock_const_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_t Abstractor::_infer_clock_const_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct constrained_slew) {

  float_pair_t P1(si, OT_FLT_ZERO);
  float_pair_t P2(si, OT_FLT_ZERO);
  float_pair_t P(si, OT_FLT_ZERO);
  

  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return si;
  }

  // Get the tail and head of the path.
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto decided = abs_edge_ptr->slew_e_ptr()->from_node_ptr();

  auto from_node_ptr = tail;
  auto from_rf = irf;
  float_t load = head->pin_ptr()->load(el,orf);
  int ckel=!el;
  

  //rctree
  if (from_node_ptr!=decided) {

    if( decided->num_fanins() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      return si;
    }

    auto e = decided->fanin().head()->item();

    if(e->from_node_ptr()!=tail) {
      LOG(ERROR) << "Fail to infer timing on from abs node is 1 level earlier than decided abs node" +
                 from_node_ptr->pin_ptr()->name();
      return si;
    }

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    P1 = _infer_timing(abs_edge_ptr, e, ckel, from_rf, to_rf, P.first, load);
    P2 = _infer_timing(abs_edge_ptr, e, ckel, !from_rf,!to_rf, P.first, load);
    


    from_node_ptr=decided;
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //from_rf = to_rf;
  }


  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != head && !from_node_ptr->is_clock_sink()) {
    
    edge_pt e =nullptr;

    if(from_node_ptr == abs_edge_ptr->slew_e_ptr()->from_node_ptr()){
      e = abs_edge_ptr->slew_e_ptr();
    }
    // Sanity check. The path propagation can't meet a branching pin.
    else if(from_node_ptr->num_fanouts() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      break;
    }
    else {
      e = from_node_ptr->fanout().head()->item();
    }


    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.
//    auto e = from_node_ptr->fanout().head()->item();

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t1 = _infer_timing(abs_edge_ptr, e, ckel, from_rf, to_rf, P1.first, load);
    auto t2 = _infer_timing(abs_edge_ptr, e, ckel, !from_rf, !to_rf, P2.first, load);

    P1.first = t1.first; //slew
    P1.second += t1.second; //delay
    P2.first = t2.first; //slew
    P2.second += t2.second; //delay



    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //from_rf = to_rf;
  }
  
  
  //P=P1;

  for(const auto &  e:from_node_ptr->fanout()){
    if(e->to_node_ptr() == head){
      timing_pt timing_ptr =  e->timing_ptr(el,from_rf,orf);  
      if(timing_ptr==nullptr){
        //from_rf=!from_rf;
        //CHECK(false);
        //timing_ptr =  e->timing_ptr(el,from_rf,orf);
      }
      //CHECK(timing_ptr!=nullptr)<<el<<from_rf<<orf;
      //test_pt test =  e->to_node_ptr()->pin_ptr()->test_ptr();
      //float_t raw_rat = test->rat(from_rf,orf);
      //float_t cppr_rat = e->to_node_ptr()->rat(from_rf, orf);
      auto t =  e->timing_arc_ptr(el)->constraint(from_rf, orf, P.first, constrained_slew);
//      auto lut = (orf==RISE)? e->timing_arc_ptr(el)->timing_ptr(from_rf,orf)->rise_;
  
      //CHECK(t>0)<<to_string(P.first)<<" "<<to_string(constrained_slew)<< " "<< to_string(t);


      if(el==EARLY)      
//        P.second = min(t+P1.second,t+P2.second);
        P.second =t+P1.second;
      else {
        P.second= t-P1.second;
//        P.second = min(t-P1.second,t-P2.second);
      }

      break;
    }  
  }


  return P.second;
}


// Function: _infer_clock_comb_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_pair_t Abstractor::_infer_clock_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);
  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

//  CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  float_t so = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
  P.first = so;

  const auto tail = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  auto to_rf = orf;
  auto to_node_ptr = tail;

  while( to_node_ptr != head ){

  //  CHECK( to_node_ptr->num_fanins() == 1);
    edge_pt e = to_node_ptr->fanin().head()->item();
    // load here is not important  

    int from_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    //CHECK(e->timing_sense() != NON_UNATE);
  
    float_t input_slew = e->from_node_ptr()->slew(el, from_rf);
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, input_slew, load);


    // slew must be constant 
    P.second += t.second; // delay 
    to_node_ptr = e->from_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
  //CHECK(to_rf == irf);

  return P;
}
//----------------------------------------------------------------
//TODO
float_t Abstractor::_infer_list_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, list<abs_edge_pt> & edgelist){
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());

  //TODO
  return P;
}

//----------------------------------------------------------------------------------------------------------------------------
void_t Abstractor::_abstract_cross_timing(abs_edge_pt abs_edge_ptr, abs_edge_pt a, abs_edge_pt b){
  
  if(abs_edge_ptr == nullptr) return;
   
  if(abs_edge_ptr->type() != AbsEdgeType::CROSS_ABS_EDGE){
    LOG(ERROR) <<  "Incorrect abs_edge type (expect CROSS_ABS_EDGE) ";
    return;
  }
//  CHECK(abs_edge_ptr->to_abs_node_ptr()->same_load());
//  CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew());

   EL_RF_ITER(el, irf) {

    timing_pt old_timing_ptr=nullptr;
    timing_pt new_timing_ptr=nullptr;

    switch(abs_edge_ptr->timing_sense()){
      case NON_UNATE:

        if(abs_edge_ptr->from_abs_node_ptr()->is_clock() && abs_edge_ptr->slew_e_ptr()!=nullptr){

          //CHECK(abs_edge_ptr->slew_e_ptr()!=nullptr);
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);

          if(old_timing_ptr!=nullptr){
            _initiate_cross_timing(abs_edge_ptr, el, irf , irf, a, b);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());

          }

          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
          if(old_timing_ptr!=nullptr){
            _initiate_cross_timing(abs_edge_ptr, el, irf , !irf, a, b);

            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
          }
        }else if(abs_edge_ptr->from_abs_node_ptr()->is_clock()){
          
          if(irf==RISE){
            _initiate_cross_timing(abs_edge_ptr, el, irf , irf, a, b);
            _initiate_cross_timing(abs_edge_ptr, el, irf , !irf, a, b);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);

          }
        }
        else{
          _initiate_cross_timing(abs_edge_ptr, el, irf , irf, a, b);
          _initiate_cross_timing(abs_edge_ptr, el, irf , !irf, a, b);

        }
        break;
      case POSITIVE_UNATE:
          _initiate_cross_timing(abs_edge_ptr, el, irf , irf, a, b);
        break;
      case NEGATIVE_UNATE:
          _initiate_cross_timing(abs_edge_ptr, el, irf , !irf, a, b);
        break;
      default:
        //CHECK(false);
        break;
    }
  }

  EL_RF_RF_ITER(el, irf, orf) {
    _abstract_cross_timing(abs_edge_ptr, el ,irf, orf, a, b);
  }
}

void_t Abstractor::_abstract_cross_timing(abs_edge_pt abs_edge_ptr, int el , int irf, int orf, abs_edge_pt a, abs_edge_pt b){
  auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);
  if(timing_ptr == nullptr) return;
  auto& dlut = (orf == RISE) ? timing_ptr->cell_rise() : timing_ptr->cell_fall();
  auto& tlut = (orf == RISE) ? timing_ptr->rise_transition() : timing_ptr->fall_transition();

  for(unsigned_t l(0); l<dlut.size1(); ++l) {
    for(unsigned_t s(0); s<dlut.size2(); ++s) {
      auto load = dlut.indices1(l);
      auto slew = dlut.indices2(s);
      float_t P = OT_FLT_ZERO;
      switch(abs_edge_ptr->type()) {
        case AbsEdgeType::CROSS_ABS_EDGE:
          P = _infer_cross_timing(abs_edge_ptr, el, irf, orf, slew, load, a, b);
        break;
        default:
          //CHECK(false);
        break;
      }
      dlut.assign_value(l, s, P);
    }
  }      
  _shrink_lut(dlut);

  for(unsigned_t l(0); l<tlut.size1(); ++l) {
    for(unsigned_t s(0); s<tlut.size2(); ++s) {
      auto load = dlut.indices1(l);
      auto slew = dlut.indices2(s);
      float_t P = OT_FLT_ZERO;
      switch(abs_edge_ptr->type()) {
        case AbsEdgeType::CROSS_ABS_EDGE:
          if(abs_edge_ptr-> to_abs_node_ptr()->same_slew()){
            P = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
          }else {
            P = _infer_cross_slew_timing(abs_edge_ptr, el, irf, orf, slew, load, a, b);
//            cout<< abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<abs_edge_ptr-> to_abs_node_ptr()->pin_ptr()->name()<< "  infer cross slew"<<endl;
          }
        break;
        default:
          //CHECK(false);
        break;
      }
      tlut.assign_value(l, s, P);
    }
  }
  _shrink_lut(tlut);

}
//----------------------------------------------------------------------------------------------------
float_t Abstractor::_infer_cross_slew_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct slew, float_ct load, abs_edge_pt a, abs_edge_pt b){
  float_t P(slew);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  float_t t;
  if(abs_edge_ptr->num_nonunate() ==  0){
    int mrf =  (a->timing_sense() == POSITIVE_UNATE) ? irf : !irf;
    float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
    t = _infer_abs_slew_timing(abs_edge_ptr, a, el, irf, mrf, slew, ld);
    P = t; // slew


    float_t si = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
    t = _infer_abs_slew_timing(abs_edge_ptr, b, el, mrf, orf, si, load);
    P = t; // slew

  }else{
    if(a->timing_sense()==NON_UNATE && b->timing_sense()==NON_UNATE){
      float_t P1(P);// first no unate-> rise
      float_t P2(P);// first no unate-> fall
      timing_pt tptr1 = a->timing_ptr(el, irf, RISE);
      timing_pt tptr2 = a->timing_ptr(el, irf, FALL);
      float_t ld = _same_max_min( a->to_abs_node_ptr()->max_load(el, RISE), a->to_abs_node_ptr()->min_load(el, RISE));

      float_t t1(P);
      if(tptr1!=nullptr){
        t1 = _infer_abs_slew_timing(abs_edge_ptr, a, el, irf, RISE, slew, ld);
      }

      float_t t2(P);
      ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, FALL), a->to_abs_node_ptr()->min_load(el, FALL));
      if(tptr2!=nullptr){
        t2 = _infer_abs_slew_timing(abs_edge_ptr, a, el, irf, FALL, slew, ld);
      }
      P1 = t1; //slew
      P2 = t2; //slew
      int irf1=RISE;
      int irf2=FALL;
      float_t t1r;
      float_t t2r;
      float_t si = _same_max_min( a->to_abs_node_ptr()->max_slew(el, irf1), a->to_abs_node_ptr()->min_slew(el, irf1));
      t1r = _infer_abs_slew_timing(abs_edge_ptr, b, el, irf1, orf, si, load);

      si = _same_max_min( a->to_abs_node_ptr()->max_slew(el, irf2), a->to_abs_node_ptr()->min_slew(el, irf2));
      t2r = _infer_abs_slew_timing(abs_edge_ptr, b, el, irf2, orf, si, load);


      // skew (second)
      switch(el){
        case EARLY:// min
          P=(t1r>t2r)?t2r:t1r;
          //CHECK(t2r!=0.0f && t1r!=0.0f);
        break;
        case LATE:// max
          P=(t1r<t2r)?t2r:t1r;
        break;
        default:
          //CHECK(false);
        break;
      }
      
    
    }else if(a->timing_sense()==NON_UNATE ){
      int mrf = (b->timing_sense()==POSITIVE_UNATE)? orf: !orf;
      float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
      t = _infer_abs_slew_timing(abs_edge_ptr,a, el, irf, mrf, slew, ld);
      P = t; 
      float_t si = _same_max_min(a->to_abs_node_ptr()->max_slew(el, mrf), a->to_abs_node_ptr()->min_slew(el,mrf));
      t = _infer_abs_slew_timing(abs_edge_ptr, b, el, mrf, orf, si, load); 
      P = t; // slew
      //from_rf = (b->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;

    }else if(b->timing_sense()==NON_UNATE){
      int mrf = (a->timing_sense()==POSITIVE_UNATE)? irf: !irf;
      float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
      t = _infer_abs_slew_timing(abs_edge_ptr,a, el, irf, mrf, slew, ld);
      P = t; //slew
      float_t si = _same_max_min(a->to_abs_node_ptr()->max_slew(el, mrf), a->to_abs_node_ptr()->min_slew(el,mrf));
      t = _infer_abs_slew_timing(abs_edge_ptr, b, el, mrf, orf, si, load);
      P = t; // slew
    }
    else{
      //CHECK(false);
    }
    
  }
  return P;

}


//----------------------------------------------------------------------------------------------------------------------------
/*
float_t Abstractor::_infer_back_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  //float_t so = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
  
  const auto tail = abs_edge_ptr->to_abs_node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr();
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();
  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }

  auto to_rf = orf;
  auto to_node_ptr = tail;
   
// computing first abs e  
{
  int from_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;   
  auto t = _infer_abs_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf);
  P += t; // delay
  
  to_node_ptr = first_abs_e_ptr->from_abs_node_ptr();
  to_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
}

  while( to_node_ptr != head ){
    CHECK( to_node_ptr->num_fanins() == 1)<< head->pin_ptr()->name()<<" -> "<<tail->pin_ptr()->name()<<" "<<to_node_ptr->pin_ptr()->name()<<" "<< to_node_ptr->num_fanins();
    abs_edge_pt e = to_node_ptr->fanin().head()->item();
    // load here is not important  

    int from_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    CHECK(e->timing_sense() != NON_UNATE);

    auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);


    // slew must be constant 
    P += t; // delay 
    to_node_ptr = e->from_abs_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
  CHECK(to_rf == irf);

  return P;



}  
*/

//----------------------------------------------------------------------------------------------------
float_t Abstractor::_infer_cross_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct slew, float_ct load, abs_edge_pt a, abs_edge_pt b){
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  //CHECK(a->to_abs_node_ptr()->same_load());
  //CHECK(a->to_abs_node_ptr()->same_slew());

  float_t t;
  if(abs_edge_ptr->num_nonunate() ==  0){
  
    int mrf =  (a->timing_sense() == POSITIVE_UNATE) ? irf : !irf;


    float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
    t = _infer_abs_timing(abs_edge_ptr, a, el, irf, mrf, slew, ld);
    P += t; // delay

    float_t si = _same_max_min(a->to_abs_node_ptr()->max_slew(el, mrf), a->to_abs_node_ptr()->min_slew(el, mrf));
    t = _infer_abs_timing(abs_edge_ptr, b, el, mrf, orf, si, load);
    P += t; // delay

  }else{
    if(a->timing_sense()==NON_UNATE && b->timing_sense()==NON_UNATE){
      float_t P1(P);// first no unate-> rise
      float_t P2(P);// first no unate-> fall
      timing_pt tptr1 = a->timing_ptr(el, irf, RISE);
      timing_pt tptr2 = a->timing_ptr(el, irf, FALL);
      float_t ld = _same_max_min( a->to_abs_node_ptr()->max_load(el, RISE), a->to_abs_node_ptr()->min_load(el, RISE));

      float_t t1(P);
      if(tptr1!=nullptr){
        t1 = _infer_abs_timing(abs_edge_ptr, a, el, irf, RISE, slew, ld);
      }

      float_t t2(P);
      ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, FALL), a->to_abs_node_ptr()->min_load(el, FALL));
      if(tptr2!=nullptr){
        t2 = _infer_abs_timing(abs_edge_ptr, a, el, irf, FALL, slew, ld);
      }
      P1 += t1; //delay
      P2 += t2; //delay
      int irf1=RISE;
      int irf2=FALL;
      float_t t1r;
      float_t t2r;
      float_t si =  _same_max_min(a->to_abs_node_ptr()->max_slew(el, irf1), a->to_abs_node_ptr()->min_slew(el, irf1));
      t1r = _infer_abs_timing(abs_edge_ptr, b, el, irf1, orf, si, load);
      t1r =  t1r + P1;
      si =  _same_max_min(a->to_abs_node_ptr()->max_slew(el, irf2), a->to_abs_node_ptr()->min_slew(el, irf2));
      t2r = _infer_abs_timing(abs_edge_ptr, b, el, irf2, orf, si, load);
      t2r =  t2r + P2;


      // delay (second)
      switch(el){
        case EARLY:// min
          P=(t1r>t2r)?t2r:t1r;
          //CHECK(t2r!=0.0f && t1r!=0.0f);
        break;
        case LATE:// max
          P=(t1r<t2r)?t2r:t1r;
        break;
        default:
          //CHECK(false);
        break;
      }
      
    
    }else if(a->num_nonunate()>0 ){
      int mrf = (b->timing_sense()==POSITIVE_UNATE)? orf: !orf;
      float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
      t = _infer_abs_timing(abs_edge_ptr,a, el, irf, mrf, slew, ld);
      P += t; 
      float_t si = _same_max_min(a->to_abs_node_ptr()->max_slew(el, mrf), a->to_abs_node_ptr()->min_slew(el,mrf));
      t = _infer_abs_timing(abs_edge_ptr, b, el, mrf, orf, si, load); 
      P += t; // delay
      //from_rf = (b->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;

    }else if(b->num_nonunate()>0){
      int mrf = (a->timing_sense()==POSITIVE_UNATE)? irf: !irf;
      float_t ld = _same_max_min(a->to_abs_node_ptr()->max_load(el, mrf), a->to_abs_node_ptr()->min_load(el, mrf));
      t = _infer_abs_timing(abs_edge_ptr,a, el, irf, mrf, slew, ld);
      P += t; 
      float_t si = _same_max_min(a->to_abs_node_ptr()->max_slew(el, mrf), a->to_abs_node_ptr()->min_slew(el,mrf));
      t = _infer_abs_timing(abs_edge_ptr, b, el, mrf, orf, si, load);
      P += t; // delay 
    }
    else{
      //CHECK(false)<< a->num_nonunate()<<" "<<b->num_nonunate();
    }
  }
  //CHECK(from_rf == orf);
  return P;

}


//----------------------------------------------------------------------------------------------------------------------------
float_t Abstractor::_infer_back_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  //float_t so = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
  
  const auto tail = abs_edge_ptr->to_abs_node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr();
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();
  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }

  auto to_rf = orf;
  auto to_node_ptr = tail;
   
// computing first abs e  
{
  int from_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;   
  auto t = _infer_abs_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf);
  P += t; // delay
  
  to_node_ptr = first_abs_e_ptr->from_abs_node_ptr();
  to_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
}

  while( to_node_ptr != head ){
    //CHECK( to_node_ptr->num_fanins() == 1)<< head->pin_ptr()->name()<<" -> "<<tail->pin_ptr()->name()<<" "<<to_node_ptr->pin_ptr()->name()<<" "<< to_node_ptr->num_fanins();
    abs_edge_pt e = to_node_ptr->fanin().head()->item();
    // load here is not important  

    int from_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    //CHECK(e->timing_sense() != NON_UNATE);

    auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);


    // slew must be constant 
    P += t; // delay 
    to_node_ptr = e->from_abs_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
  //CHECK(to_rf == irf);

  return P;



}  


float_t Abstractor::_infer_back_comb_non_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());

  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();

  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }
 
  
  const auto tail = abs_edge_ptr->to_abs_node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr();

  auto to_rf = orf;
  auto to_node_ptr = tail;

  abs_node_pt temp = tail;
  abs_edge_pt first_non=nullptr;
  abs_edge_pt last_non=nullptr;
   
  int first_from_rf = RISE;


  if( first_abs_e_ptr->timing_sense()==NON_UNATE){
    first_non=first_abs_e_ptr;
    first_from_rf = RISE;
    if(last_non ==  nullptr){
      last_non = first_abs_e_ptr;
    }
  }else {
    first_from_rf=(first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? first_from_rf: !first_from_rf;
  }

 
  temp = first_abs_e_ptr->from_abs_node_ptr();

  while( temp != head ){
    //CHECK(temp->num_fanins()==1) << head->pin_ptr()->name()<<" -> "<<tail->pin_ptr()->name()<<" "<<temp->pin_ptr()->name()<<endl;
    abs_edge_pt e =temp->fanin().head()->item();
    if( e->num_nonunate()>0){
      first_non = e;
      first_from_rf = RISE;
      if(last_non ==  nullptr){
        last_non = e;
      }
    }else{
      first_from_rf=(e->timing_sense() == POSITIVE_UNATE) ? first_from_rf: !first_from_rf;
    }
    temp = e->from_abs_node_ptr();
  }

  first_from_rf = (first_from_rf==RISE) ? irf: !irf;
  
 


  //First abs edge is not non unate
  if(first_abs_e_ptr != last_non ){
    int from_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    auto t = _infer_abs_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf);
    P += t; // delay
  
    to_node_ptr = first_abs_e_ptr->from_abs_node_ptr();
    to_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  

    while( to_node_ptr != last_non->to_abs_node_ptr()){
      if(to_node_ptr->num_fanins()!=1){
        LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
        break;
      }
      abs_edge_pt e = to_node_ptr->fanin().head()->item();
      int from_rf =  (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
      auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);

      P += t; // delay
      to_node_ptr = e->from_abs_node_ptr();
      to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    }
  }

  // start non unate part
  float_t P1(P);// first no unate-> rise
  float_t P2(P);// first no unate-> fall
  timing_pt tptr1 = last_non->timing_ptr(el, RISE, to_rf);
  auto lut_tpr1 = (to_rf == RISE)? tptr1->cell_rise(): tptr1->cell_fall();

  timing_pt tptr2 = last_non->timing_ptr(el, FALL, to_rf);
  auto lut_tpr2 =  (to_rf == RISE)?tptr2->cell_rise(): tptr2->cell_fall();
  

  float_t t1(P);
  if(tptr1!=nullptr){
    t1 = _infer_abs_timing(abs_edge_ptr, last_non, el, RISE, to_rf);
  }

  float_t t2(P);
  if(tptr2!=nullptr){
    t2 = _infer_abs_timing(abs_edge_ptr, last_non, el, FALL, to_rf);
  }

  P1 += t1; //delay
  P2 += t2; //delay
  
  to_node_ptr = last_non->from_abs_node_ptr();
  int orf1=RISE;
  int orf2=FALL;

  while( to_node_ptr != first_non->from_abs_node_ptr()) {
    if(to_node_ptr->num_fanins()!=1){
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    auto e = to_node_ptr->fanin().head()->item();
    int irf1;
    int irf2;
    float_t t1f,t1r;
    float_t t2f,t2r;
    switch(e->timing_sense()){
      case NON_UNATE:
        t1r = _infer_abs_timing(abs_edge_ptr, e, el, RISE, orf1);
        t1r =  t1r + P1;

        t2r = _infer_abs_timing(abs_edge_ptr, e, el, RISE, orf2);
        t2r =  t2r + P2;



        t1f = _infer_abs_timing(abs_edge_ptr, e, el, FALL, orf1);
        t1f =  t1f + P1;


        t2f= _infer_abs_timing(abs_edge_ptr, e, el, FALL, orf2);
        t2f =  t2f + P2;

        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r>t2r)?t2r:t1r;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
            P2=(t1f>t2f)?t2f:t1f;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
          break;
          case LATE:// max
            P1=(t1r<t2r)?t2r:t1r;
            P2=(t1f<t2f)?t2f:t1f;
          break;
          default:
            CHECK(false);
          break;
        }
        irf1=RISE;
        irf2=FALL;

      break;

      case POSITIVE_UNATE:
        irf1=orf1;
        irf2=orf2;
        t1 = _infer_abs_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 += t1;
        P2 += t2;

      break;
      case NEGATIVE_UNATE:

        irf1=!orf1;
        irf2=!orf2;
        t1 = _infer_abs_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 += t1;
        P2 += t2;
      break;
      default:
        CHECK(false);
      break;
    }
    orf1=irf1;
    orf2=irf2;
    to_node_ptr = e->from_abs_node_ptr();

  }


  to_rf = first_from_rf;

  P=(first_from_rf==orf1)?P1:P2;
//  CHECK(orf1!=orf2);


  while( to_node_ptr != head ){

    //CHECK( to_node_ptr->num_fanins() == 1);
    abs_edge_pt e = to_node_ptr->fanin().head()->item();
    // load here is not important  

    int from_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    //CHECK(e->timing_sense() != NON_UNATE);

    auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);


    // slew must be constant 
    P += t; // delay 
    to_node_ptr = e->from_abs_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
  //CHECK(to_rf == irf);


  return P;

 
}

float_pair_t Abstractor::_infer_clock_comb_non_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);
  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  float_t so = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
  P.first = so;

  const auto tail = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  auto to_rf = orf;
  auto to_node_ptr = tail;
  // find last non and first non
  
  node_pt temp = tail;
  edge_pt first_non=nullptr;
  edge_pt last_non=nullptr;
  //int last_to_rf = RISE;
  int first_from_rf = RISE;
  while( temp != head ){
    //CHECK(temp->num_fanins()==1);
    edge_pt e =temp->fanin().head()->item();
    if( e->timing_sense()==NON_UNATE){
      first_non = e;
      first_from_rf=RISE;
      if(last_non ==  nullptr){
        last_non = e;
      }
    }
    if(e!=first_non){
      first_from_rf=(e->timing_sense() == POSITIVE_UNATE) ? first_from_rf: !first_from_rf;
    }
    temp = e->from_node_ptr();
  }
  
  first_from_rf = (first_from_rf==RISE) ? irf: !irf;
  
  while( to_node_ptr != last_non->to_node_ptr()){
    if(to_node_ptr->num_fanins()!=1){
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    edge_pt e = to_node_ptr->fanin().head()->item();
    int from_rf =  (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    float_t input_slew = e->from_node_ptr()->slew(el, from_rf);
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, input_slew, load);
    
    P.second += t.second; // delay
    to_node_ptr = e->from_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
 
  // start non unate part
  float_pair_t P1(P.first, P.second);// first no unate-> rise
  float_pair_t P2(P.first, P.second);// first no unate-> fall
  timing_pt tptr1 = last_non->timing_ptr(el, RISE, to_rf);
  timing_pt tptr2 = last_non->timing_ptr(el, FALL, to_rf);

  float_t input_slew = last_non->from_node_ptr()->slew(el, RISE);

  float_pair_t t1(P.first,P.second);
  if(tptr1!=nullptr){
    t1 = _infer_timing(abs_edge_ptr, last_non, el, RISE, to_rf, input_slew, load);
  }
  float_pair_t t2(P.first,P.second);
  input_slew = last_non->from_node_ptr()->slew(el, FALL);
  if(tptr2!=nullptr){
    t2 = _infer_timing(abs_edge_ptr, last_non, el, FALL, to_rf, input_slew, load);
  }

  P1.second += t1.second; //delay
  P2.second += t2.second; //delay

  to_node_ptr = last_non->from_node_ptr();
  int orf1=RISE;
  int orf2=FALL;

 
  while( to_node_ptr != first_non->from_node_ptr()) {
    if(to_node_ptr->num_fanins()!=1){ 
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    auto e = to_node_ptr->fanin().head()->item();
    int irf1;
    int irf2;
    float_pair_t t1f,t1r;
    float_pair_t t2f,t2r;
    float_pair_t det1r;
    float_pair_t det2r;
    float_pair_t det1f;
    float_pair_t det2f;

    switch(e->timing_sense()){
      case NON_UNATE:
        input_slew = e->from_node_ptr()->slew(el, RISE);
        t1r = _infer_timing(abs_edge_ptr, e, el, RISE, orf1, input_slew, load);
        det1r =t1r;
        t1r.second =  t1r.second + P1.second;
        
        t2r = _infer_timing(abs_edge_ptr, e, el, RISE, orf2, input_slew , load);
        det2r = t2r;
        t2r.second =  t2r.second + P2.second;



        input_slew = e->from_node_ptr()->slew(el, FALL);
        t1f= _infer_timing(abs_edge_ptr, e, el, FALL, orf1, input_slew, load);
        det1f=t1f;
        t1f.second =  t1f.second + P1.second;

              
        t2f= _infer_timing(abs_edge_ptr, e, el, FALL, orf2, input_slew, load);
        det2f=t2f;
        t2f.second =  t2f.second + P2.second;
   
        //DEBUG 


        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r.second>t2r.second)?t2r:t1r;
            //CHECK(t2r.second!=0 && t1r.second!=0);
            P2=(t1f.second>t2f.second)?t2f:t1f;
            //CHECK(t2r.second!=0 && t1r.second!=0);
          break;
          case LATE:// max
            P1=(t1r.second<t2r.second)?t2r:t1r;
            P2=(t1f.second<t2f.second)?t2f:t1f;
          break;
          default:
            CHECK(false);
          break;
        }
        irf1=RISE;
        irf2=FALL;

      break;
    
      case POSITIVE_UNATE:
        irf1=orf1;
        irf2=orf2;
        input_slew = e->from_node_ptr()->slew(el, irf1);
        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, input_slew, load);
        input_slew = e->from_node_ptr()->slew(el, irf2);
        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, input_slew, load);

        P1.second += t1.second;
        P2.second += t2.second;

      break;
      case NEGATIVE_UNATE:

        irf1=!orf1;
        irf2=!orf2;
        input_slew = e->from_node_ptr()->slew(el, irf1);
        t1 = _infer_timing(abs_edge_ptr, e, el, irf1, orf1, input_slew, load);
        input_slew = e->from_node_ptr()->slew(el, irf2);
        t2 = _infer_timing(abs_edge_ptr, e, el, irf2, orf2, input_slew, load);

        P1.second += t1.second;
        P2.second += t2.second;
      break;
      default:
        CHECK(false);
      break;
    }

    //update
    orf1=irf1;
    orf2=irf2;
    to_node_ptr = e->from_node_ptr();    


  }

  P=(first_from_rf==orf1)?P1:P2;      
  //CHECK(orf1!=orf2);  

  while( to_node_ptr != head ){

    //CHECK( to_node_ptr->num_fanins() == 1);
    edge_pt e = to_node_ptr->fanin().head()->item();
    // load here is not important  

    int from_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
    //CHECK(e->timing_sense() == NON_UNATE);
  
    float_t input_slew = e->from_node_ptr()->slew(el, from_rf);
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, input_slew, load);


    // slew must be constant 
    P.second += t.second; // delay 
    to_node_ptr = e->from_node_ptr();
    to_rf = (e->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;
  }
  //CHECK(to_rf == irf);

  return P;
}
float_t Abstractor::_infer_forward_slew_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct slew, float_ct load) {
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew()&& abs_edge_ptr->to_abs_node_ptr()->same_load());
  const auto tail = abs_edge_ptr->to_abs_node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr();
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();
  auto from_rf = irf;
  auto from_node_ptr = head;
 
  // computing first abs e 
{ 
  int to_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
  float_t ld = _same_max_min(first_abs_e_ptr->to_abs_node_ptr()->max_load(el, to_rf), first_abs_e_ptr->to_abs_node_ptr()->min_load(el, to_rf)); 
  auto t = _infer_abs_slew_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf, slew, ld);
  P = t; // slew
  
  from_node_ptr = first_abs_e_ptr->to_abs_node_ptr();
  from_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
}

  while(from_node_ptr != tail){
    //CHECK( from_node_ptr->num_fanouts() == 1);
    abs_edge_pt e = from_node_ptr->fanout().head()->item();
    
    int to_rf =  (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_abs_slew_timing(abs_edge_ptr, e, el, from_rf, to_rf);
    P = t; // slew
    from_node_ptr = e->to_abs_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
  }
  //CHECK(from_rf == orf);
  return P;
}

float_t Abstractor::_infer_forward_slew_non_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load){
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();

  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }

  const auto tail = abs_edge_ptr->from_abs_node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr();

  auto from_rf = irf;
  auto from_node_ptr = tail;
  
  abs_node_pt temp = tail;
  abs_edge_pt first_non=nullptr;
  abs_edge_pt last_non=nullptr;

  int trf=irf;
  int last_to_rf = RISE;

  if( first_abs_e_ptr->timing_sense()==NON_UNATE){
    first_non = first_abs_e_ptr;
    last_to_rf = RISE;
    last_non = first_abs_e_ptr;
  }else {
    trf=(first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
  }

  temp = first_abs_e_ptr->to_abs_node_ptr();

  while( temp != head ){
    //CHECK(temp->num_fanouts()==1) << head->pin_ptr()->name()<<" -> "<<tail->pin_ptr()->name();
    abs_edge_pt e =temp->fanout().head()->item();
    if( e->num_nonunate()>0){
      last_non = e;
      last_to_rf = RISE;
      if(first_non ==  nullptr){
        first_non = e;
      }
    }
    trf=(e->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
    if(e!=last_non){
      last_to_rf=(e->timing_sense() == POSITIVE_UNATE) ? last_to_rf: !last_to_rf;
    }
    temp = e->to_abs_node_ptr();
  }

  last_to_rf = (last_to_rf==RISE) ? orf: !orf;


  float_t slew = si;
  //First abs edge is not non unate
  if(first_abs_e_ptr != first_non ){
    int to_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    float_t ld = _same_max_min(first_abs_e_ptr->to_abs_node_ptr()->max_load(el, to_rf), first_abs_e_ptr->to_abs_node_ptr()->min_load(el, to_rf));

    auto t = _infer_abs_slew_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf, slew, ld);
    P = t; // delay

    from_node_ptr = first_abs_e_ptr->to_abs_node_ptr();
    from_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;


    while( from_node_ptr != first_non->from_abs_node_ptr()){
      if(from_node_ptr->num_fanouts()!=1){
        LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
        break;
      }
      abs_edge_pt e = from_node_ptr->fanout().head()->item();
      int to_rf =  (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
      auto t = _infer_abs_slew_timing(abs_edge_ptr, e, el, from_rf, to_rf);

      P = t; // slew
      from_node_ptr = e->to_abs_node_ptr();
      from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    }
    slew = first_non->from_abs_node_ptr()->max_slew(el, from_rf);
  }


  // start non unate part
  float_t P1(P);// first no unate-> rise
  float_t P2(P);// first no unate-> fall
  timing_pt tptr1 = first_non->timing_ptr(el, from_rf, RISE);
  //auto lut_tpr1 = (to_rf == RISE)? tptr1->cell_rise(): tptr1->cell_fall();

  timing_pt tptr2 = first_non->timing_ptr(el, from_rf, FALL);
  //auto lut_tpr2 =  (to_rf == RISE)?tptr2->cell_rise(): tptr2->cell_fall();
  float_t ld = _same_max_min(first_non->to_abs_node_ptr()->max_load(el, RISE), first_non->to_abs_node_ptr()->min_load(el, RISE));
  
  float_t t1(P);
  if(tptr1!=nullptr){
    t1 = _infer_abs_slew_timing(abs_edge_ptr, first_non, el, from_rf, RISE, slew, ld);
  }

  ld = _same_max_min(first_non->to_abs_node_ptr()->max_load(el, FALL), first_non->to_abs_node_ptr()->min_load(el, FALL));
  float_t t2(P);
  if(tptr2!=nullptr){
    t2 = _infer_abs_slew_timing(abs_edge_ptr, first_non, el, from_rf, FALL, slew, ld);
  }

  P1 = t1; //slew
  P2 = t2; //slew

  from_node_ptr = first_non->to_abs_node_ptr();
  int irf1=RISE;
  int irf2=FALL;
  while( from_node_ptr != last_non->to_abs_node_ptr()) {
    if(from_node_ptr->num_fanouts()!=1){
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    auto e = from_node_ptr->fanout().head()->item();
    int orf1;
    int orf2;
    float_t t1f,t1r;
    float_t t2f,t2r;
    switch(e->timing_sense()){
      case NON_UNATE:
        t1r = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf1, RISE);
        //t1r =  t1r + P1;

        t2r = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf2, RISE);
        //t2r =  t2r + P2;

        t1f = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf1, FALL);
        //t1f =  t1f + P1;

        t2f = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf2, FALL);
        //t2f =  t2f + P2;
        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r>t2r)?t2r:t1r;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
            P2=(t1f>t2f)?t2f:t1f;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
          break;
          case LATE:// max
            P1=(t1r<t2r)?t2r:t1r;
            P2=(t1f<t2f)?t2f:t1f;
          break;
          default:
            CHECK(false);
          break;
        }
        orf1=RISE;
        orf2=FALL;

      break;

      case POSITIVE_UNATE:
        orf1=irf1;
        orf2=irf2;
        t1 = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 = t1;
        P2 = t2;

      break;
      case NEGATIVE_UNATE:

        orf1=!irf1;
        orf2=!irf2;
        t1 = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_slew_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 = t1;
        P2 = t2;
      break;
      default:
        //CHECK(false);
      break;
    }
    irf1=orf1;
    irf2=orf2;
    from_node_ptr = e->to_abs_node_ptr();
  }

  from_rf = last_to_rf;

  P=(last_to_rf==irf1)?P1:P2;
  //CHECK(irf1!=irf2);

  while( from_node_ptr!= head ){
    //CHECK( from_node_ptr->num_fanouts() == 1);
    abs_edge_pt e = from_node_ptr->fanout().head()->item();
    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //CHECK(e->timing_sense() != NON_UNATE);
    auto t = _infer_abs_slew_timing(abs_edge_ptr, e, el, from_rf, to_rf);
    P = t; // slew
    from_node_ptr = e->to_abs_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
   
  }
  //CHECK(from_rf == orf);
  return P;


}

float_t Abstractor::_infer_forward_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct slew, float_ct load) {
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());

  const auto tail = abs_edge_ptr->to_abs_node_ptr();
  const auto head = abs_edge_ptr->from_abs_node_ptr();
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();
  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }

  auto from_rf = irf;
  auto from_node_ptr = head;


  // computing first abs e 
{ 
  int to_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
  float_t ld = _same_max_min(first_abs_e_ptr->to_abs_node_ptr()->max_load(el, to_rf), first_abs_e_ptr->to_abs_node_ptr()->min_load(el, to_rf)); 
  auto t = _infer_abs_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf, slew, ld);
  P += t; // delay
  
  from_node_ptr = first_abs_e_ptr->to_abs_node_ptr();
  from_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
}

  while(from_node_ptr != tail){
    //CHECK( from_node_ptr->num_fanouts() == 1);
    abs_edge_pt e = from_node_ptr->fanout().head()->item();
    
    int to_rf =  (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);
    P += t; // delay 
    from_node_ptr = e->to_abs_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
  }
  //CHECK(from_rf == orf);
  return P;
}
//  infer forward comb non timing

float_t Abstractor::_infer_forward_comb_non_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load){
  float_t P(OT_FLT_ZERO);
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew() && abs_edge_ptr->to_abs_node_ptr()->same_load());
  abs_edge_pt first_abs_e_ptr = abs_edge_ptr->first_abs_e_ptr();

  if(first_abs_e_ptr == nullptr){
    LOG(ERROR) << "first abs e ptr == nullptr";
    return P;
  }

  const auto tail = abs_edge_ptr->from_abs_node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr();

  auto from_rf = irf;
  auto from_node_ptr = tail;
  
  abs_node_pt temp = tail;
  abs_edge_pt first_non=nullptr;
  abs_edge_pt last_non=nullptr;

  int trf=irf;
  int last_to_rf = RISE;

  if( first_abs_e_ptr->timing_sense()==NON_UNATE){
    first_non = first_abs_e_ptr;
    last_to_rf = RISE;
    last_non = first_abs_e_ptr;
  }else {
    trf=(first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
  }

  temp = first_abs_e_ptr->to_abs_node_ptr();

  while( temp != head ){
    //CHECK(temp->num_fanouts()==1) << head->pin_ptr()->name()<<" -> "<<tail->pin_ptr()->name();
    abs_edge_pt e =temp->fanout().head()->item();
    if( e->num_nonunate()>0){
      last_non = e;
      last_to_rf = RISE;
      if(first_non ==  nullptr){
        first_non = e;
      }
    }
    trf=(e->timing_sense() == POSITIVE_UNATE) ? trf: !trf;
    if(e!=last_non){
      last_to_rf=(e->timing_sense() == POSITIVE_UNATE) ? last_to_rf: !last_to_rf;
    }
    temp = e->to_abs_node_ptr();
  }

  last_to_rf = (last_to_rf==RISE) ? orf: !orf;


  float_t slew = si;
  //First abs edge is not non unate
  if(first_abs_e_ptr != first_non ){
    int to_rf =  (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    float_t ld = _same_max_min(first_abs_e_ptr->to_abs_node_ptr()->max_load(el, to_rf), first_abs_e_ptr->to_abs_node_ptr()->min_load(el, to_rf));

    auto t = _infer_abs_timing(abs_edge_ptr, first_abs_e_ptr, el, from_rf, to_rf, slew, ld);
    P += t; // delay

    from_node_ptr = first_abs_e_ptr->to_abs_node_ptr();
    from_rf = (first_abs_e_ptr->timing_sense() == POSITIVE_UNATE) ? to_rf : !to_rf;


    while( from_node_ptr != first_non->from_abs_node_ptr()){
      if(from_node_ptr->num_fanouts()!=1){
        LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
        break;
      }
      abs_edge_pt e = from_node_ptr->fanout().head()->item();
      int to_rf =  (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
      auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);

      P += t; // delay
      from_node_ptr = e->to_abs_node_ptr();
      from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    }
    slew = first_non->from_abs_node_ptr()->max_slew(el, from_rf);
  }


  // start non unate part
  float_t P1(P);// first no unate-> rise
  float_t P2(P);// first no unate-> fall
  timing_pt tptr1 = first_non->timing_ptr(el, from_rf, RISE);
  //auto lut_tpr1 = (to_rf == RISE)? tptr1->cell_rise(): tptr1->cell_fall();

  timing_pt tptr2 = first_non->timing_ptr(el, from_rf, FALL);
  //auto lut_tpr2 =  (to_rf == RISE)?tptr2->cell_rise(): tptr2->cell_fall();
  float_t ld = _same_max_min(first_non->to_abs_node_ptr()->max_load(el, RISE), first_non->to_abs_node_ptr()->min_load(el, RISE));
  
  float_t t1(P);
  if(tptr1!=nullptr){
    t1 = _infer_abs_timing(abs_edge_ptr, first_non, el, from_rf, RISE, slew, ld);
  }

  ld = _same_max_min(first_non->to_abs_node_ptr()->max_load(el, FALL), first_non->to_abs_node_ptr()->min_load(el, FALL));
  float_t t2(P);
  if(tptr2!=nullptr){
    t2 = _infer_abs_timing(abs_edge_ptr, first_non, el, from_rf, FALL, slew, ld);
  }

  P1 += t1; //delay
  P2 += t2; //delay

  from_node_ptr = first_non->to_abs_node_ptr();
  int irf1=RISE;
  int irf2=FALL;
  while( from_node_ptr != last_non->to_abs_node_ptr()) {
    if(from_node_ptr->num_fanouts()!=1){
      LOG(ERROR) << "Fail to infer non unate timing on the branching pin ";
      break;
    }
    auto e = from_node_ptr->fanout().head()->item();
    int orf1;
    int orf2;
    float_t t1f,t1r;
    float_t t2f,t2r;
    switch(e->timing_sense()){
      case NON_UNATE:
        t1r = _infer_abs_timing(abs_edge_ptr, e, el, irf1, RISE);
        t1r =  t1r + P1;

        t2r = _infer_abs_timing(abs_edge_ptr, e, el, irf2, RISE);
        t2r =  t2r + P2;

        t1f = _infer_abs_timing(abs_edge_ptr, e, el, irf1, FALL);
        t1f =  t1f + P1;

        t2f = _infer_abs_timing(abs_edge_ptr, e, el, irf2, FALL);
        t2f =  t2f + P2;
        // delay (second)
        switch(el){
          case EARLY:// min
            P1=(t1r>t2r)?t2r:t1r;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
            P2=(t1f>t2f)?t2f:t1f;
            //CHECK(t2r!=0.0f && t1r!=0.0f);
          break;
          case LATE:// max
            P1=(t1r<t2r)?t2r:t1r;
            P2=(t1f<t2f)?t2f:t1f;
          break;
          default:
            CHECK(false);
          break;
        }
        orf1=RISE;
        orf2=FALL;

      break;

      case POSITIVE_UNATE:
        orf1=irf1;
        orf2=irf2;
        t1 = _infer_abs_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 += t1;
        P2 += t2;

      break;
      case NEGATIVE_UNATE:

        orf1=!irf1;
        orf2=!irf2;
        t1 = _infer_abs_timing(abs_edge_ptr, e, el, irf1, orf1);
        t2 = _infer_abs_timing(abs_edge_ptr, e, el, irf2, orf2);

        P1 += t1;
        P2 += t2;
      break;
      default:
        CHECK(false);
      break;
    }
    irf1=orf1;
    irf2=orf2;
    from_node_ptr = e->to_abs_node_ptr();
  }

  from_rf = last_to_rf;

  P=(last_to_rf==irf1)?P1:P2;
  //CHECK(irf1!=irf2);

  while( from_node_ptr!= head ){
    //CHECK( from_node_ptr->num_fanouts() == 1);
    abs_edge_pt e = from_node_ptr->fanout().head()->item();
    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //CHECK(e->timing_sense() != NON_UNATE);
    auto t = _infer_abs_timing(abs_edge_ptr, e, el, from_rf, to_rf);
    P += t; // delay 
    from_node_ptr = e->to_abs_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
   
  }
  //CHECK(from_rf == orf);
  return P;


}
/*
// Procedure: _abstract_po_seg_timing
void_t Abstractor::_abstract_po_seg_timing_v1(abs_edge_pt new_abs_edge_ptr, pin_pt gear, rctree_pt new_rctree_ptr){

  if(gear == nullptr) {
    LOG(ERROR) << "Fail to abstract PO seg timing (nullptr exception)";
    return;
  }


  float_vt slew_indices;
  timing_lut_pt old_tlut;
  timing_lut_pt old_dlut;
  timing_lut_pt new_tlut;
  timing_lut_pt new_dlut;
  timing_pt old_timing_ptr;
  timing_pt new_timing_ptr;

  // a net from primay input to primary output
  if(gear->node_ptr()->num_fanins() == 0) { 
    LOG(WARNING) << "Ignore abstracting po segment timing (floating wire)";
    return;
  }

    // Restore the capacitance.
   
      for(const auto& e2 : gear->node_ptr()->fanout()) {
        if(e2->to_node_ptr() != new_abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()){
          continue;
        }
        CHECK(e2->to_node_ptr() == new_abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr());

        const auto& pin_name = e2->to_node_ptr()->pin_ptr()->name();

//        if(_find_abs_edge(e1->from_node_ptr()->pin_ptr(), e2->to_node_ptr()->pin_ptr()) != nullptr) {
//          continue;
//        }

//        new_abs_edge_ptr = _insert_abs_edge(e1->from_node_ptr()->pin_ptr(), e2->to_node_ptr()->pin_ptr(), AbsEdgeType::PO_SEGMENT);

        EL_RF_RF_ITER(el, irf, orf){

          old_timing_ptr = new_abs_edge_ptr->slew_e_ptr()->timing_ptr(el, irf, orf);

          if (old_timing_ptr == nullptr) continue;
      
          timing_sense_e ts = (irf==orf) ? POSITIVE_UNATE:NEGATIVE_UNATE;

          new_timing_ptr = new_abs_edge_ptr->insert_timing(el,irf,orf);
          new_timing_ptr->set_timing_sense(ts);
          new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());

          old_dlut = (orf == RISE) ? old_timing_ptr->cell_rise_ptr(): old_timing_ptr->cell_fall_ptr();
          old_tlut = (orf == RISE) ? old_timing_ptr->rise_transition_ptr(): old_timing_ptr->fall_transition_ptr();

          new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise_ptr(): new_timing_ptr->cell_fall_ptr();
          new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition_ptr(): new_timing_ptr->fall_transition_ptr();

          if (new_rctree_ptr == nullptr){
            *new_timing_ptr = *old_timing_ptr;
          }
          else {

            //CHECK(e2->net_ptr() != nullptr && e2->net_ptr()->rctree_ptr() != nullptr);

            // idx2 input slew indicies2 are the same
            new_dlut->indices2()= old_dlut->indices2();
            new_tlut->indices2()= old_tlut->indices2();

            // Retrieve the original cap from the net in e2.
            float_ct original_cap = e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)->cap(el, orf);

            new_rctree_ptr->set_cap(pin_name, el, orf, original_cap);
            new_rctree_ptr->update_rc_timing();


            // Get the raw rctree load.
            float_ct raw_load = e2->net_ptr()->rctree_ptr()->load(el,orf);

            // new idx1 (load) = old table idx1 - load
            for(const auto& idx : old_dlut->indices1()) {
              new_dlut->indices1().push_back(idx - raw_load);
            }


            for(const auto& idx : old_tlut->indices1()){
              new_tlut->indices1().push_back(idx - raw_load);
            }

            // resize
            new_dlut->resize(new_dlut->size1(),new_dlut->size2());
            new_tlut->resize(new_tlut->size1(),new_tlut->size2());

            // assign dlut value
            for(unsigned_t l=0; l<new_dlut->size1();++l){
              new_rctree_ptr->set_cap(pin_name, el, orf, new_dlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for(unsigned_t s=0; s<new_dlut->size2();++s){
                new_dlut->assign_value(l,s,new_rctree_ptr->delay(pin_name, el, orf) + old_dlut->table(l, s));
              }
            }

            // assign tlut value
            for (unsigned_t l=0;l<new_tlut->size1();++l){
              new_rctree_ptr->set_cap(pin_name,el,orf,new_tlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for (unsigned_t s=0;s<new_tlut->size2();++s){
                new_tlut->assign_value(l,s, new_rctree_ptr->slew(pin_name, el, orf, old_tlut->table(l, s)));
              }
            }
            new_rctree_ptr->set_cap(pin_name,el,orf,original_cap +
                                    (new_abs_edge_ptr->to_abs_node_ptr()->min_load(el,orf) +
                                     new_abs_edge_ptr->to_abs_node_ptr()->max_load(el,orf)) / OT_FLT_TWO );
          }
        }
      }
}
*/
// Function: _infer_po_seg_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_pair_t Abstractor::_infer_po_seg_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);

  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  // Get the tail and head of the path.
  //const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  
  //auto from_node_ptr = tail;

  auto e = abs_edge_ptr->slew_e_ptr();

  if (e->timing_sense()==NON_UNATE){

    float_pair_t t1(si, OT_FLT_ZERO);
    float_pair_t t2(si, OT_FLT_ZERO);
    
    if(e->timing_ptr(el,irf,orf)!=nullptr){
      //CHECK(e->timing_ptr(el,irf,orf)!=nullptr);
      t1 = _infer_po_seg_timing(abs_edge_ptr, e, el, irf, orf, P.first, load);
    }
   
    if(e->timing_ptr(el,!irf,orf)!=nullptr){
      //CHECK(e->timing_ptr(el,!irf,orf)!=nullptr);
      t2 = _infer_po_seg_timing(abs_edge_ptr, e, el, !irf, orf, P.first, load);
    }
    switch(el){
      case EARLY:// min
        if(e->timing_ptr(el,irf,orf)!=nullptr&& e->timing_ptr(el,!irf,orf)!=nullptr)
          P=(t1.second>t2.second)?t2:t1;
        else if (e->timing_ptr(el,irf,orf)!=nullptr){// no t2
          P = t1;
        }
        else if(e->timing_ptr(el,!irf,orf)!=nullptr){
          P=t2;
        }
        else CHECK(false);
      break;
      case LATE:// max
        if(e->timing_ptr(el,irf,orf)!=nullptr&& e->timing_ptr(el,!irf,orf)!=nullptr)
          P=(t1.second<t2.second)?t2:t1;
        else if (e->timing_ptr(el,irf,orf)!=nullptr){
          P = t1;
        }
        else if(e->timing_ptr(el,!irf,orf)!=nullptr){
          P=t2;
        }
        else CHECK(false);
      break;
      default:
       CHECK(false);
      break;
    }
  }
  else {

    //int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //CHECK(e->timing_ptr(el,irf,orf)!=nullptr);
    P = _infer_po_seg_timing(abs_edge_ptr, e, el, irf, orf, P.first, load);

    //from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;

  }

  //CHECK( abs_edge_ptr->to_node_ptr()->num_fanins()==1);

  // Infer the timing through the edge. The resulting slew is replaced with the returned
  // slew while the resulting delay is accumulated with the returned delay.

  //RCtree
  for (const auto ee: e->to_node_ptr()->fanout()){
    if(ee->to_node_ptr() == abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()){
      e= ee;
      break;
    }
  }
   
//  e =  abs_edge_ptr->to_node_ptr()->fanin().head()->item();
  
  auto t = _infer_po_seg_timing(abs_edge_ptr, e, el, orf, orf, P.first, load);


  P.first = t.first; //slew
  P.second += t.second; //delay

 
  return P;
}


// Function: _infer_comb_timing
// The function takes an input pair of two pins specified in a given abs edge and infers the
// path-based timing from the starting pin to the ending pin. The given abs edge should be assumed
// to be a unate-definite path. That is, given any transition at the starting pin, the path can be
// uniquely inferred to the ending pin.
//
//  there is a decision point that is the same as from abs node or a step later
//
float_pair_t Abstractor::_infer_comb_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);

  // Sanity check.
  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  // Get the tail and head of the path.
  const auto tail = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto head = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  const auto decided = abs_edge_ptr->slew_e_ptr()->from_node_ptr();
  

  auto from_node_ptr = tail;
  auto from_rf = irf;

  if (from_node_ptr!=decided) {

    if( decided->num_fanins() != 1) {
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }

    auto e = decided->fanin().head()->item();

    if(e->from_node_ptr()!=tail) {
      LOG(ERROR) << "Fail to infer timing on from abs node is 1 level earlier than decided abs node" +
                 from_node_ptr->pin_ptr()->name();
      return P;
    }

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    P = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);
    CHECK(std::isnan(P.second)==0)<< P.first;

    from_node_ptr=e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //from_rf = to_rf;
  }


  // Start from the tail node and infer the slew S until the head node.
  while(from_node_ptr != head) {

    // Sanity check. The path propagation can't meet a branching pin.
    edge_pt e=nullptr;

    if(from_node_ptr == abs_edge_ptr->slew_e_ptr()->from_node_ptr()){
      e =  abs_edge_ptr->slew_e_ptr();
    }
    else if (from_node_ptr->num_fanouts() != 1) {
      
      LOG(ERROR) << "Fail to infer timing on the branching pin " +
                 from_node_ptr->pin_ptr()->name()<<"   "<< abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name()
                <<" -> "<< abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name()<< " "<< from_node_ptr->num_fanouts()<<" ";
      break;
    }
    else{ 
      //CHECK( from_node_ptr->num_fanouts() == 1);
      e = from_node_ptr->fanout().head()->item();
    }
    


    // Infer the timing through the edge. The resulting slew is replaced with the returned
    // slew while the resulting delay is accumulated with the returned delay.
    
    //auto e = from_node_ptr->fanout().head()->item();

    int to_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    auto t = _infer_timing(abs_edge_ptr, e, el, from_rf, to_rf, P.first, load);
    CHECK(std::isnan(t.second)==0);

    

    P.first = t.first; //slew
    P.second += t.second; //delay

    // Proceed to the next node.
    from_node_ptr = e->to_node_ptr();
    from_rf = (e->timing_sense() == POSITIVE_UNATE) ? from_rf : !from_rf;
    //from_rf = to_rf;
  }
  //CHECK(from_rf == orf);
  CHECK(std::isnan(P.second)==0);

  return P;
}

// Function: _infer_po_seg_timing
// The core routine for inferring the timing through a given edge. The function takes an input
// abs_edge that specifies the unate-definite path at any timing split and transition, and
// propa/gate the input slew through the edge. A pair of output slew and delay is returned.
float_pair_t Abstractor::_infer_po_seg_timing(abs_edge_pt abs_edge_ptr, edge_pt e, int el, int from_rf,int to_rf, float_ct si, float_ct load) {

  if(abs_edge_ptr->type()!=AbsEdgeType::PO_SEGMENT){
    LOG(WARNING)<<"unexpected abs edge type";
    return float_pair_t(si, OT_FLT_ZERO);
  }
  if(e->timing_ptr(el,from_rf,to_rf)==nullptr && e->type()!=RCTREE_EDGE_TYPE)  {
     
    LOG(WARNING)<< "null timing ptr at po seg";
    return float_pair_t(si, OT_FLT_ZERO);

  }

  rctree_upt rctree_ptr = nullptr;
  if(e->net_ptr()!=nullptr && e->net_ptr()->rctree_ptr()!=nullptr)
    rctree_ptr  = _initiate_rctree(e->net_ptr()->rctree_ptr());

  auto to_node_ptr = e->to_node_ptr();
  auto timing_arc_ptr = e->timing_arc_ptr(el);
  float_t ori_cap=0;
  float_t temp_cap=0;
  float_pair_t P(si, OT_FLT_ZERO);
  float_pair_t t(si, OT_FLT_ZERO);

  abs_node_pt rc_abs_leave_node=nullptr;
  abs_node_pt root=nullptr;
  // Propagate the slew through the edge. Here we consider only the rctree edge and combinational
  // cell arc. This is assumed by default when the function is called.

  switch(e->edge_type()) {

  case RCTREE_EDGE_TYPE:
    if (rctree_ptr!=nullptr){
      ori_cap= rctree_ptr->rctree_node_ptr(to_node_ptr->pin_ptr()->name())->cap(el,to_rf);
      root = abs_edge_ptr->from_abs_node_ptr();
      for(const auto & abs_e : root->fanout()){
        rc_abs_leave_node = abs_e->to_abs_node_ptr();  
        if ( rc_abs_leave_node->pin_ptr()->node_ptr()!= to_node_ptr && abs_e->type() == AbsEdgeType::PO_SEGMENT){
          temp_cap =  rctree_ptr->rctree_node_ptr( rc_abs_leave_node->pin_ptr()->name())->cap( el, to_rf);
          rctree_ptr->set_cap(rc_abs_leave_node->pin_ptr()->name(), el, to_rf, (rc_abs_leave_node->max_load(el,to_rf) + rc_abs_leave_node->min_load(el,to_rf) )/2.0f-temp_cap);        
        }
      }
      
      rctree_ptr->set_cap(to_node_ptr->pin_ptr()->name(), el, to_rf, load+ori_cap);
      rctree_ptr->update_rc_timing();
      P.first = rctree_ptr->slew(to_node_ptr->pin_ptr()->name(), el, to_rf, si);
      
      P.second = rctree_ptr->delay(to_node_ptr->pin_ptr()->name(), el, to_rf);  
//      if(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name() == "x2633"){
//        cout<<"r "<< P.second<<" "<<load<<" "<<ori_cap<<" "<<load+ori_cap<<endl;
//      }
    } 
    // no rctree ptr sout=si delay=0
    else { // no rctree on original circuit
      P.first = si;
      P.second = OT_FLT_ZERO;
    }
    break;

  // Combinational edge type in which case we should consider the following cases:
  //   - case 1: to_node has the same net as the head -> get the load info from the internalinst_602:CK
  //             RC tree attached to the abs edge.
  //   - case 2: otherwise -> infer the timing from the timer
  case COMBINATIONAL_EDGE_TYPE: 
    if (e->to_node_ptr()->pin_ptr()->net_ptr()->has_primary_output()){

        ori_cap=0;
        root = abs_edge_ptr->from_abs_node_ptr();

        for(const auto & po : root->fanout()){
          if(po->to_abs_node_ptr()!=abs_edge_ptr->to_abs_node_ptr()&& abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->is_primary_output()  ){
            ori_cap=((po->to_abs_node_ptr()->max_load(el, to_rf)+ po->to_abs_node_ptr()->min_load(el, to_rf))/2.0f);
          }
        }
        //<< (abs_edge_ptr->to_abs_node_ptr()->max_load(el, to_rf)+ abs_edge_ptr->to_abs_node_ptr()->min_load(el, to_rf))/2<<endl; 
        
        P.first = timing_arc_ptr->slew(from_rf, to_rf, si, load+to_node_ptr->pin_ptr()->load(el, to_rf)/*+ori_cap*/);
        P.second = timing_arc_ptr->delay(from_rf, to_rf, si, load+to_node_ptr->pin_ptr()->load(el, to_rf)/*+ori_cap*/);
//        if(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name() == "x2451")
//          cout<<"c "<<P.second<<" "<<load<<" "<<to_node_ptr->pin_ptr()->load(el, to_rf)<<" "<<ori_cap<<" " <<load+to_node_ptr->pin_ptr()->load(el, to_rf)+ori_cap<<endl;
    } 
    else {
      CHECK(false);
    }
    break;
  case CONSTRAINT_EDGE_TYPE:
    //CHECK(false)<< abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->name()<< " -> "<< abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name()<<endl;
  break;
  // Exception.
  default:
    LOG(ERROR) << "Fail to infer the timing (EdgeType exception)" ;
    return P;
    break;
  };

  return P;
}
float_t Abstractor::_infer_abs_slew_timing(abs_edge_pt abs_edge_ptr, abs_edge_pt e, int el, int from_rf,int to_rf, float_ct slew, float_ct load){
  
  float_t P(OT_FLT_ZERO);
  auto timing_ptr = e->timing_ptr(el, from_rf, to_rf);  

  if(timing_ptr == nullptr ) {
    LOG(WARNING)<< "timing ptr is nullptr";
    return P;
  }

 
  timing_lut_pt lut_ptr;
  switch(to_rf) {
    case RISE:
      lut_ptr = timing_ptr->rise_transition_ptr(); 
    break;
    case FALL:
      lut_ptr = timing_ptr->fall_transition_ptr();
    break;
    default:
      lut_ptr = nullptr;
    break;
  };

  if(lut_ptr == nullptr){
    LOG(WARNING)<<" infer abs timing lut = nullptr";
    //CHECK(false);
    return P;
  } 
 
  if(lut_ptr->is_scalar()){
    
    return lut_ptr->table(0, 0); 
  }else {
    
    //CHECK(e->timing_sense() ==NON_UNATE);
    //CHECK();
   // cout<< e->from_abs_node_ptr()->pin_ptr()->name()<<endl;
    if(e->from_abs_node_ptr()->same_slew()){
      P = lut_ptr->lut_polation(load , _same_max_min(e->from_abs_node_ptr()->max_slew(el,from_rf),e->from_abs_node_ptr()->min_slew(el,from_rf) ));
    }  
    else{ 
      //cout<< e->from_abs_node_ptr()->pin_ptr()->name()<<endl;
      P = lut_ptr->lut_polation(load , slew);
    }
    return P;
  }
  

}
float_t Abstractor::_infer_abs_slew_timing(abs_edge_pt abs_edge_ptr, abs_edge_pt e, int el, int from_rf,int to_rf){
   
  float_t P(OT_FLT_ZERO);
  auto timing_ptr = e->timing_ptr(el, from_rf, to_rf);  

  if(timing_ptr == nullptr ) {
    LOG(WARNING)<< "timing ptr is nullptr";
    return P;
  }

 
  timing_lut_pt lut_ptr;
  switch(to_rf) {
    case RISE:
      lut_ptr = timing_ptr->rise_transition_ptr(); 
    break;
    case FALL:
      lut_ptr = timing_ptr->fall_transition_ptr();
    break;
    default:
      lut_ptr = nullptr;
    break;
  };

  if(lut_ptr == nullptr){
    LOG(WARNING)<<" infer abs timing lut = nullptr";
    //CHECK(false);
    return P;
  } 
  if(lut_ptr->is_scalar()){
    return lut_ptr->table(0, 0); 
  }else{
    //CHECK(false);
    return 0.0f;
  }
}

float_t Abstractor::_infer_abs_timing(abs_edge_pt abs_edge_ptr, abs_edge_pt e, int el, int from_rf,int to_rf){
   
  float_t P(OT_FLT_ZERO);
  auto timing_ptr = e->timing_ptr(el, from_rf, to_rf);  

  if(timing_ptr == nullptr ) {
    LOG(WARNING)<< "timing ptr is nullptr";
    return P;
  }

 
  timing_lut_pt lut_ptr;
  switch(to_rf) {
    case RISE:
      lut_ptr = timing_ptr->cell_rise_ptr(); 
    break;
    case FALL:
      lut_ptr = timing_ptr->cell_fall_ptr();
    break;
    default:
      lut_ptr = nullptr;
    break;
  };

  if(lut_ptr == nullptr){
    LOG(WARNING)<<" infer abs timing lut = nullptr";
    //CHECK(false);
    return P;
  } 
 
  if(lut_ptr->is_scalar()){
    return lut_ptr->table(0, 0); 
  }else{
    return 0.0f;
  }
}
float_t Abstractor::_infer_abs_timing(abs_edge_pt abs_edge_ptr, abs_edge_pt e, int el, int from_rf,int to_rf, float_ct slew, float_ct load) {
  
  float_t P(OT_FLT_ZERO);
  auto timing_ptr = e->timing_ptr(el, from_rf, to_rf);  

  if(timing_ptr == nullptr ) {
    LOG(WARNING)<< "timing ptr is nullptr";
    return P;
  }

 
  timing_lut_pt lut_ptr;
  switch(to_rf) {
    case RISE:
      lut_ptr = timing_ptr->cell_rise_ptr(); 
    break;
    case FALL:
      lut_ptr = timing_ptr->cell_fall_ptr();
    break;
    default:
      lut_ptr = nullptr;
    break;
  };

  if(lut_ptr == nullptr){
    LOG(WARNING)<<" infer abs timing lut = nullptr";
    CHECK(false);
    return P;
  } 
 
  if(lut_ptr->is_scalar()){
    return lut_ptr->table(0, 0); 
  }else if(!lut_ptr->empty()){
    //CHECK(e->timing_sense() ==NON_UNATE);
    //CHECK();
    P = lut_ptr->lut_polation(load , slew);
    return P;
  }else{
    return P;
  }
  
}
// Function: _infer_timing
// The core routine for inferring the timing through a given edge. The function takes an input
// abs_edge that specifies the unate-definite path at any timing split and transition, and
// propa/gate the input slew through the edge. A pair of output slew and delay is returned.
float_pair_t Abstractor::_infer_timing(abs_edge_pt abs_edge_ptr, edge_pt e, int el, int from_rf,int to_rf, float_ct si, float_ct load) {

  auto to_node_ptr = e->to_node_ptr();
  auto timing_arc_ptr = e->timing_arc_ptr(el);
  auto rctree_ptr = e->net_ptr() == nullptr ? nullptr : e->net_ptr()->rctree_ptr();
  float_t ori_cap=0;
  rctree_upt new_rc=nullptr;

  float_pair_t P(si, OT_FLT_ZERO);
  float_pair_t t(si, OT_FLT_ZERO);

  // Propagate the slew through the edge. Here we consider only the rctree edge and combinational
  // cell arc. This is assumed by default when the function is called.
  switch(e->edge_type()) {

  // RCTree edge type where we consider the following two cases:
  //   - all the load of rctree is fixed. Infer the timing from the timer.
  case RCTREE_EDGE_TYPE:
    
    // PO but no other branch out  
    if (e->net_ptr()->has_primary_output() && rctree_ptr!=nullptr){
      new_rc = _initiate_rctree(rctree_ptr);

      ori_cap= rctree_ptr->rctree_node_ptr(to_node_ptr->pin_ptr()->name())->cap(el,to_rf);

      if(abs_edge_ptr->type() == AbsEdgeType::PO_SEGMENT){
        //CHECK(false); 
      }
      
      new_rc->set_cap(to_node_ptr->pin_ptr()->name(), el, to_rf, load+ori_cap);
      new_rc->update_rc_timing();
      P.first = new_rc->slew(to_node_ptr->pin_ptr()->name(), el, to_rf, si);
      P.second = new_rc->delay(to_node_ptr->pin_ptr()->name(), el, to_rf);  
      
    } 
    else if (rctree_ptr!=nullptr){
      P.first = rctree_ptr->slew(to_node_ptr->pin_ptr()->name(), el, to_rf, si);
      P.second = rctree_ptr->delay(to_node_ptr->pin_ptr()->name(), el, to_rf);  
    }
    // no rctree ptr sout=si delay=0
    else { // no rctree on original circuit
      P.first = si;
      P.second = OT_FLT_ZERO;
    }
    break;

  // Combinational edge type in which case we should consider the following cases:
  //   - case 1: to_node has the same net as the head -> get the load info from the internal
  //             RC tree attached to the abs edge.
  //   - case 2: otherwise -> infer the timing from the timer
  case COMBINATIONAL_EDGE_TYPE: 
     
    if(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr() == e->to_node_ptr()) {
      P.first = timing_arc_ptr->slew(from_rf, to_rf, si, load);
      P.second = timing_arc_ptr->delay(from_rf, to_rf, si, load);
      CHECK(std::isnan(P.second)==0);

    }
    else if (e->to_node_ptr()->pin_ptr()->net_ptr()->has_primary_output()){
      

      if(abs_edge_ptr->type() == AbsEdgeType::PO_SEGMENT){
        //CHECK(false);
      }
      else {
        P.first = timing_arc_ptr->slew(from_rf, to_rf, si, load+to_node_ptr->pin_ptr()->load(el, to_rf));
        P.second = timing_arc_ptr->delay(from_rf, to_rf, si, load+to_node_ptr->pin_ptr()->load(el, to_rf));
        CHECK(std::isnan(P.second)==0);
      }

    } 
    else {

      P.first = timing_arc_ptr->slew(from_rf, to_rf, si, to_node_ptr->pin_ptr()->load(el, to_rf));
      P.second = timing_arc_ptr->delay(from_rf, to_rf, si, to_node_ptr->pin_ptr()->load(el, to_rf));
      CHECK(timing_arc_ptr!=nullptr);
      
      CHECK(std::isnan(P.second)==0)<< si<<" "<<to_node_ptr->pin_ptr()->load(el, to_rf);
     
    }

    break;

  // Exception.
  default:
    LOG(ERROR) << "Fail to infer the timing (EdgeType exception)" << e->edge_type();
    return P;
    break;
  };  

  CHECK(std::isnan(P.second)==0);
  return P;
}

// Function : _infer_rc_timing
// The function infers the timing of a given RC-tree path at any given timing split and transition.
float_pair_t Abstractor::_infer_rc_timing(abs_edge_pt abs_edge_ptr, int el, int rf, float_ct si, float_ct load) {

  float_pair_t P(si, OT_FLT_ZERO);

  if(abs_edge_ptr == nullptr) {
    LOG(ERROR) << "Fail to infer the timing (nullptr exception)";
    return P;
  }

  auto net_ptr = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->net_ptr();

  if(net_ptr->rctree_ptr() == nullptr) {
    return P;
  }

  P.first = net_ptr->rctree_ptr()->slew(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name(), el, rf, si);
  P.second = net_ptr->rctree_ptr()->delay(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->name(), el, rf);

  return P;
}


// Procedure: _abstract_clock_constraint_timing
// The procedure performs the timing abstraction from a given raw constraint arc, which simply
// copies the timing from the cell to the timing on the abs edge.
void_t Abstractor::_abstract_clock_constraint_timing(abs_edge_pt abs_edge_ptr) {

  if(abs_edge_ptr == nullptr) return;


  if(abs_edge_ptr->type() != AbsEdgeType::CLOCK_CONSTRAINT) {
    
    LOG(ERROR) <<  "Incorrect abs_edge type (expect CLOCK COMBINATIONAL)  ";
    
    return;
  }

  pin_pt D=abs_edge_ptr->to_abs_node_ptr()->pin_ptr();
  
  //CHECK(D->test_ptr()!=nullptr);
  
  timing_pt ct;


  // irf: clk_rf   orf: D_rf
  EL_RF_RF_ITER(el, irf, orf) {
    ct = D->test_ptr()->timing_arc_ptr(el)->timing_ptr(irf,orf);

    if(ct!=nullptr){
      // abs_edge_ptr->timing_sense():  from_abs_node_ptr --> clk sink 
      if(abs_edge_ptr->timing_sense()==POSITIVE_UNATE)
        _initiate_clock_constraint_timing(abs_edge_ptr, el, irf, orf);
      else
        _initiate_clock_constraint_timing(abs_edge_ptr, el, !irf, orf);
    }
  }
  // Iterate every cell timing arc and copies it to the abs edge.
  EL_RF_RF_ITER(el, irf, orf) {
     _abstract_slew_slew_timing(abs_edge_ptr, el, irf, orf);
  }
}

void_t Abstractor::_abstract_forward_comb_timing(abs_edge_pt abs_edge_ptr){
  if(abs_edge_ptr == nullptr) return; 
  if(abs_edge_ptr->type() != AbsEdgeType::FORWARD_TRACE_COMB){
    LOG(ERROR) <<  "Incorrect abs_edge type (expect FORWARD_TRACE_COMB)";
    return;
  }
  if(abs_edge_ptr->first_abs_e_ptr() == nullptr){
    LOG(ERROR) <<  "No first abs edge !! " ;
    return;
  }
  //abs_edge_pt first = abs_edge_ptr->first_abs_e_ptr();
  //CHECK(first->to_abs_node_ptr()->num_fanouts()==1)<<"\n" 
  //<<abs_edge_ptr->from_node_ptr()->pin_ptr()->name()<<" -> "<< abs_edge_ptr->to_node_ptr()->pin_ptr()->name()<<"\n"
  //<<abs_edge_ptr->first_abs_e_ptr()->from_node_ptr()->pin_ptr()->name()<<" -> "<<abs_edge_ptr->first_abs_e_ptr()->to_abs_node_ptr()->pin_ptr()->name()<<" "
  //<<abs_edge_ptr->first_abs_e_ptr()->to_abs_node_ptr()->num_fanouts();

  
  //CHECK(abs_edge_ptr->to_abs_node_ptr()->same_load());
 
  EL_RF_ITER(el, irf) {

    timing_pt old_timing_ptr=nullptr;
    timing_pt new_timing_ptr=nullptr;

    switch(abs_edge_ptr->timing_sense()){
      case NON_UNATE:
        if(abs_edge_ptr->from_abs_node_ptr()->is_clock() && abs_edge_ptr->slew_e_ptr()!=nullptr){
          //CHECK(abs_edge_ptr->slew_e_ptr()!=nullptr);
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);
          
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
        
          }

          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
      
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
          }
        }else if(abs_edge_ptr->from_abs_node_ptr()->is_clock()){
          if(irf==RISE){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);
          }
        }
        else{
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
         
        }
        break;
      case POSITIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
        break;
      case NEGATIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
        break;
      default:
        CHECK(false);
        break;
    }
  }

  EL_RF_RF_ITER(el, irf, orf) {
    _abstract_load_slew_timing(abs_edge_ptr, el ,irf, orf);
  } 
  
}
/*
void_t Abstractor::_abstract_input_abs_edges(){
  LOG(INFO) << "Abstracting on input abs edge ...";
  int_t ori_size = abs_edgeset().size();
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  list<abs_node_pt> nodelist;
  list<abs_edge_pt> edgelist;
  unordered_set<abs_edge_pt> removeset;

}
*/
void_t Abstractor::_abstract_list_abs_timing(abs_edge_pt abs_edge_ptr, list<abs_edge_pt>& edgelist){
  if(abs_edge_ptr == nullptr) return;
  if(abs_edge_ptr->type() != AbsEdgeType::LIST_ABS_EDGE){
    LOG(ERROR) <<  "Incorrect abs_edge type (AbsEdgeType::LIST_ABS_EDGE)";
    return;
  }
  //CHECK(abs_edge_ptr->to_abs_node_ptr()->same_load());
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew());
  EL_RF_ITER(el, irf) {

    timing_pt old_timing_ptr=nullptr;
    timing_pt new_timing_ptr=nullptr;

    switch(abs_edge_ptr->timing_sense()){
      case NON_UNATE:
        
        if(abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->is_clock_sink() && abs_edge_ptr->slew_e_ptr()!=nullptr){
          
          if(abs_edge_ptr->slew_e_ptr()==nullptr){
            //LOG(WANRING)<< "slew_e_ptr()==nullptr";
            return ;
          }
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);
          
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
        
          }

          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
      
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
          }
        }else if(abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->is_clock_sink()){
          if(irf==RISE){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);

          }
        }
        else{
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
         
        }
        break;
      case POSITIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
        break;
      case NEGATIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
        break;
      default:
        CHECK(false);
        break;
    }
  }
  EL_RF_RF_ITER(el, irf, orf) {
    _abstract_list_timing(abs_edge_ptr, el ,irf, orf, edgelist);
  }

}
void_t Abstractor::_abstract_back_comb_timing(abs_edge_pt abs_edge_ptr){
  if(abs_edge_ptr == nullptr) return;
  
  if(abs_edge_ptr->type() != AbsEdgeType::BACK_TRACE_COMB){
    LOG(ERROR) <<  "Incorrect abs_edge type (expect BACK_TRACE_COMB) ";
    return;
  }
  if(abs_edge_ptr->first_abs_e_ptr() == nullptr){ 
    LOG(ERROR) <<  "No first abs edge !! " ;
    return;
  }

  //CHECK(abs_edge_ptr->first_abs_e_ptr()->from_abs_node_ptr()->pin_ptr()->node_ptr()->num_fanins()==1);
  //CHECK(abs_edge_ptr->to_abs_node_ptr()->same_load());
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew());

  //abs_node_pt tail = abs_edge_ptr->to_abs_node_ptr();
  //abs_node_pt head = abs_edge_ptr->from_abs_node_ptr();

  

  EL_RF_ITER(el, irf) {

    timing_pt old_timing_ptr=nullptr;
    timing_pt new_timing_ptr=nullptr;

    switch(abs_edge_ptr->timing_sense()){
      case NON_UNATE:
        
        if(abs_edge_ptr->from_abs_node_ptr()->is_clock() && abs_edge_ptr->slew_e_ptr()!=nullptr){
          
          //CHECK(abs_edge_ptr->slew_e_ptr()!=nullptr);
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);
          
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
        
          }

          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
          if(old_timing_ptr!=nullptr){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
      
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
          }
        }else if(abs_edge_ptr->from_abs_node_ptr()->is_clock()){
          if(irf==RISE){
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
            _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(TimingType::RISING_EDGE);

          }
        }
        else{
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
         
        }
        break;
      case POSITIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , irf);
        break;
      case NEGATIVE_UNATE:
          _initiate_back_comb_timing(abs_edge_ptr, el, irf , !irf);
        break;
      default:
        CHECK(false);
        break;
    }
  }

  
  EL_RF_RF_ITER(el, irf, orf) {
    _abstract_load_slew_timing(abs_edge_ptr, el ,irf, orf);   
  }

}



// Procedure: _abstract_clock_combinational_timing
// The procedure performs the timing abstraction from a given raw constraint arc, which simply
// copies the timing from the cell to the timing on the abs edge.
void_t Abstractor::_abstract_clock_combinational_timing(abs_edge_pt abs_edge_ptr) {

  if(abs_edge_ptr == nullptr) return;

  if(abs_edge_ptr->type() != AbsEdgeType::CLOCK_COMBINATIONAL) {
    LOG(ERROR) <<  "Incorrect abs_edge type (expect CLOCK COMBINATIONAL)  ";
    return;
  }

  //CHECK(abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->num_fanins()==1);
   

  //CHECK(abs_edge_ptr->to_abs_node_ptr()->same_load());
  //CHECK(abs_edge_ptr->from_abs_node_ptr()->same_slew());

  edge_pt slew_e = abs_edge_ptr->slew_e_ptr();
  node_pt tail = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  node_pt head = abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();
  if(slew_e == nullptr){
    LOG(WARNING)<<"slew_e nullptr";
    return;

  }

  edge_pt load_e = slew_e ;

  while(tail!= head){
  // CHECK(tail->num_fanins() == 1);
    edge_pt e = tail->fanin().head()->item();
    if(e->type() == COMBINATIONAL_EDGE_TYPE)  {
      load_e = e;
    }
    tail = e->from_node_ptr();
  }
    

  EL_RF_ITER(el, irf) {
    switch(abs_edge_ptr->timing_sense()){
      case POSITIVE_UNATE:
          _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
        break;
      case NEGATIVE_UNATE:
          _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , !irf);
        break;
      case NON_UNATE:
          if(abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->is_clock_sink()){
            if(irf==RISE){
              _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
              _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , !irf);
            }
          }else{
            _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
            _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , !irf);
          }
        break;
      default:
        CHECK(false);
      break;
    }
  }
 
  // Iterate every cell timing arc and copies it to the abs edge.
  EL_RF_RF_ITER(el, irf, orf) {
    _abstract_load_slew_timing(abs_edge_ptr, el, irf, orf);
  }
}



float_t Abstractor::_fixed_creddit(abs_edge_pt abs_edge_ptr, int el, int ckrf, int drf){
  
  abs_node_pt clock_sink_ptr = abs_edge_ptr->from_abs_node_ptr();

  //CHECK(clock_sink_ptr->num_fanins() == 1)<< clock_sink_ptr->pin_ptr()->name();
  abs_edge_pt clock_comb_edge_ptr = clock_sink_ptr->fanin().head()->item();

  if(el == EARLY){
    return 0.0f;
  }
  if(clock_comb_edge_ptr->type() != AbsEdgeType::CLOCK_COMBINATIONAL){
    return 0.0f;
  }

  test_pt test_ptr = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->test_ptr();

  if(test_ptr == nullptr ){
    LOG(WARNING)<<" no test ptr on constraint abs edge";
    return 0.0f;
  }
  clock_tree_pt clock_tree_ptr = timer_ptr()->circuit_ptr()->clock_tree_ptr();
  
  node_pt d = abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr();
  node_pt sink = clock_sink_ptr->pin_ptr()->node_ptr();
  
  node_pt end = clock_comb_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr();

  float_t rat_raw = test_ptr->rat(el,drf);  
  float_t rat_post = d->rat(el, drf);
  float_t credit_all = (el==EARLY)? (rat_raw-rat_post) : (rat_post-rat_raw) ;
    
  //int erf = (clock_comb_edge_ptr->timing_sense() == POSITIVE_UNATE) ? ckrf : !ckrf ;  
  float_t credit_lr = clock_tree_ptr->cppr_credit(el, RISE ,ckrf, end, sink);
  //float_t credit_lf = clock_tree_ptr->cppr_credit(el, FALL ,ckrf, end, sink);
  //if(clock_comb_edge_ptr->timing_sense() == NON_UNATE){
  //  credit_lr = clock_tree_ptr->cppr_credit(el, RISE ,ckrf, end, sink); 
  //  credit_lf = clock_tree_ptr->cppr_credit(el, FALL ,ckrf, end, sink);
  
   
  return credit_all - credit_lr;
/*
  switch(el){
    case EARLY:
      break;
    case LATE:
      break;
        
    default:
      CHECK(false);
      break;
  }*/

}
// Procedure: _abstract_slew_slew_timing
void_t Abstractor::_abstract_slew_slew_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);

  if(timing_ptr == nullptr){ 
    return;
  }

  auto& lut = (orf == RISE) ? timing_ptr->rise_constraint() : timing_ptr->fall_constraint();
  //CHECK(lut.size1()>0&&lut.size2()>0);
  for(unsigned_t i(0); i<lut.size1(); ++i) {
    auto constrained_slew = lut.indices1(i);
    for(unsigned_t j(0); j<lut.size2(); ++j) {
      auto related_slew = lut.indices2(j);

      auto constraint = _infer_clock_const_timing(abs_edge_ptr, el, irf, orf, related_slew, constrained_slew);
      if(lut.size1() ==1 && lut.size2() ==1 ){ 
        float_t credit_end_to_cp = 0.0f;//_fixed_creddit(abs_edge_ptr, el, irf, orf);
        if (el == LATE)  
            constraint =  constraint - credit_end_to_cp;
      }    
      //CHECK(constraint>0)<< to_string(constraint)<<" "<<to_string(related_slew)<<" "<< to_string(constrained_slew);
      
      lut.assign_value(i, j, constraint);
    }
  }

}
void_t Abstractor::_abstract_list_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf, list<abs_edge_pt> & edgelist){
  auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf); 
  if(timing_ptr == nullptr) return;
  auto& dlut = (orf == RISE) ? timing_ptr->cell_rise() : timing_ptr->cell_fall();
  auto& tlut = (orf == RISE) ? timing_ptr->rise_transition() : timing_ptr->fall_transition();

  float_t to_slew = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));

  //assign slew
  tlut.assign_value(0, 0, to_slew);

  float_t delay = OT_FLT_ZERO;
  switch(abs_edge_ptr->type()) {
    case AbsEdgeType::LIST_ABS_EDGE:
        if(abs_edge_ptr->num_nonunate() == 0){
          delay = _infer_list_timing(abs_edge_ptr, el, irf, orf, edgelist);
        }
        else{ 
          delay = _infer_list_timing(abs_edge_ptr, el, irf, orf, edgelist);
        }
      break;
    default:
      CHECK(false);
      break;
  }
  dlut.assign_value(0, 0, delay);
  return;

}
// Procedure: _abstract_load_slew_timing
// The procedure abstracts the timing of a given unate-definite path at any timing split and
// input/output transitions.
void_t Abstractor::_abstract_load_slew_timing(abs_edge_pt abs_edge_ptr, int el, int irf, int orf) {

  auto timing_ptr = abs_edge_ptr->timing_ptr(el, irf, orf);

  if(timing_ptr == nullptr) return;

  // Get the reference to the delay and transition table.
  auto& dlut = (orf == RISE) ? timing_ptr->cell_rise() : timing_ptr->cell_fall();
  auto& tlut = (orf == RISE) ? timing_ptr->rise_transition() : timing_ptr->fall_transition();

  // Assign the delay table values.
  for(unsigned_t l(0); l<dlut.size1(); ++l) {
    for(unsigned_t s(0); s<dlut.size2(); ++s) {

      auto load = dlut.indices1(l);
      auto slew = dlut.indices2(s);

      // Infer the timing through the abs edge.
      auto P = make_pair(slew, OT_FLT_ZERO);
      switch(abs_edge_ptr->type()) {

      case AbsEdgeType::RCTREE_PATH:
        P = _infer_rc_timing(abs_edge_ptr, el, irf, slew, load);
        break;
      case AbsEdgeType::COMBINATIONAL:
        if (abs_edge_ptr->num_nonunate() == 0){ 

          CHECK(std::isnan(slew)==0); 
          P = _infer_comb_timing(abs_edge_ptr, el, irf, orf, slew, load);
        }
        else {
          P = _infer_comb_non_timing_delay(abs_edge_ptr, el, irf, orf, slew, load);
        }
        
        break;
      case AbsEdgeType::CLOCK_COMBINATIONAL:

        if (abs_edge_ptr->num_nonunate() == 0){
          P = _infer_clock_comb_timing(abs_edge_ptr, el, irf, orf, slew, load);
        }else{
          P = _infer_clock_comb_non_timing(abs_edge_ptr, el, irf, orf, slew, load);
        }
        break;
      case AbsEdgeType::PO_SEGMENT:
          P = _infer_po_seg_timing(abs_edge_ptr, el, irf, orf, slew, load);
        break;
      case AbsEdgeType::BACK_TRACE_COMB:
          //CHECK((abs_edge_ptr-> from_abs_node_ptr()->max_slew(el, orf)-abs_edge_ptr-> from_abs_node_ptr()->min_slew(el, orf))<1.0f);

          //slew =  _same_max_min(abs_edge_ptr-> from_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> from_abs_node_ptr()->max_slew(el, orf));
          if(abs_edge_ptr->num_nonunate() == 0){
            P.second = _infer_back_comb_timing(abs_edge_ptr, el, irf, orf, slew, load);
          }
          else{ 
            P.second = _infer_back_comb_non_timing(abs_edge_ptr, el, irf, orf, slew, load);
          }
        break;
      case AbsEdgeType::FORWARD_TRACE_COMB:
          //CHECK((abs_edge_ptr-> from_abs_node_ptr()->max_slew(el, orf)-abs_edge_ptr-> from_abs_node_ptr()->min_slew(el, orf))<1.0f)
          //  << abs_edge_ptr-> from_abs_node_ptr()->pin_ptr()->name();
          //slew =  _same_max_min(abs_edge_ptr-> from_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> from_abs_node_ptr()->max_slew(el, orf));
          if(abs_edge_ptr->num_nonunate() == 0){
            P.second = _infer_forward_comb_timing(abs_edge_ptr, el, irf, orf, slew, load);
          }
          else{ 
            P.second = _infer_forward_comb_non_timing(abs_edge_ptr, el, irf, orf, slew, load);
          }
        break;
      default:
        LOG(ERROR) << "Fail to abstract path timing (abs_edge type exception)";
        break;
      }
      dlut.assign_value(l, s, P.second);
    }
  }

  _shrink_lut(dlut);

  // Assign the transition table values.
  for(unsigned_t l(0); l<tlut.size1(); ++l) {
    for(unsigned_t s(0); s<tlut.size2(); ++s) {

      auto load = tlut.indices1(l);
      auto slew = tlut.indices2(s);

      // Infer the timing through the abs edge.
      auto P = make_pair(slew, OT_FLT_ZERO);
      switch(abs_edge_ptr->type()) {

      case AbsEdgeType::RCTREE_PATH:
        P = _infer_rc_timing(abs_edge_ptr, el, irf, slew, load);
        break;
      case AbsEdgeType::COMBINATIONAL:
        if (abs_edge_ptr->num_nonunate() == 0){
          P = _infer_comb_timing(abs_edge_ptr, el, irf, orf,slew, load);
        }
        else {
          P = _infer_comb_non_timing_slew(abs_edge_ptr, el, irf, orf, slew, load);
        }
        break;
      case AbsEdgeType::CLOCK_COMBINATIONAL:
          P.first = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
        break;
      case AbsEdgeType::BACK_TRACE_COMB:
          P.first = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
        break;
      case AbsEdgeType::FORWARD_TRACE_COMB:
//          if((abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf)-abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf))<1.0f)
//            << abs_edge_ptr-> to_abs_node_ptr()->pin_ptr()->name();
          if(abs_edge_ptr-> to_abs_node_ptr()->same_slew()){
            P.first = _same_max_min(abs_edge_ptr-> to_abs_node_ptr()->min_slew(el, orf), abs_edge_ptr-> to_abs_node_ptr()->max_slew(el, orf));
          }else {
            if(abs_edge_ptr->num_nonunate()==0){
              P.first = _infer_forward_slew_timing(abs_edge_ptr, el, irf, orf, slew, load);
            }else {
              P.first = _infer_forward_slew_non_timing(abs_edge_ptr, el, irf, orf, slew, load);
            }
          }
        break;
      case AbsEdgeType::PO_SEGMENT:
          P = _infer_po_seg_timing(abs_edge_ptr, el, irf, orf, slew, load);
        break;
      default:
        LOG(ERROR) << "Fail to abstract path timing (abs_edge type exception)";
        break;
      }
      tlut.assign_value(l, s, P.first);
    }
  }

  _shrink_lut(tlut);
}

void_t Abstractor::_shrink_lut(timing_lut_rt lut) {

  if(lut.empty()) return;

  size_t size1 = lut.size1();
  size_t size2 = lut.size2();
  float_t val =  lut.table(0, 0);

  if ((size1==1 && size2==1) || fabs(lut.table(size1-1,size2-1) - lut.table(0,0))>1e-2f) {
    return;
  }

  for (unsigned_t i=0; i<size1; ++i) {
    for (unsigned_t j=0; j<size2; ++j) {
      if (fabs(val - lut.table(i,j))>1e-2f) {
        return;
      }
    }
  }

  lut.resize(1, 1);
  lut.assign_value(0,0,val);
}

// Procedure: _abstract_rctree_path_timing
// The procedures takes an abs edge which contains an edge of rctree path (from a rctree root to
// a rctree leaf) and abstracts the RC timing on the edge.
// decided = nullptr
void_t Abstractor::_abstract_rctree_path_timing(abs_edge_pt abs_edge_ptr) {

  if(abs_edge_ptr == nullptr) return;

  if(abs_edge_ptr->type() != AbsEdgeType::RCTREE_PATH) {
    LOG(ERROR) << "Incorrect abs_edge type (expect RCTREE_PATH)";
    return;
  }

  EL_RF_ITER(el, irf) {
    _initiate_load_slew_timing_log(abs_edge_ptr, el, irf, irf);
  }

  EL_RF_ITER(el, irf) {
    _abstract_load_slew_timing(abs_edge_ptr, el, irf, irf);
  }
}
// Procedure: _abstract_combinational_timing_with_decided
void_t Abstractor::_abstract_combinational_timing_with_decided(abs_edge_pt abs_edge_ptr) {
  if(abs_edge_ptr == nullptr) return;

  if(abs_edge_ptr->type() != AbsEdgeType::COMBINATIONAL) {
    LOG(ERROR) << "Incorrect abs_edge type (expect COMINATIONAL)";
    return;
  }
  //LOG(INFO)<< "Start abstract comb timing with deicded";

  //timing_sense_e sense = abs_edge_ptr->timing_sense();

  edge_pt slew_e = abs_edge_ptr->slew_e_ptr();
  edge_pt load_e = _find_load_edge(abs_edge_ptr);

  EL_RF_ITER(el, irf) {
    timing_pt timing_ptr=nullptr;
    timing_pt new_timing_ptr=nullptr;
    //LOG(INFO)<< "    Start init comb timing with deicded d:"<< abs_edge_ptr->decided_abs_node_ptr()->pin_ptr()->name();
    switch(abs_edge_ptr->timing_sense()){
    case POSITIVE_UNATE:
      _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
    break;
    case NEGATIVE_UNATE:
      _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , !irf);
    break;
    case NON_UNATE:
      if(abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->is_clock_sink() ){
        timing_ptr =  slew_e->timing_ptr(el,irf,irf);
        if(timing_ptr!=nullptr){
          _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
          new_timing_ptr = abs_edge_ptr->timing_ptr(el,irf,irf);
          new_timing_ptr->set_timing_type(timing_ptr->timing_type());
        }
        timing_ptr =  slew_e->timing_ptr(el,irf,!irf);
        if(timing_ptr!=nullptr){
          _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf ,!irf);
          new_timing_ptr = abs_edge_ptr->timing_ptr(el,irf,!irf);
          new_timing_ptr->set_timing_type(timing_ptr->timing_type());
        }

      }
      else{
        _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , irf);
        _initiate_comb_timing(abs_edge_ptr,  slew_e,  load_e, el, irf , !irf);
      }
    break;
    default:
      CHECK(false);
    break;
    }

    //LOG(INFO)<< "    End init comb timing with deicded";
  }
  EL_RF_RF_ITER(el, irf,orf) {
    //LOG(INFO)<< "    Start abstract comb timing with deicded";
    _abstract_load_slew_timing(abs_edge_ptr, el, irf, orf);
    //LOG(INFO)<< "    End abstract comb timing with deicded";
  }
// LOG(INFO)<< "End abstract comb timing with deicded";



}

void_t Abstractor::_abstract_back_timing(abs_edge_pt abs_edge_ptr){
  
  if(abs_edge_ptr == nullptr) return;
  switch(abs_edge_ptr->type()) {
    case AbsEdgeType::BACK_TRACE_COMB:
      _abstract_back_comb_timing(abs_edge_ptr);
      break;
    case AbsEdgeType::FORWARD_TRACE_COMB:
      _abstract_forward_comb_timing(abs_edge_ptr);
      break;
    default:
      CHECK(false);
      break;
  }
}
// Procedure: _abstract_timing
// The procedure performs the timing abstraction on a given abs_edge according to different
// edge types that had been defined.
void_t Abstractor::_abstract_timing(abs_edge_pt abs_edge_ptr) {

  if(abs_edge_ptr == nullptr) return;

  switch(abs_edge_ptr->type()) {

  case AbsEdgeType::RCTREE_PATH:
    _abstract_rctree_path_timing(abs_edge_ptr);
    break;
  case AbsEdgeType::COMBINATIONAL:
    _abstract_combinational_timing_with_decided(abs_edge_ptr);
    break;
  case AbsEdgeType::PO_SEGMENT:
    _abstract_po_seg_timing(abs_edge_ptr);
    break;
  case AbsEdgeType::CLOCK_CONSTRAINT:
    _abstract_clock_constraint_timing(abs_edge_ptr);
    break;
  case AbsEdgeType::CLOCK_COMBINATIONAL:
    _abstract_clock_combinational_timing(abs_edge_ptr);
    break;
  default:
    LOG(ERROR) << "Fail to abstract timing (abs_edge type exception)";
    break;
  }
}

// Procedure: _abstract_timing
// This is the main procedure of abstractor. Given a abs graph, the procedure generates the timing
// luts for each abs edge in a parallel manner.
void_t Abstractor::_abstract_timing() {
  LOG(INFO)<<"Abstracting timing ...";

  #pragma omp parallel for schedule(dynamic, 1)
  for(size_t i=0; i<abs_edgeset().num_indices(); ++i) {
    _abstract_timing(abs_edgeset()[i]);
  }

}
// Procedure: _abstract_po_seg_timing
void_t Abstractor::_abstract_po_seg_timing(pin_pt gear, rctree_pt new_rctree_ptr){

  if(gear == nullptr) {
    LOG(ERROR) << "Fail to abstract PO seg timing (nullptr exception)";
    return;
  }

  abs_edge_pt abs_edge_e1_ptr;
  abs_edge_pt new_abs_edge_ptr;
  float_vt slew_indices;
  timing_lut_pt old_tlut;
  timing_lut_pt old_dlut;
  timing_lut_pt new_tlut;
  timing_lut_pt new_dlut;
  timing_pt old_timing_ptr;
  timing_pt new_timing_ptr;

  // a net from primay input to primary output
  if(gear->node_ptr()->num_fanins() == 0) {
    LOG(WARNING) << "Ignore abstracting po segment timing (floating wire)";
    return;
  }
  else {
    // Restore the capacitance.
    for(const auto& e1 : gear->node_ptr()->fanin()) {

      abs_edge_e1_ptr = _find_abs_edge(e1->from_node_ptr()->pin_ptr(), e1->to_node_ptr()->pin_ptr());
      //if(abs_edge_e1_ptr==nullptr) continue; //<< e1->from_node_ptr()->pin_ptr()->name() << " -> " << e1->to_node_ptr()->pin_ptr()->name() ;
      for(const auto& e2 : gear->node_ptr()->fanout()) {
        //CHECK(e2->to_node_ptr()->pin_ptr()!= nullptr);
        EL_RF_RF_ITER(el,irf,orf) {
          auto pin_name = e2->to_node_ptr()->pin_ptr()->name();
          auto abs_node_ptr = abs_node_dict()[abs_node_name(e2->to_node_ptr()->pin_ptr())];
          if (abs_node_ptr != nullptr){
            auto lmax = abs_node_ptr->max_load(el, orf);
            auto lmin = abs_node_ptr->min_load(el, orf);
            if (new_rctree_ptr != nullptr) {
              new_rctree_ptr->set_cap(pin_name, el, orf, (lmin+lmax) / OT_FLT_TWO );
            }
          }
          else if (new_rctree_ptr!=nullptr && e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)!=nullptr){
            auto ori_pin_cap = e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)->cap(el, orf);
            new_rctree_ptr->set_cap(pin_name, el, orf, ori_pin_cap);
          }
        }
      }

      for(const auto& e2 : gear->node_ptr()->fanout()) {
        
        const auto& pin_name = e2->to_node_ptr()->pin_ptr()->name();

        // TODO        
        if(_find_abs_edge(e1->from_node_ptr()->pin_ptr(), e2->to_node_ptr()->pin_ptr()) != nullptr) {
          continue;
        }
        CHECK(e1!=nullptr && e2 !=nullptr);
        new_abs_edge_ptr = _insert_abs_edge(e1->from_node_ptr()->pin_ptr(), e2->to_node_ptr()->pin_ptr(),nullptr, AbsEdgeType::PO_SEGMENT, POSITIVE_UNATE,0);

        EL_RF_RF_ITER(el, irf, orf){

          old_timing_ptr = abs_edge_e1_ptr->timing_ptr(el, irf, orf);

          if (old_timing_ptr == nullptr) continue;

          new_timing_ptr = new_abs_edge_ptr->insert_timing(el,irf,orf);
          new_timing_ptr->set_timing_sense(old_timing_ptr->timing_sense());
          new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());

          old_dlut = (orf == RISE) ? old_timing_ptr->cell_rise_ptr(): old_timing_ptr->cell_fall_ptr();
          old_tlut = (orf == RISE) ? old_timing_ptr->rise_transition_ptr(): old_timing_ptr->fall_transition_ptr();

          new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise_ptr(): new_timing_ptr->cell_fall_ptr();
          new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition_ptr(): new_timing_ptr->fall_transition_ptr();

          if (new_rctree_ptr == nullptr){
            *new_timing_ptr = *old_timing_ptr;
          }
          else {

            //CHECK(e2->net_ptr() != nullptr && e2->net_ptr()->rctree_ptr() != nullptr);

            // idx2 input slew indicies2 are the same
            new_dlut->indices2()= old_dlut->indices2();
            new_tlut->indices2()= old_tlut->indices2();

            // Retrieve the original cap from the net in e2.
            float_ct original_cap = e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)->cap(el, orf);
            //cout<< pin_name<<"  original_cap: "<<original_cap<<endl;

            new_rctree_ptr->set_cap(pin_name, el, orf, original_cap);
            new_rctree_ptr->update_rc_timing();

            //cout<< po_name<<" "<<old_rctree_ptr->load(el,orf)<<" "<<new_rctree_ptr->load(el,orf)<<endl;

            // Get the raw rctree load.
            float_ct raw_load = e2->net_ptr()->rctree_ptr()->load(el,orf);


            // new idx1 (load) = old table idx1 - load
            for(const auto& idx : old_dlut->indices1()) {
              //cout<< old_dlut->indices1(i) <<" "<<original_load <<" "<<old_dlut->indices1(i)-original_load<<endl
              new_dlut->indices1().push_back(idx - raw_load);
            }

            for(const auto& idx : old_tlut->indices1()){
              //cout << old_tlut->indices1(i) <<" "<<load<<endl;
              new_tlut->indices1().push_back(idx - raw_load);
            }

            // resize
            new_dlut->resize(new_dlut->size1(),new_dlut->size2());
            new_tlut->resize(new_tlut->size1(),new_tlut->size2());

            // assign dlut value
            for(unsigned_t l=0; l<new_dlut->size1();++l){
              new_rctree_ptr->set_cap(pin_name, el, orf, new_dlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for(unsigned_t s=0; s<new_dlut->size2();++s){
                new_dlut->assign_value(l,s,new_rctree_ptr->delay(pin_name, el, orf) + old_dlut->table(l, s));
              }
            }

            // assign tlut value
            for (unsigned_t l=0;l<new_tlut->size1();++l){
              new_rctree_ptr->set_cap(pin_name,el,orf,new_tlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for (unsigned_t s=0;s<new_tlut->size2();++s){
                new_tlut->assign_value(l,s, new_rctree_ptr->slew(pin_name, el, orf, old_tlut->table(l, s)));
              }
            }
            new_rctree_ptr->set_cap(pin_name,el,orf,original_cap +
                                    (new_abs_edge_ptr->to_abs_node_ptr()->min_load(el,orf) +
                                     new_abs_edge_ptr->to_abs_node_ptr()->max_load(el,orf)) / OT_FLT_TWO );
          }
        }
      }
      if(abs_edge_e1_ptr!=nullptr)
      _remove_abs_edge(abs_edge_e1_ptr);
    }
  }

}

void_t Abstractor::_build_on_abs_edge_graph(queue<abs_node_pt> &que, list<abs_edge_pt> &edgelist, unordered_set<abs_edge_pt> &removeset, bool_t* visited){
  abs_node_pt u, v;
  while(!que.empty()){
    u = que.front();
    que.pop();
    
    for(const auto & e:u->fanin()){
      if(e->type() != AbsEdgeType::CLOCK_CONSTRAINT){
        abs_edge_pt first_abs_e = e;
        int rf_v = RISE;
        int num_non = e->num_nonunate();
        int level = 1;
        v = e->from_abs_node_ptr();
        edge_pt ck_q = nullptr;
        if(v->is_clock()){
          ck_q = e->slew_e_ptr();
        }
    
        rf_v = (e->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;

        pin_pt related_pin = nullptr;
        if(u->pin_ptr()->is_constrained() && u->pin_ptr()->test_ptr()->related_pin_ptr()!=nullptr){
          related_pin = u->pin_ptr()->test_ptr()->related_pin_ptr();
        }
  
        while(v->num_fanins()==1 
              && v->same_slew() 
              && v->same_load()
              && v->fanin().head()->item()->from_abs_node_ptr()->same_slew()
              && v->fanin().head()->item()->from_abs_node_ptr()->same_load()
              && !v->is_clock() 
              && !v->pin_ptr()->node_ptr()->is_in_clock_tree()
              && !visited[v->pin_ptr()->node_ptr()->idx()] 
              && v->fanin().head()->item()->from_abs_node_ptr()->pin_ptr() != related_pin
              ){
          abs_edge_pt v_fanin = v->fanin().head()->item();
          //CHECK(v_fanin->type()!=AbsEdgeType::CLOCK_CONSTRAINT);
          rf_v = (v_fanin->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
          num_non += v_fanin->num_nonunate();

          //CHECK(v->same_slew())<< v->pin_ptr()->name()<<" -> "<< u->pin_ptr()->name()<<" "<<level;
          //CHECK(v->same_load())<< v->pin_ptr()->name();

          v = v_fanin->from_abs_node_ptr();

          ++level;
          if(v->is_clock()){
            ck_q = v_fanin->slew_e_ptr();
          }

          /*
          if( v->num_fanins()==1 && !v->fanin().head()->item()->from_abs_node_ptr()->same_slew() ){
            break;
          }*/
          if( v->num_fanins()==1){
            
            abs_node_pt next_v = v->fanin().head()->item()->from_abs_node_ptr();
            if( !next_v->same_slew() ){
              break; 
            }
            if( !next_v->same_load() ){
              break;              
            }
            if( next_v->is_clock() &&  u->pin_ptr()->is_constrained() && related_pin == next_v->pin_ptr()){
              break;
            }
          }
        }
           
                      
        if(level>1){
          timing_sense_ce ts = (num_non>0)?NON_UNATE:((rf_v == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);

          abs_edge_pt new_abs_edge = _insert_abs_edge(v->pin_ptr(), u->pin_ptr(), AbsEdgeType::BACK_TRACE_COMB, ts, num_non, first_abs_e);    
          removeset.insert(first_abs_e);

          if(v->is_clock()){
            if(ck_q->from_node_ptr()->pin_ptr()->is_clock_sink()){
              new_abs_edge->set_slew_e_ptr(ck_q);
            }
          }


          edgelist.push_back(new_abs_edge);
          
        }
        if(visited[v->pin_ptr()->node_ptr()->idx()]==false){
          visited[v->pin_ptr()->node_ptr()->idx()]=true;
          if(!v->is_clock() 
          && !v->pin_ptr()->node_ptr()->is_in_clock_tree()
          &&  v->same_slew() && v->same_load() ){
            que.push(v);
          }
        }
      }
    }
  }


}
void_t Abstractor::_abstract_back_timing(list<abs_edge_pt> & edgelist){
   LOG(INFO)<<"Start abstract on abs edge timing...";
  //int_t count = 0;
  #pragma omp parallel
  {
    #pragma omp single
    {
      for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
        #pragma omp task firstprivate(it)
        _abstract_back_timing(*it);
      }
    }
  }
 
  LOG(INFO)<<"Successfully abstract on abs edge timing...";
  
}
void_t Abstractor::_abstract_on_orphan_edges(){
  LOG(INFO)<<"Abstract on orphan abs edge";
  //int_t ori_size = abs_edgeset().size();
  unordered_set<abs_edge_pt> removeset;
  list<abs_edge_pt> edgelist;

  queue<abs_node_pt> que;

  for(const auto& item : abs_node_dict()) {
    if(item.second!=nullptr && item.second->num_fanouts() == 1 && item.second->num_fanins() == 1 && !item.second->pin_ptr()->is_primary_output() &&  !item.second->pin_ptr()->is_primary_input()){
      abs_edge_pt a = item.second->fanin().head()->item();
      abs_edge_pt b = item.second->fanout().head()->item();
      abs_node_pt from = a->from_abs_node_ptr();
      abs_node_pt to = b->to_abs_node_ptr();
      
      if(item.second->same_slew() && item.second->same_load()
         && from->same_slew() 
         && to->same_load() /*&& to->same_slew() */ 
         && a->type() != AbsEdgeType::CLOCK_CONSTRAINT && b->type() != AbsEdgeType::CLOCK_CONSTRAINT 
         && !to->pin_ptr()->node_ptr()->is_in_clock_tree()
         && !(from->is_clock() && to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr() == from->pin_ptr())
         && !(item.second->is_clock() && ((to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr() != nullptr) || (from->num_fanouts() >1) ))
        ){  


        int num_non = a->num_nonunate() + b->num_nonunate();
        timing_sense_ce ts = (num_non > 0)? NON_UNATE: ((a->timing_sense() == b->timing_sense())?POSITIVE_UNATE:NEGATIVE_UNATE);

        //CHECK( to->same_load() && item.second->same_slew() && item.second->same_load() );
        abs_edge_pt new_abs_edge_ptr = _insert_abs_edge(from->pin_ptr(), to->pin_ptr(), AbsEdgeType::FORWARD_TRACE_COMB, ts, num_non, a);
        edgelist.push_back(new_abs_edge_ptr);
       
        //removeset.insert(b);
        if(item.second->is_clock()){
          if(from->num_fanouts() <= 2 ){ // only a and new abs edge
            from->set_clock_sink(true);
          }
          else if(!(to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr() != nullptr)){
            to->set_clock_sink(true);  
          }
        }
        _abstract_back_timing(new_abs_edge_ptr);
        removeset.insert(a);
        
        //cout<<"insert     "<<from->pin_ptr()->name()<<" -> "<<to->pin_ptr()->name()<<endl;
        //cout<<"   remove: "<<a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<a->to_abs_node_ptr()->pin_ptr()->name()<<" "<<a->from_abs_node_ptr()->num_fanins()<<" "
        //                   <<Cellpin::clock_flag(from->is_clock())<<endl;
        //cout<<"   remove: "<<b->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<b->to_abs_node_ptr()->pin_ptr()->name()<<" "<<b->to_abs_node_ptr()->num_fanouts()<<" "
        //                   <<Cellpin::clock_flag(to->is_clock())<<endl;
         
        
      }
    }
  }


  for(const auto& e : removeset){
    _remove_abs_edge(e);

  }


  size_t size = 1;
  size_t prev = size;
  while(size>0){
    size = _cut_forward_orphan_edges();  
  }
  
  _merge_abs_edge(edgelist, AbsEdgeType::FORWARD_TRACE_COMB);
  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }
  
  //ori_size = abs_edgeset().size() - ori_size;
  LOG(INFO)<<"Successfully abstract on orphan abs edge ";//-- reduce "<<  to_string( ori_size - abs_edgeset().size() )<<" abs edges"<<endl;
}
void_t Abstractor::_abstract_cross_not_sameslew_sameload_edges(){
  LOG(INFO)<<"Abstract on abs edge";
  //int_t ori_size = abs_edgeset().size();
  unordered_set<abs_edge_pt> removeset;
  list<abs_edge_pt> edgelist;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  list<abs_node_pt> nodelist;

  for(const auto& item : abs_node_dict()) {
    if(item.second!=nullptr 
    && item.second->num_fanins() > 0
    && item.second->num_fanouts() > 0
    && item.second->num_fanins() <= 2
    && item.second->num_fanouts() <= 2
    && item.second->same_slew()
    && item.second->same_load()
    && !item.second->pin_ptr()->node_ptr()->is_in_clock_tree()
    && !item.second->is_clock()
    ){
      bool_t flag = true;
      int_t non_const_fanout = 0;
      int_t non_const_fanin = 0;
      
      for(const auto & fanout: item.second->fanout()){
        abs_node_pt to = fanout->to_abs_node_ptr();
        if(!to->same_load()){
          non_const_fanout++;
          for(const auto & fanin: item.second->fanin()){
            for(const auto & e:to->fanin()){
              if(e->from_abs_node_ptr() == fanin->from_abs_node_ptr() && e->timing_sense() == NON_UNATE){
                flag = false;
              }
            }
          }
        }
        if(visited[to->pin_ptr()->node_ptr()->idx()]
          ){
          flag = false;
        }
        if(fanout->type() == AbsEdgeType::CLOCK_CONSTRAINT || fanout->type() == AbsEdgeType::PO_SEGMENT){
          flag = false;
        }
        if(to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr()!=nullptr){
          for(const auto & fanin: item.second->fanin()){
            abs_node_pt from = fanin->from_abs_node_ptr();
            if(from->pin_ptr() == to->pin_ptr()->test_ptr()->related_pin_ptr()){
              flag = false;
            }
          }
        }
      }
      for(const auto & fanin: item.second->fanin()){    
        abs_node_pt from = fanin->from_abs_node_ptr();
        if(fanin->type() == AbsEdgeType::CLOCK_CONSTRAINT || fanin->type() == AbsEdgeType::PO_SEGMENT){
          flag = false;
        }
        if(!from->same_slew()){
          non_const_fanin++;
          for(const auto & fanout: item.second->fanout()){
            for(const auto & e:from->fanout()){
              if(e->to_abs_node_ptr() == fanout->to_abs_node_ptr() && e->timing_sense() == NON_UNATE){
                 flag = false;
              }
            }
          }
        }
        if(visited[from->pin_ptr()->node_ptr()->idx()]
          //|| from->pin_ptr()->name() == "inst_153:Y"
          ){
          flag = false;
        }
      }
      //TODO to delete
      if(non_const_fanout>0 && non_const_fanin>0 ){
        flag = false;
      }
      if(flag==true && visited[item.second->pin_ptr()->node_ptr()->idx()]==false
        ){
        nodelist.push_back(item.second);
        visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
      }
    }
  }
  for(const auto & n: nodelist){
    for(const auto & b: n->fanout()){
      abs_node_pt to = b->to_abs_node_ptr();
      for(const auto & a: n->fanin()){
        abs_node_pt from = a->from_abs_node_ptr();        
        int_t num_non = a->num_nonunate()+b->num_nonunate();
        timing_sense_ce ts = (num_non>0)? NON_UNATE: ((a->timing_sense() == b->timing_sense())? POSITIVE_UNATE: NEGATIVE_UNATE);

        abs_edge_pt new_abs_edge_ptr =  _insert_abs_edge(from->pin_ptr(), to->pin_ptr(), AbsEdgeType::CROSS_ABS_EDGE, ts, num_non, a);
        
        //cout<<"insert     "<< from->pin_ptr()->name()<<" -> "<<to->pin_ptr()->name()<<endl; 
        _abstract_cross_timing(new_abs_edge_ptr, a, b);
        edgelist.push_back(new_abs_edge_ptr);
        removeset.insert(a);
        removeset.insert(b);
        
      }
    }
  }

  delete [] visited;  
  for(const auto& e : removeset){
    _remove_abs_edge(e);
  }
  for(const auto & n: nodelist){
    if(n->num_fanouts() == 0 && n->num_fanins() == 0 && !n->pin_ptr()->is_primary_input() && !n->pin_ptr()->is_primary_output() ){
      _remove_abs_node(n);
    }
  }


  size_t size = 1;
  while(size>0){
    size = _cut_orphan_edges();  
  }
  _merge_abs_edge(edgelist, AbsEdgeType::CROSS_ABS_EDGE);
/*
  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }*/
  //ori_size = ori_size - abs_edgeset().size();
  LOG(INFO)<<"Successfully abstract on non same slew load abs edge ";//-- reduce "<<  to_string(ori_size)<<" abs edges"<<endl;
}

void_t Abstractor::_abstract_on_abs_edges(){
  LOG(INFO)<<"Abstract on abs edge";
  //int_t ori_size = abs_edgeset().size();
  unordered_set<abs_edge_pt> removeset;
  list<abs_edge_pt> edgelist;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  queue<abs_node_pt> que;

  for(const auto& item : abs_node_dict()) {
    if(item.second!=nullptr && item.second->num_fanouts() == 0 && !item.second->pin_ptr()->is_primary_output() &&  !item.second->pin_ptr()->is_primary_input()){
     // CHECK(item.second->num_fanins()>0) << item.second->pin_ptr()->name();
      if(item.second->same_slew() && item.second->same_load() && visited[item.second->pin_ptr()->node_ptr()->idx()]==false){
        if(!item.second->is_clock()){
          que.push(item.second);

          //CHECK(item.second->same_slew());
          //CHECK(item.second->same_load());

        }
        visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
      }
    }
    
    else if(item.second->pin_ptr()->is_primary_output()){
      for(const auto & e: item.second->fanin()){
        abs_node_pt from = e->from_abs_node_ptr();
        node_pt from_node = from->pin_ptr()->node_ptr();
        if(from->same_load()&& from->same_slew() && visited[from_node->idx()]==false && !from->pin_ptr()->is_primary_output() && !from->pin_ptr()->is_primary_input()){
          if(!from->pin_ptr()->node_ptr()->is_clock_sink()){
            que.push(from);          
            //CHECK(from->same_slew());
            //CHECK(from->same_load());


          }
          visited[from_node->idx()]=true;
        }
      }
    }

    if(!item.second->same_slew() || !item.second->same_load() || item.second->is_clock()){
      visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
    }
  }

  _build_on_abs_edge_graph(que, edgelist, removeset, visited);

  delete [] visited;  
  _abstract_back_timing(edgelist);
  for(const auto& e : removeset){
    _remove_abs_edge(e);

  }


  size_t size = 1;
  while(size>0){
    size = _cut_orphan_edges();  
  }

  _merge_abs_edge(edgelist, AbsEdgeType::BACK_TRACE_COMB);

  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }
  //ori_size = abs_edgeset().size() - ori_size;
  LOG(INFO)<<"Successfully abstract on abs edge ";/*-- reduce "<<  to_string(ori_size-abs_edgeset().size())<<" abs edges"<<endl;*/
}
// end abstract on abs edges 

//------------------------------------------------------------------
void_t Abstractor::_merge_abs_edge( list<abs_edge_pt> & edgelist , abs_edge_type_ce type ){
  LOG(INFO)<<"Start merging abs edge..";
  unordered_set<abs_edge_pt> removeset;

  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    abs_node_pt from = (*it)->from_abs_node_ptr();
    abs_node_pt to = (*it)->to_abs_node_ptr();
    
    bool_t success;
    switch ((from->num_fanouts()) > (to->num_fanins())){
      case true:
        
        for(auto const e: to->fanin()){
          if(e!=(*it) &&  e->from_abs_node_ptr() == from){
            if(((*it)->idx() < e->idx() ||  (*it)->type()!=type || e->type()!= type ) && ((*it)->type()!=AbsEdgeType::CLOCK_CONSTRAINT &&e->type()!=AbsEdgeType::CLOCK_CONSTRAINT) ){
              success = _merge_abs_edge((*it),e);
              if(success){
                removeset.insert(e);        
  
              }
            }
          }
        }
        break;
      case false: 
        for(auto const e: from->fanout()){
          if(e!=(*it) && e->to_abs_node_ptr() == to){
            if(((*it)->idx() < e->idx() || (*it)->type()!= type || e->type()!= type ) &&  ((*it)->type()!=AbsEdgeType::CLOCK_CONSTRAINT &&e->type()!=AbsEdgeType::CLOCK_CONSTRAINT) ){
              success = _merge_abs_edge((*it),e);
              if(success){
                removeset.insert(e);

              }
            }
          }
        }
        break;
    }

  }

//  LOG(INFO)<<"removeset size = "<<removeset.size()<<endl;
  for(const auto& e : removeset){
    _remove_abs_edge(e);
  }
}
//-------------------------------------------------------
void_t Abstractor::_build_forward_on_abs_edges(queue<abs_node_pt> &que , list<abs_edge_pt> & edgelist, unordered_set<abs_edge_pt> & removeset, bool_t* visited){
  abs_node_pt u;
  abs_node_pt v;
 
  while(!que.empty()){
    u = que.front();
    que.pop();
    for(const auto & e:u->fanout()){
      if(e->type() != AbsEdgeType::CLOCK_CONSTRAINT){
        abs_edge_pt first_abs_e = e;
        int rf_v = RISE;
        int num_non = e->num_nonunate();
        int level = 1;
        v = e->to_abs_node_ptr();   
        edge_pt ck_q = nullptr;
        if(u->is_clock()){
          ck_q = e->slew_e_ptr();
        }
        rf_v = (e->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
        pin_pt ck_pin = nullptr;
        abs_edge_pt second_abs_e = nullptr;
        if(u->is_clock()){
          
          ck_pin = u->pin_ptr();
          if(v->num_fanouts()==1){
            second_abs_e = v->fanout().head()->item();
          }
          // d_pin->name()<<endl;
        }

        while(v->num_fanouts()==1
              && v->same_slew()
              && v->same_load()
              && v->fanout().head()->item()->to_abs_node_ptr()->same_slew()
              && v->fanout().head()->item()->to_abs_node_ptr()->same_load()
              //&& !v->pin_ptr()->is_clock_sink()
              //&& !v->pin_ptr()->node_ptr()->is_in_clock_tree()
              && !visited[v->pin_ptr()->node_ptr()->idx()]
              //&& v->pin_ptr() == d_pin
              ){
          if(v->pin_ptr()->is_constrained() && v->pin_ptr()->test_ptr()->related_pin_ptr() == ck_pin ){
            break;
          }
          abs_edge_pt v_fanout = v->fanout().head()->item();
          //CHECK(v_fanout->type()!=AbsEdgeType::CLOCK_CONSTRAINT);
          rf_v = (v_fanout->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
          num_non += v_fanout->num_nonunate();
      
          //CHECK(v->same_slew())<< v->pin_ptr()->name()<<" -> "<< u->pin_ptr()->name()<<" "<<level;
          //CHECK(v->same_load())<< v->pin_ptr()->name();
          //CHECK(!v->pin_ptr()->node_ptr()->is_in_clock_tree());

          v = v_fanout->to_abs_node_ptr();
          ++level;
          
        }

        if(v->pin_ptr()->is_constrained() && v->pin_ptr()->test_ptr()->related_pin_ptr() == ck_pin){
          //revert 
          if(level > 2){
            num_non = num_non - first_abs_e->num_nonunate();
            rf_v = (first_abs_e->timing_sense() == POSITIVE_UNATE) ? rf_v : !rf_v;
            timing_sense_ce ts = (num_non>0)?NON_UNATE:((rf_v == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);
            //CHECK(second_abs_e!=nullptr);
            abs_edge_pt new_abs_edge = _insert_abs_edge(first_abs_e->to_abs_node_ptr()->pin_ptr(), v->pin_ptr(), AbsEdgeType::FORWARD_TRACE_COMB, ts, num_non, second_abs_e); 
            removeset.insert(second_abs_e);
            edgelist.push_back(new_abs_edge);
          } 
        }else if(level>1){  
          timing_sense_ce ts = (num_non>0)?NON_UNATE:((rf_v == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);
          abs_edge_pt new_abs_edge = _insert_abs_edge(u->pin_ptr(), v->pin_ptr(), AbsEdgeType::FORWARD_TRACE_COMB, ts, num_non, first_abs_e);
          removeset.insert(first_abs_e);
          //cout<<"insert     "<< u->pin_ptr()->name()<<" -> "<<v->pin_ptr()->name()<<endl;
          //cout<<"   remove: "<<first_abs_e->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<first_abs_e->to_abs_node_ptr()->pin_ptr()->name()<<" "<<first_abs_e->to_abs_node_ptr()->num_fanouts()<<endl;
          if(u->is_clock()){
            if(ck_q!=nullptr && ck_q->from_node_ptr()->pin_ptr()->is_clock_sink()){
              new_abs_edge->set_slew_e_ptr(ck_q);
            }
          }

          edgelist.push_back(new_abs_edge);

        }
        if(visited[v->pin_ptr()->node_ptr()->idx()]==false){
          visited[v->pin_ptr()->node_ptr()->idx()]=true;
          if(v->same_slew() && v->same_load()){
            que.push(v);
          }
        }
        
      }
    }

  }


}
//--------------------------------------------------------
void_t Abstractor::_abstract_forward_on_abs_edges(){
  LOG(INFO)<<"Abstract forward on abs edge"; 
  //int_t ori_size = abs_edgeset().size();
  unordered_set<abs_edge_pt> removeset;
  list<abs_edge_pt> edgelist;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  queue<abs_node_pt> que;
  for(const auto& item : abs_node_dict()) {
    if(!item.second->same_slew()){
      
      for(const auto & e: item.second->fanout()){
        abs_node_pt to = e->to_abs_node_ptr();
        if(to->same_slew() && to->same_load() && visited[to->pin_ptr()->node_ptr()->idx()]==false && !to->pin_ptr()->node_ptr()->is_in_clock_tree()){
          que.push(to);
          visited[to->pin_ptr()->node_ptr()->idx()]=true;
        }  
      }
      visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
    }

    if(item.second->is_clock()){
      if(item.second->same_slew() && item.second->same_load() && visited[item.second->pin_ptr()->node_ptr()->idx()]==false){
        que.push(item.second);
         visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
      }
      visited[item.second->pin_ptr()->node_ptr()->idx()]=true;
    }
  
  }
  
  _build_forward_on_abs_edges(que, edgelist, removeset, visited);

  delete [] visited; 
  _abstract_back_timing(edgelist);
  for(const auto& e : removeset){
    _remove_abs_edge(e);
  }
  size_t size = 1;
  while(size>0){
    size = _cut_orphan_edges();
  }
  size = 1;
  while(size>0){
    size = _cut_forward_orphan_edges();
  }
  _merge_forward_abs_edge(edgelist);
  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }
  LOG(INFO)<<"Successfully abstract forward on abs edge ";//-- reduce "<<  to_string(ori_size -abs_edgeset().size())<<" abs edges"<<endl;

}

void_t Abstractor::_merge_forward_abs_edge( list<abs_edge_pt> & edgelist ){
  LOG(INFO)<<"Start merging forward abs edge..";
  unordered_set<abs_edge_pt> removeset;
  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    abs_node_pt from = (*it)->from_abs_node_ptr();
    abs_node_pt to = (*it)->to_abs_node_ptr();
    bool_t success;
    switch ((from->num_fanouts()) > (to->num_fanins())){
      case true:
        for(auto const e: to->fanin()){
          if(e!=(*it) &&  e->from_abs_node_ptr() == from){
            if((*it)->idx() < e->idx() ||  (*it)->type()!=AbsEdgeType::FORWARD_TRACE_COMB || e->type()!=AbsEdgeType::FORWARD_TRACE_COMB){
              success = _merge_abs_edge((*it),e);
              if(success){
                removeset.insert(e);
              }
            }
          }
        }
        break;
      case false:
        for(auto const e: from->fanout()){
          if(e!=(*it) && e->to_abs_node_ptr() == to){
            if((*it)->idx() < e->idx() ||  (*it)->type()!=AbsEdgeType::FORWARD_TRACE_COMB || e->type()!=AbsEdgeType::FORWARD_TRACE_COMB){
              success = _merge_abs_edge((*it),e);
              if(success){
                removeset.insert(e);
              }
            }
          }
        }
        break;
    }
  }
  for(const auto& e : removeset){
    _remove_abs_edge(e);
  }

}




bool_t Abstractor::_merge_abs_edge(abs_edge_pt a, abs_edge_pt b){
  if(a ==nullptr || b ==nullptr){
    LOG(WARNING)<<"try to merge null abs edges";
    return false;
  }
  if(a->from_abs_node_ptr() != b->from_abs_node_ptr()){
    LOG(WARNING)<<"try to merge abs edges : from abs node inconssist";
    return false;
  }

  if(a->to_abs_node_ptr() != b->to_abs_node_ptr()){
    LOG(WARNING)<<"try to merge abs edges : to abs node inconssist";
    return false;
  }
/*
  if(!a->from_abs_node_ptr()->same_slew() || !a->to_abs_node_ptr()->same_load()){
    LOG(WARNING)<<"try to merge abs edges : not scalar";
    return false;
  }
*/
//    if(b->timing_sense() == NON_UNATE){
//      a->set_timing_sense(NON_UNATE);
//    }
  if(a->type() == AbsEdgeType::CLOCK_CONSTRAINT){
    LOG(WARNING)<<"try to merge abs edges : clock const type";
    return false;
  }
  bool_t success = false ;

  EL_RF_RF_ITER(el, irf, orf){
    timing_pt timing_a = a->timing_ptr(el, irf, orf);
    timing_pt timing_b = b->timing_ptr(el, irf, orf);
    if(timing_a !=nullptr && timing_b != nullptr){  
      success = success | _merge_abs_edge(a, b, el, irf, orf);
    } 
    
  }
 /* 
  if(a->timing_sense()!=b->timing_sense()){
    cout<<  a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "<<a->to_abs_node_ptr()->pin_ptr()->name()<<endl;
    cout<< a->timing_sense() <<" "<< b->timing_sense()<<endl;
    if(a->timing_sense() == POSITIVE_UNATE && b->timing_sense() == NEGATIVE_UNATE){
      a->set_timing_sense(NON_UNATE);
      a->set_num_nonunate(1) ;
    }else if(b->timing_sense() == NON_UNATE){
      a->set_timing_sense(NON_UNATE);
      a->set_num_nonunate( b->num_nonunate());
    }
  }
  */
  return success;

}

void_t Abstractor::_merge_abs_edge_fall_slew(timing_pt timing_a, timing_pt timing_b, int el, int irf, int orf, size_t idx1, size_t idx2){

  if( !timing_a->fall_transition().empty() && !timing_b->fall_transition().empty()){
    switch (el){
    case EARLY:
      if(timing_a->fall_transition().table(idx1, idx2) > timing_b->fall_transition().table(idx1, idx2)){
        timing_a->fall_transition().assign_value(idx1, idx2, timing_b->fall_transition().table(idx1, idx2));
      }                                      
     
      break;
    case LATE:
      if(timing_a->fall_transition().table(idx1, idx2) < timing_b->fall_transition().table(idx1, idx2)){
        timing_a->fall_transition().assign_value(idx1, idx2, timing_b->fall_transition().table(idx1, idx2));
      }
      break;
    default:
      CHECK(false);
      break;
    }
  }else if(!timing_b->fall_transition().empty()){ // a fall transition empty
    timing_a->fall_transition().assign_value(idx1, idx2, timing_b->fall_transition().table(idx1, idx2));
  }

}

void_t Abstractor::_merge_abs_edge_rise_slew(timing_pt timing_a, timing_pt timing_b, int el, int irf, int orf, size_t idx1, size_t idx2){


  // slew
  if( !timing_a->rise_transition().empty() && !timing_b->rise_transition().empty()){
    switch (el){
    case EARLY:
      if(timing_a->rise_transition().table(idx1, idx2) > timing_b->rise_transition().table(idx1, idx2)){
        timing_a->rise_transition().assign_value(idx1, idx2, timing_b->rise_transition().table(idx1, idx2));
      }                                          
      break;
    case LATE:
      if(timing_a->rise_transition().table(idx1, idx2) < timing_b->rise_transition().table(idx1, idx2)){
        timing_a->rise_transition().assign_value(idx1, idx2, timing_b->rise_transition().table(idx1, idx2));
      }
      break;
    default:
      CHECK(false);
      break;
    }
  }else if(!timing_b->rise_transition().empty()){ // a rise transistion empty
    timing_a->rise_transition().assign_value(idx1, idx2, timing_b->rise_transition().table(idx1, idx2));
  }

}
void_t Abstractor::_merge_abs_edge_fall_delay(timing_pt timing_a, timing_pt timing_b, int el, int irf, int orf, size_t idx1, size_t idx2){

  if( !timing_a->cell_fall().empty() && !timing_b->cell_fall().empty()){
    switch (el){
    case EARLY:
      if(timing_a->cell_fall().table(idx1, idx2) > timing_b->cell_fall().table(idx1, idx2)){
        timing_a->cell_fall().assign_value(idx1, idx2, timing_b->cell_fall().table(idx1, idx2));
      }                                      
     
      break;
    case LATE:
      if(timing_a->cell_fall().table(idx1, idx2) < timing_b->cell_fall().table(idx1, idx2)){
        timing_a->cell_fall().assign_value(idx1, idx2, timing_b->cell_fall().table(idx1, idx2));
      }
      break;
    default:
      CHECK(false);
      break;
    }
  }else if(!timing_b->cell_fall().empty()){ // a cell fall empty
    timing_a->cell_fall().assign_value(idx1, idx2, timing_b->cell_fall().table(idx1, idx2));
  }
}
void_t Abstractor::_merge_abs_edge_rise_delay(timing_pt timing_a, timing_pt timing_b, int el, int irf, int orf, size_t idx1, size_t idx2){
   

  if( !timing_a->cell_rise().empty() && !timing_b->cell_rise().empty()){
    switch (el){
    case EARLY:
      if(timing_a->cell_rise().table(idx1, idx2) > timing_b->cell_rise().table(idx1, idx2)){
        timing_a->cell_rise().assign_value(idx1, idx2, timing_b->cell_rise().table(idx1, idx2));
      }                                          
      break;
    case LATE:
      if(timing_a->cell_rise().table(idx1, idx2) < timing_b->cell_rise().table(idx1, idx2)){
        timing_a->cell_rise().assign_value(idx1, idx2, timing_b->cell_rise().table(idx1, idx2));
      }
      break;
    default:
      CHECK(false);
      break;
    }
  }else if(!timing_b->cell_rise().empty()){ // a cell rise empty
    timing_a->cell_rise().assign_value(idx1, idx2, timing_b->cell_rise().table(idx1, idx2));
  }


  
}
bool_t Abstractor::_merge_abs_edge(abs_edge_pt a, abs_edge_pt b, int el, int irf, int orf){
  
  timing_pt timing_a = a->timing_ptr(el, irf, orf);
  timing_pt timing_b = b->timing_ptr(el, irf, orf);
  if(timing_a ==nullptr || timing_b == nullptr) {
    LOG(WARNING)<<" timing pt nullptr";
    return false;
  }

  bool_t flag = false;
  if(!timing_b->cell_rise().empty() && timing_a->cell_rise().empty()){
    timing_a->cell_rise().resize(timing_b->cell_rise().size1(),timing_b->cell_rise().size2());  
    flag = true;
  }
  if(!timing_b->cell_fall().empty() && timing_a->cell_fall().empty()){
    timing_a->cell_fall().resize(timing_b->cell_fall().size1(),timing_b->cell_fall().size2());
    flag = true;
  }
  if(!timing_b->rise_transition().empty() && timing_a->rise_transition().empty()){
    timing_a->rise_transition().resize(timing_b->rise_transition().size1(),timing_b->rise_transition().size2());
    flag = true;
  }
  if(!timing_b->fall_transition().empty() && timing_a->fall_transition().empty()){
    timing_a->fall_transition().resize(timing_b->fall_transition().size1(),timing_b->fall_transition().size2());
    flag = true;
  }
  /*if(flag && timing_a->timing_type() != timing_b->timing_type()){
    
    timing_a->set_timing_type(timing_b->timing_type());
  }*/
 
  if (a->to_abs_node_ptr()->same_load() && a->from_abs_node_ptr()->same_load()){
    _merge_abs_edge_rise_delay(timing_a, timing_b, el, irf, orf, 0, 0);
    _merge_abs_edge_fall_delay(timing_a, timing_b, el, irf, orf, 0, 0);
    _merge_abs_edge_rise_slew(timing_a, timing_b, el, irf, orf, 0, 0);
    _merge_abs_edge_fall_slew(timing_a, timing_b, el, irf, orf, 0, 0);

  }else{

    //CHECK(timing_a->cell_rise().size1() == timing_b->cell_rise().size1());
    //CHECK(timing_a->cell_rise().size2() == timing_b->cell_rise().size2());
 
    for(size_t idx1=0; idx1<timing_a->cell_rise().size1(); ++idx1) {
      for(size_t idx2=0 ; idx2<timing_a->cell_rise().size2(); ++idx2){
      //CHECK(fabs(timing_a->cell_rise().indices1(idx1) - timing_b->cell_rise().indices1(idx1) )<0.01f);

       _merge_abs_edge_rise_delay(timing_a, timing_b, el, irf, orf, idx1, idx2);

      }
    }
    //CHECK(timing_a->cell_fall().size1() == timing_b->cell_fall().size1());
    //CHECK(timing_a->cell_fall().size2() == timing_b->cell_fall().size2());

    for(size_t idx1=0; idx1<timing_a->cell_fall().size1(); ++idx1) {
      for(size_t idx2=0 ; idx2<timing_a->cell_fall().size2(); ++idx2){
        //CHECK(fabs(timing_a->cell_fall().indices1(idx1) - timing_b->cell_fall().indices1(idx1) )<0.01f);
        _merge_abs_edge_fall_delay(timing_a, timing_b, el, irf, orf, idx1, idx2);
        
          
        
      }
    }

    //CHECK(timing_a->rise_transition().size1() == timing_b->rise_transition().size1());
    //CHECK(timing_a->rise_transition().size2() == timing_b->rise_transition().size2());

    for(size_t idx1=0; idx1<timing_a->rise_transition().size1(); ++idx1) { 
      for(size_t idx2=0 ; idx2<timing_a->rise_transition().size2(); ++idx2){
/*
        CHECK(fabs(timing_a->rise_transition().indices1(idx1) - timing_b->rise_transition().indices1(idx1) )<0.01f) 
        << timing_a->rise_transition().indices1(idx1)<<" "
        << timing_b->rise_transition().indices1(idx1)
        <<a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "
        <<a->to_abs_node_ptr()->pin_ptr()->name()<< " "<<a->from_abs_node_ptr()->same_slew();
        CHECK(fabs(timing_a->rise_transition().indices2(idx2) - timing_b->rise_transition().indices2(idx2) )<0.01f)
        <<timing_a->rise_transition().indices2(idx2)<<" "
        <<timing_b->rise_transition().indices2(idx2)<<" "
        <<a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "
        <<a->to_abs_node_ptr()->pin_ptr()->name()<< " "<<a->from_abs_node_ptr()->same_slew()<<" "
        <<a->from_abs_node_ptr()->min_slew(el, irf)<<" "
        <<a->from_abs_node_ptr()->max_slew(el, irf);
*/
        _merge_abs_edge_rise_slew(timing_a, timing_b, el, irf, orf, idx1, idx2);
      }
    }
    //CHECK(timing_a->fall_transition().size1() == timing_b->fall_transition().size1());
    //CHECK(timing_a->fall_transition().size2() == timing_b->fall_transition().size2());

    for(size_t idx1=0; idx1<timing_a->fall_transition().size1(); ++idx1) {
      for(size_t idx2=0 ; idx2<timing_a->fall_transition().size2(); ++idx2){
/*
        CHECK(fabs(timing_a->fall_transition().indices1(idx1) - timing_b->fall_transition().indices1(idx1) )< 0.01f)
        << timing_a->fall_transition().indices1(idx1) <<" "
        <<timing_b->fall_transition().indices1(idx1)
        <<a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "
        <<a->to_abs_node_ptr()->pin_ptr()->name()<< " "<<a->from_abs_node_ptr()->same_slew();
        CHECK(fabs(timing_a->fall_transition().indices2(idx2) - timing_b->fall_transition().indices2(idx2) )< 0.01f)
        <<timing_a->fall_transition().indices2(idx2)<<" "
        << timing_b->fall_transition().indices2(idx2) 
        <<a->from_abs_node_ptr()->pin_ptr()->name()<<" -> "
        <<a->to_abs_node_ptr()->pin_ptr()->name()<< " "<<a->from_abs_node_ptr()->same_slew();
*/
        _merge_abs_edge_fall_slew(timing_a, timing_b, el, irf, orf, idx1, idx2);
      }
    }
  }

  
  return true;
  
}
//--------------------------------------------------------------------------------------
void_t Abstractor::_abstract_po_seg_timing(abs_edge_pt abs_edge_ptr){
  if(abs_edge_ptr == nullptr) return;

  if(abs_edge_ptr->type() != AbsEdgeType::PO_SEGMENT) {
    LOG(ERROR) << "Incorrect abs_edge type (expect COMINATIONAL)";
    return;
  }
  //LOG(INFO)<< "Start abstract comb timing with deicded";


  //CHECK(abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr()->num_fanouts()==1);
  
  edge_pt slew_e = abs_edge_ptr->slew_e_ptr();//from_abs_node_ptr()->pin_ptr()->node_ptr()->fanout().head()->item();
  

  EL_RF_RF_ITER(el, irf, orf) {
    
    timing_pt timing_ptr =  slew_e->timing_ptr(el,irf,orf);
    if(timing_ptr!=nullptr){
     
      _initiate_po_seg_timing(abs_edge_ptr,  slew_e, el, irf , orf);
      timing_pt new_timing_ptr = abs_edge_ptr->timing_ptr(el,irf,orf);
      new_timing_ptr->set_timing_type(timing_ptr->timing_type());
      
    }

  }
 

/*  
  EL_RF_ITER(el, irf) {
    timing_pt old_timing_ptr= nullptr;
    timing_pt new_timing_ptr=nullptr;

    switch(abs_edge_ptr->timing_sense()){
      case POSITIVE_UNATE:
          _initiate_po_seg_timing(abs_edge_ptr,  slew_e, el, irf , irf);
        break;
      case NEGATIVE_UNATE:
          _initiate_po_seg_timing(abs_edge_ptr,  slew_e,  el, irf , !irf);
        break;
      case NON_UNATE:
        
        if(abs_edge_ptr->from_abs_node_ptr()->is_clock() && abs_edge_ptr->slew_e_ptr()!=nullptr){
          //CHECK(abs_edge_ptr->slew_e_ptr()!=nullptr);
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);

          if(old_timing_ptr!=nullptr){
            _initiate_po_seg_timing(abs_edge_ptr, slew_e,  el, irf , irf);
            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());

          }

          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
          if(old_timing_ptr!=nullptr){
            _initiate_po_seg_timing(abs_edge_ptr, slew_e,  el, irf , !irf);

            new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
            new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
          }
        }else if(abs_edge_ptr->from_abs_node_ptr()->is_clock()){
         if(irf==RISE){
            old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);
            if(old_timing_ptr!=nullptr){
              _initiate_po_seg_timing(abs_edge_ptr, slew_e, el, irf , irf);
              new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
              new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
            }
            old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
            if(old_timing_ptr!=nullptr){
              _initiate_po_seg_timing(abs_edge_ptr, slew_e, el, irf , !irf);  
              new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
              new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
            }
          }
        }
        else{
          old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,irf);
            if(old_timing_ptr!=nullptr){
              _initiate_po_seg_timing(abs_edge_ptr, slew_e, el, irf , irf);
              new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, irf);
              new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
            }
            old_timing_ptr =  abs_edge_ptr->slew_e_ptr()->timing_ptr(el,irf,!irf);
            if(old_timing_ptr!=nullptr){
              _initiate_po_seg_timing(abs_edge_ptr, slew_e, el, irf , !irf);
              new_timing_ptr = abs_edge_ptr->timing_ptr(el, irf, !irf);
              new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());
            }


        }
        break;
      default:
        CHECK(false);
      break;
    }
  }

*/
  EL_RF_RF_ITER(el, irf,orf){
    timing_pt timing_ptr =  slew_e->timing_ptr(el,irf,orf);
    if(timing_ptr!=nullptr){
      _abstract_load_slew_timing(abs_edge_ptr, el,irf,orf);
    }
  }
   

}
// Procedure: _abstract_po_seg_timing
void_t Abstractor::_abstract_po_seg_timing(abs_edge_pt new_abs_edge_ptr, pin_pt gear, rctree_pt new_rctree_ptr) {

  if(gear == nullptr) {
    LOG(ERROR) << "Fail to abstract PO seg timing (nullptr exception)";
    return;
  }

  float_vt slew_indices;
  timing_lut_pt old_tlut;
  timing_lut_pt old_dlut;
  timing_lut_pt new_tlut;
  timing_lut_pt new_dlut;
  timing_pt old_timing_ptr;
  timing_pt new_timing_ptr;

  // a net from primay input to primary output
  if(gear->node_ptr()->num_fanins() == 0) {
    LOG(WARNING) << "Ignore abstracting po segment timing (floating wire)";
    return;
  } else {

    // Restore the capacitance.
//    for(const auto& e1 : gear->node_ptr()->fanin()) {
        //CHECK(new_abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr()->num_fanouts()==1);
        edge_pt e1 = new_abs_edge_ptr->from_abs_node_ptr()->pin_ptr()->node_ptr()->fanout().head()->item();
//      for(const auto& e2 : gear->node_ptr()->fanout()) {
        //CHECK(e2->to_node_ptr()->pin_ptr()!= nullptr);
        //CHECK(new_abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->num_fanins()==1);
        edge_pt e2 =  new_abs_edge_ptr->to_abs_node_ptr()->pin_ptr()->node_ptr()->fanin().head()->item();
          
        EL_RF_RF_ITER(el,irf,orf) {
          auto pin_name = e2->to_node_ptr()->pin_ptr()->name();
          auto abs_node_ptr = abs_node_dict()[abs_node_name(e2->to_node_ptr()->pin_ptr())];
          if (abs_node_ptr != nullptr) {
            auto lmax = abs_node_ptr->max_load(el, orf);
            auto lmin = abs_node_ptr->min_load(el, orf);
            if (new_rctree_ptr != nullptr) {
              new_rctree_ptr->set_cap(pin_name, el, orf, (lmin+lmax) / OT_FLT_TWO );
            }
          } else if (new_rctree_ptr!=nullptr && e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)!=nullptr) {
            auto ori_pin_cap = e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)->cap(el, orf);
            new_rctree_ptr->set_cap(pin_name, el, orf, ori_pin_cap);
          }
        }
//      }


        const auto& pin_name = e2->to_node_ptr()->pin_ptr()->name();



        EL_RF_RF_ITER(el, irf, orf) {

          old_timing_ptr = e1->timing_ptr(el, irf, orf);

          if (old_timing_ptr == nullptr) continue;

          new_timing_ptr = new_abs_edge_ptr->insert_timing(el,irf,orf);
          timing_sense_e ts = (irf == orf)? POSITIVE_UNATE:NEGATIVE_UNATE;
          new_timing_ptr->set_timing_sense(ts);
          new_timing_ptr->set_timing_type(old_timing_ptr->timing_type());

          old_dlut = (orf == RISE) ? old_timing_ptr->cell_rise_ptr(): old_timing_ptr->cell_fall_ptr();
          old_tlut = (orf == RISE) ? old_timing_ptr->rise_transition_ptr(): old_timing_ptr->fall_transition_ptr();

          new_dlut = (orf == RISE) ? new_timing_ptr->cell_rise_ptr(): new_timing_ptr->cell_fall_ptr();
          new_tlut = (orf == RISE) ? new_timing_ptr->rise_transition_ptr(): new_timing_ptr->fall_transition_ptr();

          if (new_rctree_ptr == nullptr) {
            *new_timing_ptr = *old_timing_ptr;
          } else {

            //CHECK(e2->net_ptr() != nullptr && e2->net_ptr()->rctree_ptr() != nullptr);

            // idx2 input slew indicies2 are the same
            new_dlut->indices2()= old_dlut->indices2();
            new_tlut->indices2()= old_tlut->indices2();

            // Retrieve the original cap from the net in e2.
            float_ct original_cap = e2->net_ptr()->rctree_ptr()->rctree_node_ptr(pin_name)->cap(el, orf);

            new_rctree_ptr->set_cap(pin_name, el, orf, original_cap);
            new_rctree_ptr->update_rc_timing();


            // Get the raw rctree load.
            float_ct raw_load = e2->net_ptr()->rctree_ptr()->load(el,orf);
            //float_vt indecies1 = _infer_range_timing(old_dlut->indices1(),)
            // new idx1 (load) = old table idx1 - load
            for(const auto& idx : old_dlut->indices1()) {
              new_dlut->indices1().push_back(idx - raw_load);
            }

            for(const auto& idx : old_tlut->indices1()) {
              new_tlut->indices1().push_back(idx - raw_load);
            }

            // resize
            new_dlut->resize(new_dlut->size1(),new_dlut->size2());
            new_tlut->resize(new_tlut->size1(),new_tlut->size2());

            // assign dlut value
            for(unsigned_t l=0; l<new_dlut->size1(); ++l) {
              new_rctree_ptr->set_cap(pin_name, el, orf, new_dlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for(unsigned_t s=0; s<new_dlut->size2(); ++s) {
                new_dlut->assign_value(l,s,new_rctree_ptr->delay(pin_name, el, orf) + old_dlut->table(l, s));
              }
            }

            // assign tlut value
            for (unsigned_t l=0; l<new_tlut->size1(); ++l) {
              new_rctree_ptr->set_cap(pin_name,el,orf,new_tlut->indices1(l)+original_cap);
              new_rctree_ptr->update_rc_timing();
              for (unsigned_t s=0; s<new_tlut->size2(); ++s) {
                new_tlut->assign_value(l,s, new_rctree_ptr->slew(pin_name, el, orf, old_tlut->table(l, s)));
              }
            }
            new_rctree_ptr->set_cap(pin_name,el,orf,original_cap +
                                    (new_abs_edge_ptr->to_abs_node_ptr()->min_load(el,orf) +
                                     new_abs_edge_ptr->to_abs_node_ptr()->max_load(el,orf)) / OT_FLT_TWO );
          }
        }
  }

}

// Procedure: write_celllib
// The function that write the abstracted macro into two celllib, one for early and one for late.
void_t Abstractor::write_celllib(string_crt early_lib_fpath, string_crt late_lib_fpath) {

  LOG(INFO) << "Writing abstracted macro to " + early_lib_fpath + "/" + late_lib_fpath;

  // Invoke two tasks to write the two cell libraries through concurrent OpenMP.
  #pragma omp parallel
  {
    #pragma omp single
    {
      // Task 1: Early library.
      #pragma omp task
      { _write_celllib(EARLY, early_lib_fpath); }

      // Task 2: Late library.
      #pragma omp task
      { _write_celllib(LATE, late_lib_fpath); }
    }
  }  // Implicit synchronization barrier. ---------------------------------------------------------

  LOG(INFO) << "Successfully wrote abstracted macro";
}

// Procedure: _write_celllib
// The main procedure that writes the abstracted macro into a celllib in a given early or late
// timing split. Notice that we in order to initiate a celllib, the following tasks have to be
// done:
//
// 1. Set the type of the celllib to be either early or late.
// 2. Create lut template objects.
// 2. Create a cell and generate all related field.
//
void_t Abstractor::_write_celllib(int el, string_crt celllib_fpath) {

  ofstream ofs(celllib_fpath, std::ios::out | std::ios::trunc);
  LOG_IF(FATAL, !ofs.is_open()) << "Fail to open " + celllib_fpath;
  //FILE* fptr = fopen(celllib_fpath.c_str(), "w");
  //LOG_IF(FATAL, fptr==nullptr) << "Fail to open " + celllib_fpath;

  // Write the comment.
  ofs << "/* Generated by " << PACKAGE_STRING << " - " << __time_info() << " */\n\n";
  //fprintf(fptr, "/* Generated by %s - %s */\n\n", PACKAGE_STRING, __time_info().c_str());

  // Write library name.
  ofs << "library (\"Macro\") {\n";
  //fprintf(fptr, "library (\"Macro\") {\n");

  // Insert the lut template. Notice that the lut template is initiated through copy assignment.
  for(const auto& item : *lut_template_dict_ptr()) {
    if(item.second->size1() != 1 || item.second->size2() != 1 )
    ofs << *(item.second);
    //item.second->write(fptr);
  }

  // Insert the cell. Ideally there should be only one cell.
  ofs << "  cell (\"" << macro_name() << "\") {\n";
  //fprintf(fptr, "  cell (\"%s\") {\n", macro_name().c_str());

  // Iterate over the abs node set and insert them as cellpins.
  for(const auto& item : abs_node_dict()) {

    // Write the cellpin name.
    ofs << "    pin (\"" << item.second->name() << "\") {\n";
    //fprintf(fptr, "    pin (\"%s\") {\n", item.second->name().c_str());

    // Write the pin direction.
    ofs << "      direction : " << Cellpin::direction_name(item.second->direction()) << ";\n";
    //fprintf(fptr, "      direction : %s;\n", Cellpin::direction_name(item.second->direction()).c_str());

    // Write the pin capacitance.
    if(item.second->cap(el)!=0){
      ofs << "      capacitance : " << item.second->cap(el) << ";\n";
    }
    //fprintf(fptr, "      capacitance : %f;\n", item.second->cap(el));

    // Write the clock flag.
    if(item.second->is_clock()){
    ofs << "      clock : " << Cellpin::clock_flag(item.second->is_clock()) << "\n";
    }
    //fprintf(fptr, "      clock : %s\n", Cellpin::clock_flag(item.second->is_clock()).c_str());

    // Iterate all fanin abs edges and insert associated timings. Notice that the timings to be
    // written into the library are initiated through the copy assignment. Before writing to the
    // celllib, timings with identical timing type should be mergeed.

    for(auto& e : item.second->fanin()) {
      timing_pt pos_timing_ptr = e->merged_pos(el);
      timing_pt neg_timing_ptr = e->merged_neg(el);
      timing_upt non_timing_uptr = e->merged_non_timing(el);
      if(non_timing_uptr!= nullptr){
        ofs <<  *(non_timing_uptr.get());
      }
      else {

        if(pos_timing_ptr != nullptr) {
          ofs << *(pos_timing_ptr);
        }
        if(neg_timing_ptr != nullptr) {
          ofs << *(neg_timing_ptr);
        }
      }

    }

    ofs << "    }\n";
    //fprintf(fptr, "    }\n");
  }

  // Write the ending symbol of cell group.
  ofs << "  }\n";
  //fprintf(fptr, "  }\n");

  // Write the ending symbol of library group.
  ofs << "}\n";
  //fprintf(fptr, "}\n");

  //fclose(fptr);
}

// Function: _insert_lut_template
// This procedure inserts a new item to the lut template dictionary from a given lut template.
// Notice that this function performs the copy assignment.
//
// Naming rule:
//             constraint table constraint_slew_slew_<index1>x<index2>
//                  delay table delay_load_slew_<index1>x<index2>
//             transition table outputslew_load_slew_<index1>x<index2>
//
lut_template_pt Abstractor::_insert_lut_template(lut_template_type_e type_e, size_t index1, size_t index2) {

  string_t name;
  switch (type_e) {
  case (lut_template_type_e::CONSTRAINT_TABLE):
    name = "SS_";
    break;
  case (lut_template_type_e::DELAY_TABLE):
    name = "LS_";
    break;
  case (lut_template_type_e::TRANSITION_TABLE):
    name = "LS_";
    break;
  default:
    CHECK(false);
    break;
  }

  name = name + to_string(index1) + "x" + to_string(index2);

  // Obtain the template if it exists.
  auto lut_temp_ptr = lut_template_dict()[name];
  if(lut_temp_ptr != nullptr) {
    return lut_temp_ptr;
  }
  // Otherwise insert a new template.
  else {
    lut_temp_ptr = lut_template_dict().insert(name);

    // Initialize the variable.
    switch (type_e) {

    // Constraint lut template (slew versus slew).
    case lut_template_type_e::CONSTRAINT_TABLE:
      lut_temp_ptr->set_variable1(CONSTRAINED_PIN_TRANSITION);
      lut_temp_ptr->set_variable2(RELATED_PIN_TRANSITION);
      break;

    // Cell arc template (slew versus load).
    case lut_template_type_e::DELAY_TABLE:
    case lut_template_type_e::TRANSITION_TABLE:
      lut_temp_ptr->set_variable1(TOTAL_OUTPUT_NET_CAPACITANCE);
      lut_temp_ptr->set_variable2(INPUT_NET_TRANSITION);
      break;

    default:
      return nullptr;
      break;
    }

    // Initialize the dimension.
    for (unsigned_t i=1; i<=index1; ++i) {
      lut_temp_ptr->insert_index1(i);
    }
    for (unsigned_t i=1; i<=index2; ++i) {
      lut_temp_ptr->insert_index2(i);
    }
    return lut_temp_ptr;
  }
}

void_t Abstractor::_update_lut_template(timing_pt timing_ptr) {

  if(timing_ptr == nullptr) return;

  // Sweep the 6 tables.
  if(!timing_ptr->cell_rise().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::DELAY_TABLE,
                                    timing_ptr->cell_rise().size1(),
                                    timing_ptr->cell_rise().size2());
    timing_ptr->cell_rise().set_lut_template_ptr(tmp);
  }

  if(!timing_ptr->cell_fall().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::DELAY_TABLE,
                                    timing_ptr->cell_fall().size1(),
                                    timing_ptr->cell_fall().size2());
    timing_ptr->cell_fall().set_lut_template_ptr(tmp);
  }

  if(!timing_ptr->rise_transition().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::TRANSITION_TABLE,
                                    timing_ptr->rise_transition().size1(),
                                    timing_ptr->rise_transition().size2());
    timing_ptr->rise_transition().set_lut_template_ptr(tmp);
  }

  if(!timing_ptr->fall_transition().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::TRANSITION_TABLE,
                                    timing_ptr->fall_transition().size1(),
                                    timing_ptr->fall_transition().size2());
    timing_ptr->fall_transition().set_lut_template_ptr(tmp);
  }

  if(!timing_ptr->rise_constraint().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::CONSTRAINT_TABLE,
                                    timing_ptr->rise_constraint().size1(),
                                    timing_ptr->rise_constraint().size2());
    timing_ptr->rise_constraint().set_lut_template_ptr(tmp);
  }

  if(!timing_ptr->fall_constraint().empty()) {
    auto tmp = _insert_lut_template(TimingLUTType::CONSTRAINT_TABLE,
                                    timing_ptr->fall_constraint().size1(),
                                    timing_ptr->fall_constraint().size2());
    timing_ptr->fall_constraint().set_lut_template_ptr(tmp);
  }

}

// Procedure: _update_lut_template
void_t Abstractor::_update_lut_template() {

  // Merge the timing.
  //#pragma omp parallel for schedule(dynamic, 1)
  //for(size_t i=0; i<abs_edgeset().num_indices(); ++i) {
  //  if(abs_edgeset()[i] != nullptr) {
  //    abs_edgeset()[i]->merge_timing();
  //  }
  //}

  // Strategy 1. ----------------------------------------------------------------------------------
  for(auto e : abs_edgeset()) {
    // remove useless decided abs node
    //if(e->decided_abs_node_ptr() != e->to_abs_node_ptr() && e->decided_abs_node_ptr() !=e->from_abs_node_ptr()) {
      //_remove_abs_node(e->decided_abs_node_ptr());
      //e->set_decided_abs_node_ptr(nullptr);
    //}
    if(e!=nullptr){
      EL_ITER(el) {
      //if(e->merged_non(el) != nullptr) {
      //  _update_lut_template(e->merged_non(el));
      //}
      //else {
        if(e->merged_pos(el) != nullptr) {
          _update_lut_template(e->merged_pos(el));
        }
        if(e->merged_neg(el) != nullptr) {
          _update_lut_template(e->merged_neg(el));
        }
      //}
      }
    }
  }
  return;
  // End of strategy 1. ---------------------------------------------------------------------------



  // Strategy 2: With parallelization. ------------------------------------------------------------
  /*unordered_map<int, lut_template_pt> delay;
  unordered_map<int, lut_template_pt> transition;
  unordered_map<int, lut_template_pt> constraint;

  auto register_lut = [] (unordered_map<int, lut_template_pt>& M, timing_lut_pt lut) {
    if(lut && !lut->empty()) {
      int s = (int)lut->size1();
      s = (s << 16) | (int)lut->size2();
      M[s] = nullptr;
    }
  };

  auto assign_template = [] (unordered_map<int, lut_template_pt>& M, timing_lut_pt lut) {
    if(lut && !lut->empty()) {
      int s = (int)lut->size1();
      s = (s << 16) | (int)lut->size2();
      lut->set_lut_template_ptr(M[s]);
    }
  };

  for(size_t i=0; i<abs_edgeset().num_indices(); ++i) {

    abs_edge_pt abs_edge_ptr= abs_edgeset()[i];

    if (abs_edge_ptr == nullptr) continue;

    EL_ITER(el){

      auto non_timing_ptr = abs_edge_ptr->merged_non(el);
      auto pos_timing_ptr = abs_edge_ptr->merged_pos(el);
      auto neg_timing_ptr = abs_edge_ptr->merged_neg(el);

      switch (abs_edge_ptr -> type()) {

        case AbsEdgeType::RCTREE_PATH:
        case AbsEdgeType::COMBINATIONAL:
        case AbsEdgeType::JUMP:
        case AbsEdgeType::PO_SEGMENT:

          // non unate abs edge
          if (non_timing_ptr!=nullptr) {
            register_lut(delay, non_timing_ptr->cell_rise_ptr());
            register_lut(delay, non_timing_ptr->cell_fall_ptr());
            register_lut(transition, non_timing_ptr->rise_transition_ptr());
            register_lut(transition, non_timing_ptr->fall_transition_ptr());
          }
          else {
            if(pos_timing_ptr!=nullptr){
              register_lut(delay, pos_timing_ptr->cell_rise_ptr());
              register_lut(delay, pos_timing_ptr->cell_fall_ptr());
              register_lut(transition, pos_timing_ptr->rise_transition_ptr());
              register_lut(transition, pos_timing_ptr->fall_transition_ptr());
            }
            if(neg_timing_ptr!=nullptr){
              register_lut(delay, neg_timing_ptr->cell_rise_ptr());
              register_lut(delay, neg_timing_ptr->cell_fall_ptr());
              register_lut(transition, neg_timing_ptr->rise_transition_ptr());
              register_lut(transition, neg_timing_ptr->fall_transition_ptr());
            }
          }

        break;
        case AbsEdgeType::CONSTRAINT:
          if(pos_timing_ptr!=nullptr){
            register_lut(constraint, pos_timing_ptr->rise_constraint_ptr());
            register_lut(constraint, pos_timing_ptr->fall_constraint_ptr());
          }
          if(neg_timing_ptr!=nullptr){
            register_lut(constraint, neg_timing_ptr->rise_constraint_ptr());
            register_lut(constraint, neg_timing_ptr->fall_constraint_ptr());
          }
        break;
        default:
          CHECK(false);
        break;
      }
    }
  }

  for (auto & it : delay){
    it.second = _insert_lut_template(TimingLUTType::DELAY_TABLE, it.first >> 16, it.first & 0xFFFF);
  }
  for (auto & it : transition){
    it.second = _insert_lut_template(TimingLUTType::TRANSITION_TABLE, it.first >> 16, it.first & 0xFFFF);
  }
  for (auto & it : constraint){
    it.second = _insert_lut_template(TimingLUTType::CONSTRAINT_TABLE, it.first >> 16, it.first & 0xFFFF);
  }

  #pragma omp parallel for schedule(dynamic, 1)
  for(size_t i=0; i<abs_edgeset().num_indices(); ++i) {

    abs_edge_pt abs_edge_ptr= abs_edgeset()[i];

    if (abs_edge_ptr == nullptr) continue;

    EL_ITER(el){

      auto non_timing_ptr=abs_edge_ptr->merged_non(el);
      auto pos_timing_ptr=abs_edge_ptr->merged_pos(el);
      auto neg_timing_ptr=abs_edge_ptr->merged_neg(el);

      switch (abs_edge_ptr -> type()){
        case AbsEdgeType::RCTREE_PATH:
        case AbsEdgeType::COMBINATIONAL:
        case AbsEdgeType::JUMP:
        case AbsEdgeType::PO_SEGMENT:

          if (non_timing_ptr!=nullptr){
            assign_template(delay,non_timing_ptr->cell_rise_ptr());
            assign_template(delay,non_timing_ptr->cell_fall_ptr());
            assign_template(transition,non_timing_ptr->rise_transition_ptr());
            assign_template(transition,non_timing_ptr->fall_transition_ptr());
          }
          else {
            if(pos_timing_ptr!=nullptr){
              assign_template(delay,pos_timing_ptr->cell_rise_ptr());
              assign_template(delay,pos_timing_ptr->cell_fall_ptr());
              assign_template(transition,pos_timing_ptr->rise_transition_ptr());
              assign_template(transition,pos_timing_ptr->fall_transition_ptr());

            }
            if(neg_timing_ptr!=nullptr){
              assign_template(delay,neg_timing_ptr->cell_rise_ptr());
              assign_template(delay,neg_timing_ptr->cell_fall_ptr());
              assign_template(transition,neg_timing_ptr->rise_transition_ptr());
              assign_template(transition,neg_timing_ptr->fall_transition_ptr());
            }
          }
        break;

        case AbsEdgeType::CONSTRAINT:
          if(pos_timing_ptr!=nullptr){
            assign_template(constraint,pos_timing_ptr->rise_constraint_ptr());
            assign_template(constraint,pos_timing_ptr->fall_constraint_ptr());
          }
          if(neg_timing_ptr!=nullptr){
            assign_template(constraint,neg_timing_ptr->rise_constraint_ptr());
            assign_template(constraint,neg_timing_ptr->fall_constraint_ptr());
          }
        break;

        default:
          CHECK(false);
        break;
      }
    }
  }
  // End strategy 2. ------------------------------------------------------------------------------
  */


}
void_t Abstractor::_topological_order(size_t* order,size_t & idx){

  LOG(INFO)<<"Start topological sort...";

  idx = 0;
  size_t* num_fanin = new size_t [abs_edgeset().num_indices()];
  //map<size_t, size_t> num_fanin;
  queue<abs_edge_pt> que;
  for (size_t i = 0; i<abs_edgeset().num_indices(); ++i){
    abs_edge_pt e = abs_edgeset()[i];
    if(e!=nullptr){
      abs_node_pt from = e->from_abs_node_ptr();
      num_fanin[i] = from->num_fanins();
      if(num_fanin[i]==0){
        order[idx] = i;
        ++idx;
        que.push(e);
      }
    }else{
      num_fanin[i]=OT_INT_MAX;
    }
  }
  while(!que.empty()){
    abs_edge_pt e = que.front();
    que.pop();
    abs_node_pt to = e->to_abs_node_ptr();
    for(const auto & fanout : to->fanout()){
      --num_fanin[fanout->idx()];
      if(num_fanin[fanout->idx()] == 0){
        order[idx] = fanout->idx();
        ++idx;
        que.push(fanout);
      }
    }
  }
  //CHECK(idx <= abs_edgeset().num_indices()) << idx<<" "<<abs_edgeset().num_indices();
  LOG(INFO)<<"Successfully topo...";
  
  delete [] num_fanin;
}
void_t Abstractor::_prepared_test( bool_t * common_point){

  LOG(INFO)<<"Start preparing test...";
  size_t* order = new size_t [abs_edgeset().num_indices()];
  size_t max_idx;
  _topological_order(order, max_idx); 

  unordered_map<int_t, set<node_pt> > te;
  //CHECK(max_idx <= abs_edgeset().num_indices())<< max_idx<<" "<<abs_edgeset().num_indices();

  for (size_t idx = 0; idx< max_idx; ++idx){
    int_t i = order[idx];
    abs_edge_pt e = abs_edgeset()[i];
    if(e!=nullptr && e->type() != AbsEdgeType::CLOCK_CONSTRAINT && !e->to_abs_node_ptr()->pin_ptr()->node_ptr()->is_in_clock_tree() ){
      abs_node_pt from = e->from_abs_node_ptr();
      abs_node_pt to = e->to_abs_node_ptr();
      if(from->pin_ptr()->is_clock_sink()){
        if(te.count(i)==0){
          //set<node_pt> tmp;
          te[i]=set<node_pt>();
        }
        te[i].insert(from->pin_ptr()->node_ptr());
      }
      for(const auto & in_e: from->fanin()){
        if(te.count(in_e->idx())>0){
          if(te.count(i) == 0){          
//            set<node_pt> tmp;
            te[i]=set<node_pt>();
          }
          te[i].insert(te[in_e->idx()].begin(), te[in_e->idx()].end());
        }
      }
      if(to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr()!= nullptr && te.count(i) >0 ){
        
        pin_pt ck = to->pin_ptr()->test_ptr()->related_pin_ptr();
        common_point[ck->node_ptr()->idx()]=true;

        for(const auto& iter : te[i]){
          node_pt sink = iter;
          node_pt cp = timer_ptr()->circuit_ptr()->clock_tree_ptr()->lca_node_ptr( ck->node_ptr() , sink);
          common_point[cp->idx()]=true;
        }
        if(to->num_fanouts()==0){
          te[i].clear();
        }
        //te[i].clear();

        //test[to->pin_ptr()->node_ptr()].insert(te[i].begin(), te[i].end());
      }
      if(!from->same_slew() && from->pin_ptr()->node_ptr()->is_in_clock_tree() && to->pin_ptr()->node_ptr()->is_in_clock_tree() ){
        common_point[to->pin_ptr()->node_ptr()->idx()]=true;
      }
      
    }
    if(e!=nullptr){
      abs_node_pt from = e->from_abs_node_ptr();
      abs_node_pt to = e->to_abs_node_ptr();
      if(!from->same_slew() && from->pin_ptr()->node_ptr()->is_in_clock_tree() && to->pin_ptr()->node_ptr()->is_in_clock_tree() ){
        common_point[to->pin_ptr()->node_ptr()->idx()]=true;
      }
    }
  }
  delete [] order;
  LOG(INFO)<<"Successfully preparing test...";

}
node_pt Abstractor::_find_common_point(abs_node_pt ck, abs_node_pt sink, bool_t* path){

  abs_node_pt u = sink;  
  abs_node_pt v;
  abs_node_pt clock_root = abs_node_dict()[abs_node_name(timer_ptr()->circuit_ptr()->clock_tree_root_pin_ptr())] ;
  if(path[u->pin_ptr()->node_ptr()->idx()]) 
    return u->pin_ptr()->node_ptr();

  while( u != clock_root && u->same_slew()){

    //CHECK(u->num_fanins()==1)<<u->num_fanins();
    abs_edge_pt e = u->fanin().head()->item();
    v = e->from_abs_node_ptr();

    if(path[v->pin_ptr()->node_ptr()->idx()]) 
      return v->pin_ptr()->node_ptr();
    u = v;
  }
  return nullptr;

 
  
}

void_t Abstractor::_abstract_clock(){
  //map<node_pt, set<node_pt> > test;
  LOG(INFO)<<"Start abstract clock...";


  pin_pt clock_pin = timer_ptr()->circuit_ptr()->clock_tree_root_pin_ptr();
  //abs_node_pt clock_root = abs_node_dict()[abs_node_name(clock_pin)] ;
  abs_node_pt v;
  abs_node_pt u; 
  //abs_node_pt prev_u; 
  //auto path = new bool_t[timer_ptr()->nodeset().num_indices()];
  //memset(path, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
 
  List< abs_node_pt > leaves;
  
  auto common_point = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(common_point, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());

  _prepared_test(common_point);
  for(const auto & abs_node_ptr: abs_node_dict()){
    if((common_point[abs_node_ptr.second->pin_ptr()->node_ptr()->idx()] && abs_node_ptr.second->same_slew())){
      //CHECK(abs_node_ptr.second->same_load() && abs_node_ptr.second->same_slew());
      leaves.push_back(abs_node_ptr.second); 
    }  
  }

  for(const auto& l :leaves){
    int rf = RISE;
    int_t non = 0; 
    u = l;
    //prev_u =nullptr;  
    edge_pt slew_e = nullptr ; 
    while(u->same_slew() ){
      //CHECK(u->num_fanins()==1)<<u->num_fanins() << " "<<  u->pin_ptr()->name();

      abs_edge_pt e = u->fanin().head()->item();
      rf = (e->timing_sense() == POSITIVE_UNATE)? rf: !rf;
      if(e->timing_sense() == NON_UNATE){
        non+=e->num_nonunate();
      }
      v = e->from_abs_node_ptr();
      if(( common_point[u->pin_ptr()->node_ptr()->idx()] && v->same_slew() ) || u->num_fanouts() == 0 ){
        _remove_abs_edge(e);
        //CHECK(v->same_load());
        //cout<<"     remove "<< v->pin_ptr()->name()<<" -> "<< u->pin_ptr()->name()<<endl;
      }

      //prev_u = u;
      if(e->slew_e_ptr() != nullptr){
        slew_e = e->slew_e_ptr();
      }
      if( u->num_fanouts() == 0 && u->num_fanins() == 0 && !u->pin_ptr()->is_primary_input() && !u->pin_ptr()->is_primary_output())
        _remove_abs_node(u);

      u = v;
      if(common_point[u->pin_ptr()->node_ptr()->idx()] )
        break;

    }
    timing_sense_ce ts = (non>0)?NON_UNATE:((rf == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);


    if(u != nullptr && u != l && u->same_slew() ){

      //CHECK(u->same_slew() && l->same_slew() && u->same_load() && l->same_load());

       
      if(slew_e !=nullptr){
        _insert_abs_edge(u->pin_ptr(), l->pin_ptr(), slew_e, AbsEdgeType::CLOCK_COMBINATIONAL,ts, non);
      }else {
        _insert_abs_edge(u->pin_ptr(), l->pin_ptr(), nullptr, AbsEdgeType::RCTREE_PATH,ts, non);
      }
    }
  }

  delete [] common_point; 
  LOG(INFO)<<"Successfully abstract clock...";

}
void_t Abstractor::_abstract_cross_abs_edges(size_t fanin, size_t fanout){
  LOG(INFO) << "Abstracting on cross abs edge ...";
  //int_t ori_size = abs_edgeset().size();
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  list<abs_node_pt> nodelist;
  list<abs_edge_pt> edgelist;
  unordered_set<abs_edge_pt> removeset;
  for(const auto& item : abs_node_dict()) {
    if(    item.second->num_fanins() == fanin
        && item.second->num_fanouts() == fanout
        && !item.second->is_clock() 
        && item.second->same_slew() 
        && item.second->same_load() 
      ){
      
      bool_t same_slew_load = true;

      for(const auto & fanine: item.second->fanin()){
        abs_node_pt from = fanine->from_abs_node_ptr();
        if(visited[from->pin_ptr()->node_ptr()->idx()]){
          same_slew_load = false;
        }
        if(!from->same_slew() || !from->same_load()){
          same_slew_load = false;
        }
        if(fanine->type() == AbsEdgeType::CLOCK_CONSTRAINT){
          same_slew_load = false;
        }
        if(from->is_clock()){
          //cout<< from->pin_ptr()->name()<<endl;
          for(const auto & fanoute:item.second->fanout()){
            abs_node_pt to = fanoute->to_abs_node_ptr();  
            //cout<<" "<<to->pin_ptr()->name()<<" ";
            if(to->pin_ptr()->is_constrained() && to->pin_ptr()->test_ptr()->related_pin_ptr() == from->pin_ptr()){
              same_slew_load = false;
              //cout<<"X "<< item.second->pin_ptr()->name();
            }
            //cout<<endl;
          }
        }
      }
      for(const auto & fanoute:item.second->fanout()){
        abs_node_pt to = fanoute->to_abs_node_ptr();
        if(visited[to->pin_ptr()->node_ptr()->idx()]){
          same_slew_load = false;
        }
        if(!to->same_load() || ! to->same_slew()){
          same_slew_load = false; 
        }
        if(fanoute->type() == AbsEdgeType::CLOCK_CONSTRAINT){
          same_slew_load = false;
        }
      }
      if(same_slew_load){
        nodelist.push_back(item.second);
        visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
      }
    }
  }
  
  for(const auto & n: nodelist){
    for(const auto & fanine: n->fanin()){
      abs_node_pt from = fanine->from_abs_node_ptr();
      for(const auto & fanoute: n->fanout()){
        size_t non = fanoute->num_nonunate() + fanine->num_nonunate();
        abs_node_pt to = fanoute->to_abs_node_ptr(); 
        
        timing_sense_ce ts = (non>0)?NON_UNATE:((fanine->timing_sense()==fanoute->timing_sense())?POSITIVE_UNATE:NEGATIVE_UNATE);
        abs_edge_pt new_abs_edge = _insert_abs_edge(from->pin_ptr(), to->pin_ptr(), AbsEdgeType::CROSS_ABS_EDGE, ts, non, fanine);        

        _abstract_cross_timing(new_abs_edge, fanine, fanoute);
        edgelist.push_back(new_abs_edge);
        removeset.insert(fanoute);
      }
      removeset.insert(fanine);
    }  
  }

  delete [] visited;
  
  
  _merge_abs_edge(edgelist, AbsEdgeType::CROSS_ABS_EDGE);

  for(const auto& e : removeset ){
    _remove_abs_edge(e);
  }
  for(const auto& n : nodelist ){
    if( n->num_fanouts() == 0 && n->num_fanins() == 0 
        && !n->pin_ptr()->is_primary_input()
        && !n->pin_ptr()->is_primary_output()
      ){
      _remove_abs_node(n);
    }
  }


  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }

  //ori_size = ori_size- abs_edgeset().size();
  LOG(INFO) << "Successfully abstracting on cross abs edge ...";// --reudce "<< to_string( ori_size  ) <<endl;

}//----------------------------------------------------------------------------------------------------------------------------------

void_t Abstractor::_abstract_on_list_abs_edge(){
  LOG(INFO) << "Abstracting on comb to d ...";
  list<abs_node_pt> nodelist;
  unordered_set<abs_edge_pt> removeset;
  list<abs_edge_pt> edgelist;
  auto visited = new bool_t[timer_ptr()->nodeset().num_indices()];
  memset(visited, 0, sizeof(bool_t)*timer_ptr()->nodeset().num_indices());
  queue<abs_node_pt> que;
  abs_node_pt u;
  abs_node_pt v;
  
  //int_t ori_size = abs_edgeset().size();

  for(const auto& item : abs_node_dict()) {
    if(item.second!=nullptr && item.second->pin_ptr()->is_primary_input() &&  item.second->pin_ptr() != timer_ptr()->circuit_ptr()->clock_tree_ptr()->root_pin_ptr()){
      que.push(item.second);  
      visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
    }
    if(item.second!=nullptr && item.second->pin_ptr()->is_clock_sink() && visited[item.second->pin_ptr()->node_ptr()->idx()] == false){
      que.push(item.second);
      visited[item.second->pin_ptr()->node_ptr()->idx()] = true;
    }
  }  

  while(!que.empty()){
    u = que.front();
    que.pop();
    for(const auto & e:u->fanout()){
      v = e->to_abs_node_ptr();
      // include Q pin and the first same slew pin drived from the input
      if(v->same_slew() && v->same_load() && visited[v->pin_ptr()->node_ptr()->idx()] == false){
        visited[v->pin_ptr()->node_ptr()->idx()] = true;
        nodelist.push_back(v);
      }else if(visited[v->pin_ptr()->node_ptr()->idx()] == false){
        visited[v->pin_ptr()->node_ptr()->idx()] = true;
        que.push(v);
      }
    }
  }

  int can = 0;
  size_t* order = new size_t [abs_edgeset().num_indices()];
  size_t max_idx;
  _topological_order(order, max_idx);

  for(const auto & n:nodelist){

    unordered_map <abs_node_pt, list<abs_edge_pt> > te;
    unordered_map <abs_node_pt, int_t > terf;
    unordered_map <abs_node_pt, int_t> te_num_non;
    for(const auto & e:n->fanout()){
      
      abs_node_pt to = e->to_abs_node_ptr();
      te[to] = list<abs_edge_pt>();
      te[to].push_back(e);
      terf[to] = RISE;
      terf[to] = (e->timing_sense() == POSITIVE_UNATE) ? terf[to]: !terf[to];
      te_num_non[to] = e->num_nonunate();
    }
  
    for (size_t idx = 0; idx< max_idx; ++idx){
      int_t i = order[idx];
      abs_edge_pt e = abs_edgeset()[i];
      abs_node_pt from = e->from_abs_node_ptr();
      abs_node_pt to = e->to_abs_node_ptr();
      if(te[from].size()>0){
        if(te.count(to) == 0){
          te[to] = list<abs_edge_pt>();
          //CHECK(!to->pin_ptr()->node_ptr()->is_in_clock_tree());
          te[to].insert( te[to].end() ,te[from].begin(), te[from].end());
          te[to].push_back(e);
          te_num_non[to] = te_num_non[from] + e->num_nonunate();
          terf[to] = (e->timing_sense() == POSITIVE_UNATE) ? terf[from]: !terf[from];
          
        }else { // there are two edge
          //removeset.insert(te[from].front());
          //removeset.insert(te[to].front());
          timing_sense_ce ts1 = (te_num_non[to] > 0)?NON_UNATE:((terf[to] == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);
          // already there
          abs_edge_pt new_abs_edge1 = _insert_abs_edge(n->pin_ptr(), to->pin_ptr(), AbsEdgeType::LIST_ABS_EDGE, ts1, te_num_non[to], te[to].front());
          edgelist.push_back(new_abs_edge1);
          _abstract_list_abs_timing(new_abs_edge1, te[to]);
    
          // second one
          timing_sense_ce ts2 = (te_num_non[from]+e->num_nonunate() > 0)?NON_UNATE:((terf[from] == RISE)? POSITIVE_UNATE: NEGATIVE_UNATE);
          abs_edge_pt new_abs_edge2 = _insert_abs_edge(n->pin_ptr(), to->pin_ptr(), AbsEdgeType::LIST_ABS_EDGE, ts2, te_num_non[from]+e->num_nonunate(), te[from].front());
          edgelist.push_back(new_abs_edge2);
          te[from].push_back(e);
          _abstract_list_abs_timing(new_abs_edge2, te[from]);
     
          break;
        }

      }
    }
  }
  
  for(const auto& e : removeset){
    _remove_abs_edge(e);
  }

  size_t size = 1;
  while(size>0){
   size =  _cut_orphan_edges();
  }

  
  
  _merge_abs_edge(edgelist, AbsEdgeType::LIST_ABS_EDGE);

  for(list<abs_edge_pt>::iterator it = edgelist.begin(); it!=edgelist.end() ; ++it){
    if((*it)!=nullptr){
      (*it)->set_type(AbsEdgeType::COMBINATIONAL);
    }
  }
  //ori_size = ori_size - abs_edgeset().size();
  LOG(INFO)<<"Successfully abstract on orphan abs edge ";//-- reduce "<<  to_string( ori_size )<<" abs edges"<<endl;
/*
  for(const auto & i:nodelist){
    
    que.push(i);
    
    while(!que.empty()){
      u = que.front();
      que.pop();
      for(const auto & e:i->fanout()){
        abs_node_pt from = e->from_abs_node_ptr();
        abs_node_pt to = e->to_abs_node_ptr();

        if(!to->pin_ptr()->is_constrained()){
          can++;
        }

      }
    }
  }*/
  delete [] visited;
  delete [] order;
}
// Procedure: _abstract_boundary_timing
// The procedure runs the timer to obtain the boundary timing of the abstraction. During the
// in-context usage, we are expected to support any input slew from 5ps to 250ps and any output
// load from 5fF to 205 fF. On the basis of these values, we are able to constraint the timing
// boundary (slew and load).
void_t Abstractor::_abstract_boundary_timing() {

  LOG(INFO) << "Abstracting boundary timing ...";

  EL_RF_ITER(el, rf) {
    for(CircuitPrimaryInputIter pi(timer_ptr()->circuit_ptr()); pi(); ++pi) {
      timer_ptr()->set_slew(pi.primary_input_ptr()->pin_ptr(), el, rf, OT_ABS_MIN_INPUT_SLEW);
    }

    for(CircuitPrimaryOutputIter po(timer_ptr()->circuit_ptr()); po(); ++po) {
      timer_ptr()->set_load(po.primary_output_ptr(), el, rf, OT_ABS_MIN_OUTPUT_LOAD);
    }
  }

  timer_ptr()->update_timing();

  for (const auto& item : abs_node_dict()) {
    EL_RF_ITER(el, rf) {
      item.second->set_min_slew( el, rf, item.second->pin_ptr()->node_ptr()->slew(el,rf));
      item.second->set_min_load( el, rf, item.second->pin_ptr()->load(el,rf));
    }
  }

  // Get max slew & max load
  EL_RF_ITER(el, rf) {
    for(CircuitPrimaryInputIter pi(timer_ptr()->circuit_ptr()); pi(); ++pi) {
      timer_ptr()->set_slew(pi.primary_input_ptr()->pin_ptr(), el, rf, OT_ABS_MAX_INPUT_SLEW);
    }

    for(CircuitPrimaryOutputIter po(timer_ptr()->circuit_ptr()); po(); ++po) {

      timer_ptr()->set_load(po.primary_output_ptr(), el, rf, OT_ABS_MAX_OUTPUT_LOAD);
      
    }
  }

  timer_ptr()->update_timing();


  for (const auto& item : abs_node_dict()) {
    EL_RF_ITER(el, rf) {
      item.second->set_max_slew( el, rf, item.second->pin_ptr()->node_ptr()->slew(el,rf));
      item.second->set_max_load( el, rf, item.second->pin_ptr()->load(el,rf));
       
    }
    item.second->set_same_slew();
    item.second->set_same_load();
  }


  // Reset the load and update the timing.
  EL_RF_ITER(el, rf) {
    for(const auto& item : timer_ptr()->circuit_ptr()->primary_output_dict()) {
      timer_ptr()->set_load(item.second, el, rf, OT_FLT_ZERO);
    }
  }
  timer_ptr()->update_timing();


  LOG(INFO) << "Successfully abstracted boundary timing";
}

// Function: _infer_indices
float_t Abstractor::_same_max_min(float_t minv, float_t maxv) {

  float_t out;

  if(minv > maxv) swap(minv, maxv);

  // Lambda: table_size
  auto table_size = [] (float_t minv, float_t maxv) -> size_t {
    if( minv > maxv ) swap(minv, maxv);
    if ((maxv - minv) < 1.0f) return 1;
    else if ((maxv - minv) < 5.0f) return 2;
    else if ((maxv-minv) < 10.0f) return 4;
    else return OT_ABS_TABLE_SIZE;
  };


  auto size = table_size(minv, maxv);
  if(size != 1) 
    out= (maxv + minv)/2.0f;
  else
    out= (maxv + minv)/2.0f;
  return out;
}


// Function: _infer_indices
float_vt Abstractor::_infer_indices(float_t minv, float_t maxv) {

  float_vt out;

  if(minv > maxv) swap(minv, maxv);

  // Lambda: table_size
  auto table_size = [] (float_t minv, float_t maxv) -> size_t {
    if( minv > maxv ) swap(minv, maxv);
    if ((maxv - minv) < 1.0f) return 1;
    else if ((maxv - minv) < 5.0f) return 2;
    else if ((maxv-minv) < 10.0f) return 4;
    else return OT_ABS_TABLE_SIZE+3;
  };

  // Lambda: next_index
  auto next_index = [] (float_t minv, float_t maxv, size_t size , unsigned_t i) {
    if (size <= 1) return maxv;
    if (size == 2) {
      switch (i) {
      case 0:
        return minv;
        break;
      case 1:
        return maxv;
        break;
      default:
        break;
      }
    }
    if( size == 4 ) {
      switch (i) {
      case 0:
        return minv;
        break;
      case 1:
        return (maxv - minv) * 0.25f + minv;
        break;
      case 2:
        return (maxv - minv) * 0.5f + minv;
        break;
      case 3:
      default:
        return maxv;
        break;
      }
    } else { // size == 8  && max - min >= 10
      switch (i) {
      case 11:
        return maxv;
        break;
      case 10:
        return maxv- (maxv - minv) * 0.2f;
        break;
      case 9:
        return maxv - (maxv - minv) * 0.3f;;
        break;
      case 8:
        return maxv - (maxv - minv) * 0.4f;
        break;
      case 7:
        return maxv - (maxv - minv) * 0.5f;
        //CHECK(output>  maxv - (maxv - minv) * 0.3f);
        break;
      case 6:
        return maxv - (maxv - minv) * 0.6f;
        //CHECK(output>   maxv - (maxv - minv) * 0.6f);
        break;
      case 5:
        return maxv - (maxv - minv) * 0.7f;
        //CHECK(output> maxv - (maxv - minv) * 0.8f);
        break;
      case 4:
        return maxv - (maxv - minv) * 0.8f;
        //CHECK(output>maxv - (maxv - minv) * 0.9f);
        break;
      case 3:
        return maxv - (maxv - minv) * 0.9f;
        //CHECK(output>maxv - (maxv - minv) * 0.95f);
        break;
      case 2:
        return maxv - (maxv - minv) * 0.95f;
        //CHECK(output> maxv - (maxv - minv) * 0.98f);
        break;
      case 1:
        return maxv - (maxv - minv) * 0.98f;
        //CHECK(output>minv);
        break;
      case 0:
        return minv;
        break;
      default:
        return (minv + maxv) / OT_FLT_TWO;
        break;
      }
    }
  };

  auto size = table_size(minv, maxv);
  for(unsigned_t i(0); i<size; ++i) {
    out.push_back(next_index(minv,maxv,size,i));
  }
  return out;
}

};  // End of OpenTimer namespace. ----------------------------------------------------------------



