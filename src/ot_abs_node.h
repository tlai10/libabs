/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_NODE_H_
#define OT_ABS_NODE_H_

#include "ot_timer.h"
#include "ot_abs_typedef.h"
#include "ot_abs_edge.h"
#include "ot_abs_macrodef.h"

namespace OpenTimer {

// Class: AbsNode
class AbsNode{

  public:

    AbsNode(string_crt);                                              // Constructor.
    ~AbsNode();                                                       // Destructor.

    void_t insert_fanin(abs_edge_pt);                                 // Insert a fanin.
    void_t insert_fanout(abs_edge_pt);                                // Insert a fanout.
    void_t remove_fanin(abs_edge_pt);                                 // Remove a fanin.
    void_t remove_fanout(abs_edge_pt);                                // Remove a fanout.
    
    inline string_crt name() const;                                   // Query the name.
    inline string_crt original_name() const;                          // Query the original name.

    bool_t is_clock() const;                                   // Query the clock status.

    inline pin_pt pin_ptr() const;                                    // Query the pin pointer.
    inline abs_edgelist_pt fanin_ptr() const;                         // Query the fanin ptr.
    inline abs_edgelist_rt fanin() const;                             // Query the fanin ref.
    inline abs_edgelist_pt fanout_ptr() const;                        // Query the fanout ptr.
    inline abs_edgelist_rt fanout() const;                            // Query the fanout ref.

    inline float_t cap(int el) const;

    inline void_t set_pin_ptr(pin_pt);                                // Set the pin ptr.
    inline void_t set_max_slew(int, int, float_ct);                   // Set the max slew.
    inline void_t set_min_slew(int, int, float_ct);                   // Set the min slew.
    inline void_t set_max_load(int, int, float_ct);                   // Set the max load.
    inline void_t set_min_load(int, int, float_ct);                   // Set the min load.
    inline void_t set_clock_sink(bool_t);

    inline float_t max_slew(int, int) const;                          // Query the max slew.
    inline float_t min_slew(int, int) const;                          // Query the min slew.
    inline float_t max_load(int, int) const;                          // Query the max load.
    inline float_t min_load(int, int) const;                          // Query the min load.

    inline bool_t same_slew() const;
    inline bool_t same_load() const;

    size_t num_fanouts() const;
    size_t num_fanins() const;
    
    pin_direction_e direction() const;                                // Query the direction.
    void_t set_same_load();
    void_t set_same_slew();
  /*  
    bool_t operator<(const abs_node_pt& b){
      return ((pin_ptr()->node_ptr()->num_fanins()- num_fanins()) > (b->pin_ptr()->node_ptr()->num_fanins() - b->num_fanins()));
    }
*/
  
    inline static void_pt operator new(size_t);                       // Operator new.
    inline static void_t operator delete(void_pt);                    // Operator delete.

    AbsNode& operator = (const AbsNode&) = delete;                    // Disable copy assignment.
    static void_pt operator new [] (size_t) = delete;                 // Disable array new.
    static void_t operator delete [] (void_pt) = delete;              // Disable array delete.

  private:
 
    string_t _name;                                                   // Name (not original).
    bool_t _same_slew;
    bool_t _same_load;
    bool_t _is_clock_sink;
    float_t _max_slew[2][2];                                          // Max possible slew.
    float_t _min_slew[2][2];                                          // Min possible slew.
    float_t _max_load[2][2];                                          // Max possible load.
    float_t _min_load[2][2];                                          // Min possible load.
     
    pin_pt _pin_ptr;                                                  // pin ptr. 

    abs_edgelist_pt _fanin_ptr;                                       // Fanin AbsEdge
    abs_edgelist_pt _fanout_ptr;                                      // Fanout AbsEdge

    static abs_node_allocator_t _allocator;                           // Allocator.
};

// Operator: new
inline void_pt AbsNode::operator new(size_t size) {
  return _allocator.allocate();
}

// Operator: delete
inline void_t AbsNode::operator delete(void_pt ptr) {
  _allocator.deallocate((abs_node_pt)ptr);
}

// Function: original_name
inline string_crt AbsNode::original_name() const{
  return pin_ptr()->name();
}            

// Function: name
inline string_crt AbsNode::name() const {
  return _name;
}

// Procedure: set_pin_ptr
inline void_t AbsNode::set_pin_ptr(pin_pt ptr) {
  _pin_ptr = ptr;
}

// Function: pin_ptr
inline pin_pt AbsNode::pin_ptr() const{
  return _pin_ptr;
} 

// Function: fanin_ptr
inline abs_edgelist_pt AbsNode::fanin_ptr() const{
  return _fanin_ptr;
}

// Function: fanin
inline abs_edgelist_rt AbsNode::fanin() const {
  return *_fanin_ptr;
}

// Function: fanout_ptr
inline abs_edgelist_pt AbsNode::fanout_ptr() const{
  return _fanout_ptr;
}

// Function: fanout
inline abs_edgelist_rt AbsNode::fanout() const {
  return *_fanout_ptr;
}

// Procedure: set_max_slew
inline void_t AbsNode::set_max_slew(int el, int rf, float_ct v) {
  _max_slew[el][rf] = v;
}

// Procedure: set_min_slew
inline void_t AbsNode::set_min_slew(int el, int rf, float_ct v) {
  _min_slew[el][rf] = v;
}

// Procedure: set_max_load
inline void_t AbsNode::set_max_load(int el, int rf, float_ct v) {
  _max_load[el][rf] = v;
}

// Procedure: set_min_load
inline void_t AbsNode::set_min_load(int el, int rf, float_ct v) {
  _min_load[el][rf] = v;
}

// Function: max_slew
inline float_t AbsNode::max_slew(int el, int rf) const {
  return _max_slew[el][rf];
}

// Function: min_slew
inline float_t AbsNode::min_slew(int el, int rf) const {
  return _min_slew[el][rf] ;
}

// Function: max_load
inline float_t AbsNode::max_load(int el, int rf) const {
  return _max_load[el][rf];
}

// Function: min_load
inline float_t AbsNode::min_load(int el, int rf) const {
  return _min_load[el][rf];
}

// Function: cap
inline float_t AbsNode::cap(int el) const {
  return pin_ptr()->is_primary_input() ? pin_ptr()->load(el, RISE) : OT_FLT_ZERO;
}
inline bool_t AbsNode::same_slew() const{
  return _same_slew;
  /*
  */
}

// has same load
inline bool_t AbsNode::same_load() const{
  return _same_load;
}
inline void_t AbsNode::set_clock_sink(bool_t is){
  _is_clock_sink = is;
}

};// End of namespace OpenTimer. ------------------------------------------------------------------

#endif


