/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#include "ot_abs_node.h"

namespace OpenTimer {

// AbsNode allocator.    
abs_node_allocator_t AbsNode::_allocator;

// Constructor.
AbsNode::AbsNode(string_crt name) :
  _name(name),
  _pin_ptr(nullptr),
  _same_slew(false),
  _same_load(false),
  _is_clock_sink(false)
{
  _fanin_ptr = new abs_edgelist_t();
  _fanout_ptr = new abs_edgelist_t();
  EL_RF_ITER(el,rf){
    _max_slew[el][rf]=OT_ABS_NODE_DEFAULT_MAX_SLEW;
    _min_slew[el][rf]=OT_ABS_NODE_DEFAULT_MIN_SLEW;
    _max_load[el][rf]=OT_ABS_NODE_DEFAULT_MAX_CAPACITANCE;
    _min_load[el][rf]=OT_ABS_NODE_DEFAULT_MIN_CAPACITANCE;
  
  }
}

// Destructor.
AbsNode::~AbsNode() {
  delete _fanin_ptr;
  delete _fanout_ptr;
}

// Procedure: insert_fanin
void_t AbsNode::insert_fanin(abs_edge_pt abs_edge_ptr){
  abs_edgelist_iter_t satellite = _fanin_ptr->push_back(abs_edge_ptr);
  abs_edge_ptr->set_fanin_satellite(satellite);
}

// Procedure: insert_fanout
void_t AbsNode::insert_fanout(abs_edge_pt abs_edge_ptr){
  abs_edgelist_iter_t satellite = _fanout_ptr->push_back(abs_edge_ptr);
  abs_edge_ptr->set_fanout_satellite(satellite);
}

// Procedure: remove_fanin
void_t AbsNode::remove_fanin(abs_edge_pt abs_edge_ptr){
  if(abs_edge_ptr == nullptr) return;
  fanin_ptr()->remove(abs_edge_ptr->fanin_satellite());
  abs_edge_ptr->set_fanin_satellite(NULL);
}

// Procedure: remove_fanout
void_t AbsNode::remove_fanout(abs_edge_pt abs_edge_ptr){
  if(abs_edge_ptr == nullptr) return;
  fanout_ptr()->remove(abs_edge_ptr->fanout_satellite());
  abs_edge_ptr->set_fanout_satellite(NULL);
}

// Function: num_fanins
size_t AbsNode::num_fanins() const{
  return fanin_ptr()->size(); 
}

// Function: num_fanouts
size_t AbsNode::num_fanouts() const{
  return fanout_ptr()->size();
}

// Function: direction
pin_direction_e AbsNode::direction() const {                                    

  switch(pin_ptr()->direction()) {

    case PRIMARY_INPUT_PIN_DIRECTION:
      return INPUT_CELLPIN_DIRECTION;
    break;

    case PRIMARY_OUTPUT_PIN_DIRECTION:
      return OUTPUT_CELLPIN_DIRECTION;
    break;

    default:
      return INTERNAL_CELLPIN_DIRECTION;
    break;
  }
}                                                                                     

// Function: is_clock
bool_t AbsNode::is_clock() const {
  //if(pin_ptr()->is_clock_sink())
    return pin_ptr()->is_clock_sink()||_is_clock_sink;
  //else return  _is_clock_sink || ;
/*
  for (const auto & e: fanout() ){
    if(e->type()==AbsEdgeType::CLOCK_CONSTRAINT || e->type()==AbsEdgeType::CLOCK_COMBINATIONAL){
      return true;
    }
  }

  return false;
*/
}


void_t AbsNode::set_same_load(){
  EL_RF_ITER(el,rf){
    if((max_load(el,rf) - min_load(el,rf)) > 0.1f){
      _same_load = false;
      return;
    }
  }  
  _same_load = true;
  return ;
 
}
void_t AbsNode::set_same_slew(){

  EL_RF_ITER(el,rf){
    if((max_slew(el,rf) - min_slew(el,rf)) > 0.1f){
      _same_slew = false;
      return ;
    }
  } 
  _same_slew = true; 
  return;
}

};  // End of namespace OpenTimer. ----------------------------------------------------------------




