/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_HEADERDEF_H_
#define OT_ABS_HEADERDEF_H_

#include <memory>
#include <list>

using std::make_pair;
using std::priority_queue;
using std::list;
#endif
