/******************************************************************************
 *                                                                            *
 * Copyright (c) 2016, Tin-Yin Lai, Tsung-Wei Huang and Martin D. F. Wong,    *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ABS_TYPEDEF_H_
#define OT_ABS_TYPEDEF_H_

#include "ot_typedef.h"
#include "ot_abs_enumdef.h"
#include "ot_abs_headerdef.h"
#include "ot_abs_classdef.h"
namespace OpenTimer {

using abs_node_dict_t = Dictionary <string_t, abs_node_t>;
using abs_node_dict_pt = Dictionary <string_t, abs_node_t>*;
using abs_node_dict_rt = Dictionary <string_t, abs_node_t>&;
using abs_node_dict_upt = unique_ptr<abs_node_dict_t>;

using abs_edge_type_e = AbsEdgeType;
using abs_edge_type_ce = const AbsEdgeType;

using abs_edgelist_rt = abs_edgelist_t&;

using lut_template_type_e = TimingLUTType;

using lut_template_dict_rt = lut_template_dict_t&;
using size_float_pair_t = pair<size_t, float_t>;

};

#endif

