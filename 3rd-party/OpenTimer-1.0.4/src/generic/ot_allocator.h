/******************************************************************************
 *                                                                            *
 * Copyright (c) 2015, Tsung-Wei Huang and Martin D. F. Wong,                 *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ALLOCATOR_H_
#define OT_ALLOCATOR_H_

namespace OpenTimer {

// Class: Allocator
// The memory pool based allocator of a single fixed data of type T.
template <typename T>
class Allocator {
  
  using T_t = T;                                            // Item type.
  using T_pt = T*;                                          // Item ptr type. 
  using T_ppt = T**;                                        // Item double ptr type. 
  using T_rt = T&;                                          // Item ref ptr type.
    
  struct MempoolBlock{
    T_pt block;                                             // Block memory.
    size_t size;                                            // Size of the block (count).
    struct MempoolBlock* next;                              // Pointer to the next block.
  };

  struct MempoolSlot{
    MempoolBlock* head;                                     // Head pool block.
    MempoolBlock* tail;                                     // Tail pool block.
    T_ppt free_entries;                                     // Pointer array for recycle items.
    size_t num_free_entries;                                // Size of recycle box.
    int free_entry_cursor;                                  // Cursor to next item from recycle.
    int pool_block_cursor;                                  // Cursor to next item from block.
  };

  using mempoolblock_t = MempoolBlock;                      // Mempool block type.
  using mempoolblock_pt = MempoolBlock*;                    // Mempool block ptr type.
  using mempoolblock_rt = MempoolBlock&;                    // Mempool block ref type.
  using mempoolslot_t = MempoolSlot;                        // Mempool slot type.
  using mempoolslot_pt = MempoolSlot*;                      // Mempool slot ptr type.
  using mempoolslot_rt = MempoolSlot&;                      // Mempool slot ref type.

  public:
    
    inline Allocator(size_t n=16);                          // Constructor.
    inline ~Allocator();                                    // Destructor.

    inline T_pt allocate();                                 // Allocate an entry of type T.
    inline void deallocate(T_pt);                           // Deallocate an entry of type T.

  private:

    Allocator & operator = (const Allocator &) = delete;    // Disable copy assignment.

    mempoolslot_t _slot;                                    // Slot (meta data storage).

    inline mempoolblock_pt _allocate_mempool(size_t);       // Allocate a pool block.
    inline void _deallocate_mempool(mempoolblock_pt);       // Deallocate a pool block.
};

// Constructor.
template <typename T>
inline Allocator<T>::Allocator(size_t n) {

  // Create the first memory pool block. By default, we create at least 32 items
  // for the first pool block.
  mempoolblock_pt first_pool = _allocate_mempool(n);
  
  // Assign the meta-data to the slot.
  _slot.head = first_pool;
  _slot.tail = first_pool;
  _slot.num_free_entries = n;
  _slot.free_entries = (T_ppt)malloc(n*sizeof(T_pt));
  _slot.free_entry_cursor = -1;
  _slot.pool_block_cursor = 0;
}

// Destructor.
template <typename T>
inline Allocator<T>::~Allocator() {

  // Iterate the linked list and free each pool block.
  mempoolblock_pt pre = nullptr;
  mempoolblock_pt cur = _slot.head; 
  while(cur != nullptr) {
    pre = cur;
    cur = cur->next;
    _deallocate_mempool(pre);
  }

  // Free the memory of slot.
  free(_slot.free_entries);
}

// Function: _allocate_mempool
template <typename T>
inline typename Allocator<T>::mempoolblock_t* Allocator<T>::_allocate_mempool(size_t n) {
  mempoolblock_pt ptr = (mempoolblock_pt)malloc(sizeof(mempoolblock_t));
  ptr->block = (T_pt)malloc(n*sizeof(T));
  ptr->size = n;
  ptr->next = nullptr;
  return ptr;
}

// Procedure: _deallocate_mempool
template <typename T>
inline void Allocator<T>::_deallocate_mempool(mempoolblock_pt ptr) {
  free(ptr->block);
  free(ptr);
}

// Function: allocate
// Allocate a memory piece of type T from the memory pool and return the T* to that memory.
template <typename T>
inline typename Allocator<T>::T_pt Allocator<T>::allocate() {

  // Allocate item from free entry if any.
  if(_slot.free_entry_cursor >= 0) {
    return (_slot.free_entries[_slot.free_entry_cursor--]);
  }
  // Allocate item from pool, where we have to create a new pool block if the latest one
  // is full.
  else {
    if((size_t)_slot.pool_block_cursor == _slot.tail->size) {
      mempoolblock_pt ptr = _allocate_mempool((_slot.tail->size) << 1);
      _slot.tail->next = ptr;
      _slot.tail = ptr;
      _slot.pool_block_cursor = 0;
    }
    return &(_slot.tail->block[_slot.pool_block_cursor++]);
  }
}

// Function: deallocate
// Deallocate given memory piece of type T.
template <typename T>
inline void Allocator<T>::deallocate(T_pt ptr) {
  
  // Go to the next free entry.
  ++_slot.free_entry_cursor;

  // Reallocate the free entry array if it is full.
  if((size_t)_slot.free_entry_cursor == _slot.num_free_entries) {
    _slot.num_free_entries = _slot.num_free_entries << 1;
    _slot.free_entries = (T_ppt)realloc(_slot.free_entries, sizeof(T_pt)*_slot.num_free_entries);
  }

  // Insert the deallocated item to the end of the free entry array.
  _slot.free_entries[_slot.free_entry_cursor] = ptr;
}

};  // End of namespace OpenTimer. ----------------------------------------------------------------



#endif

