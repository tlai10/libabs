/******************************************************************************
 *                                                                            *
 * Copyright (c) 2015, Tsung-Wei Huang and Martin D. F. Wong,                 *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_ORDEREDSET_H_
#define OT_ORDEREDSET_H_

#include "ot_headerdef.h"

namespace OpenTimer {

// Class: OrderedSet
// An ordered set is a data structure consists of a collection of items that support random
// access. Each inserted item is referenced by an index and can be accessed through operator []
// on the ordered set.
template < class T, class Allocator = ::std::allocator<T> >
class OrderedSet {

  public:

    using data_t = vector<T*>;                              // Data type.     
    using data_pt = data_t*;                                // Data ptr type.
    using data_rt = data_t&;                                // Data ref type.
    using recy_t = vector<size_t>;                          // Recycle array type.
    using recy_pt = recy_t*;                                // Recycle array ptr type.
    using recy_rt = recy_t&;                                // Recycle array ref type.

    struct iter_t {                                         // Iterator type.
      
      typename data_t::iterator itr;                        // Cursor.
      typename data_t::iterator end;                        // Ending.

      inline iter_t& operator ++();                         // Postfix iter.
      inline T*& operator *();                              // Deref iter.
      inline bool operator != (const iter_t&);              // Comparator.
    };
    
    struct const_iter_t {                                   // Iterator type.
      
      typename data_t::const_iterator itr;                  // Cursor.
      typename data_t::const_iterator end;                  // Ending.

      inline const_iter_t& operator ++();                   // Postfix iter.
      inline const T*& operator *() const;                  // Deref iter.
      inline bool operator != (const const_iter_t&);        // Comparator.
    };

    inline OrderedSet();                                    // Constructor.
    inline ~OrderedSet();                                   // Destructor.
    
    inline size_t size() const;                             // Query the size.
    inline size_t num_indices() const;                      // Query the index size.

    inline void remove(const size_t);                       // Remove an item.

    inline size_t insert();                                 // Insert an item.

    inline iter_t begin();                                  // Iterator status.
    inline iter_t end();                                    // Iterator status.

    inline data_rt data();
    
    inline T* operator [] (const size_t);                   // Operator.
    inline const T* operator [] (const size_t) const;       // Operator.
    
  private:

    size_t _size;                                           // Size.

    data_t _data;                                           // Data array.
    recy_t _recy;                                           // Recycle indices.
};  

// Mutable iterator. ------------------------------------------------------------------------------

// Operator: ++ ()
template <class T, class Allocator>
inline typename OrderedSet<T, Allocator>::iter_t&
OrderedSet<T, Allocator>::iter_t::operator ++ () { 
  ++itr;
  while(itr != end && *itr == nullptr) ++itr;
  return *this;
}

// Operator: * ()
template <class T, class Allocator>
inline T*& OrderedSet<T, Allocator>::iter_t::operator * () {
  return *itr;
}

// Operator: !=
template <class T, class Allocator>
inline bool OrderedSet<T, Allocator>::iter_t::operator != (const iter_t& rhs) {
  return itr != rhs.itr;
}

// Immutable iterator. ----------------------------------------------------------------------------

// Operator: ++ ()
template <class T, class Allocator>
inline typename OrderedSet<T, Allocator>::const_iter_t&
OrderedSet<T, Allocator>::const_iter_t::operator ++ () {
  ++itr;
  while(itr != end && *itr == nullptr) ++itr;
  return *this;
}

// Operator: * ()
template <class T, class Allocator>
inline const T*& OrderedSet<T, Allocator>::const_iter_t::operator * () const {
  return *itr;
}

// Operator: !=
template <class T, class Allocator>
inline bool OrderedSet<T, Allocator>::const_iter_t::operator != (const const_iter_t& rhs) {
  return itr != rhs.itr;
}

// OrderedSet. ------------------------------------------------------------------------------------

// Constructor.
template <class T, class Allocator>
inline OrderedSet<T, Allocator>::OrderedSet() {
  _size = 0;
}

// Destructor
template <class T, class Allocator>
inline OrderedSet<T, Allocator>::~OrderedSet() {
  for(auto &i : _data) {
    delete i;
  }
}

// Function: begin
template <class T, class Allocator>
inline typename OrderedSet<T, Allocator>::iter_t OrderedSet<T, Allocator>::begin() {
  return { _data.begin(), _data.end() } ;
}

// Function: begin
template <class T, class Allocator>
inline typename OrderedSet<T, Allocator>::iter_t OrderedSet<T, Allocator>::end() {
  return { _data.end(), _data.end() } ;
}

// Function: data
template <class T, class Allocator>
inline typename OrderedSet<T, Allocator>::data_rt OrderedSet<T, Allocator>::data() {
  return _data;
}

// Operator
template <class T, class Allocator>
inline T* OrderedSet<T, Allocator>::operator [] (const size_t idx) {
  return idx < num_indices() ? _data[idx] : nullptr;
}

// Function: num_indices
template <class T, class Allocator>
inline size_t OrderedSet<T, Allocator>::num_indices() const {
  return _data.size();
}

// Function: size
template <class T, class Allocator>
inline size_t OrderedSet<T, Allocator>::size() const {
  return _size;
}

// Procedure: remove
// Remove an item that is specified by the idx.
template <class T, class Allocator>
inline void OrderedSet<T, Allocator>::remove (const size_t idx) {

  auto ptr = operator[](idx);

  if(ptr == nullptr) return;

  delete _data[idx];
  _data[idx] = nullptr;
  --_size;
  _recy.push_back(idx);
}

// Procedure: insert
// Insert a new item.
template <class T, class Allocator>
inline size_t OrderedSet<T, Allocator>::insert() {

  size_t idx;

  if(!_recy.empty()) {
    idx = _recy.back();
    _recy.pop_back();
    _data[idx] = new T();
  }
  else {
    idx = _data.size();
    _data.push_back(new T());
  }

  ++_size;

  return idx;
}


};  // End of namespace OpenTimer. ----------------------------------------------------------------

#endif
