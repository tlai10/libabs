/******************************************************************************
 *                                                                            *
 * Copyright (c) 2015, Tsung-Wei Huang and Martin D. F. Wong,                 *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#ifndef OT_DICTIONARY_H_
#define OT_DICTIONARY_H_

#include "ot_headerdef.h"
#include "ot_classdef.h"

namespace OpenTimer {

#define OT_PRIME32_1 2654435761U
#define OT_PRIME32_2 2246822519U
#define OT_PRIME32_3 3266489917U
#define OT_PRIME32_4  668265263U
#define OT_PRIME32_5  374761393U

#define OT_PRIME64_1 11400714785074694791ULL
#define OT_PRIME64_2 14029467366897019727ULL
#define OT_PRIME64_3  1609587929392839161ULL
#define OT_PRIME64_4  9650029242287828579ULL
#define OT_PRIME64_5  2870177450012600261ULL

#define OT_HASH_SEED 196613

// Class: Dictionary
// K: key type
// T: obj type
// Dictionary class stores the mapping of an object's key (name) into its physical memory
// address location. This class will automatically create the memory for the given object
// and will automatically delete the memory by its destructor.
template <
  class K, class V, 
  class Hash = ::std::hash<K>, 
  class KeyEqual = ::std::equal_to<K>,
  class Allocator = ::std::allocator< ::std::pair<const K, V*> >
>
class Dictionary : private unordered_map <K, V*, Hash, KeyEqual, Allocator> {
  
  public:
  
    using base = unordered_map <K, V*>;                               // Base type.
    using iter_t = typename base::iterator;                           // Iter.
    using iter_ct = typename base::const_iterator;                    // Constant iter.

    inline Dictionary();                                              // Constructor.
    inline Dictionary(size_t);                                        // Constructor.
    inline ~Dictionary();                                             // Destructor.

    inline bool empty() const;                                        // Query the size status.

    inline size_t size() const;                                       // Query the size.
    inline size_t max_size() const;                                   // Query the size.
    
    inline void clear();                                              // Clear the hash table.
    inline void rehash(size_t);                                       // Rehash the table.
    inline void remove(const K &);                                    // Erase an item.

    inline V* insert(const K &);                                      // Insert an new item.
    inline V* rehash(const K &, const K &);                           // Rekey an item.

    inline iter_t begin();                                            // Query the iter_t.
    inline iter_ct begin() const;                                     // Query the iter_t.
    inline iter_t end();                                              // Query the iter_t.
    inline iter_ct end() const;                                       // Query the iter_t.

    inline V* operator [] (const K &);                                // Operator [].
    inline const V* operator [] (const K &) const;                    // Operator [].

  private:
    
};

// Constructor
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline Dictionary<K, V, Hash, KeyEqual, Allocator>::Dictionary() {
}

// Constructor
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline Dictionary<K, V, Hash, KeyEqual, Allocator>::Dictionary(size_t prime) :
  base(prime) {
}

// Destructor
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline Dictionary<K, V, Hash, KeyEqual, Allocator>::~Dictionary() {
  clear();
}

// Function: size
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline size_t Dictionary<K, V, Hash, KeyEqual, Allocator>::size() const {
  return base::size();
}

// Function: max_size
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline size_t Dictionary<K, V, Hash, KeyEqual, Allocator>::max_size() const {
  return base::max_size();
}

// Function: empty
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline bool Dictionary<K, V, Hash, KeyEqual, Allocator>::empty() const {
  return base::empty();
}

// Procedure: clear
// Clear the object.
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline void Dictionary<K, V, Hash, KeyEqual, Allocator>::clear() {
  for(auto itr = base::begin(); itr != base::end(); ++itr) {
    delete itr->second;
  }
  base::clear();
}

// Function: begin
template <class K, class V, class Hash, class KeyEqual, class Allocator>
typename Dictionary<K, V, Hash, KeyEqual, Allocator>::iter_t 
Dictionary<K, V, Hash, KeyEqual, Allocator>::begin() {
  return base::begin();
}

// Function: begin
template <class K, class V, class Hash, class KeyEqual, class Allocator>
typename Dictionary<K, V, Hash, KeyEqual, Allocator>::iter_ct 
Dictionary<K, V, Hash, KeyEqual, Allocator>::begin() const {
  return base::begin();
}

// Function: end
template <class K, class V, class Hash, class KeyEqual, class Allocator>
typename Dictionary<K, V, Hash, KeyEqual, Allocator>::iter_t 
Dictionary<K, V, Hash, KeyEqual, Allocator>::end() {
  return base::end();
}

// Function: end
template <class K, class V, class Hash, class KeyEqual, class Allocator>
typename Dictionary<K, V, Hash, KeyEqual, Allocator>::iter_ct 
Dictionary<K, V, Hash, KeyEqual, Allocator>::end() const {
  return base::end();
}

// Operator: []
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline const V* Dictionary<K, V, Hash, KeyEqual, Allocator>::operator [] (const K &key) const {
  auto itr = base::find(key);
  return (itr == base::end()) ? nullptr : itr->second;
}

// Operator: []
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline V* Dictionary<K, V, Hash, KeyEqual, Allocator>::operator [] (const K &key) {
  auto itr = base::find(key);
  return (itr == base::end()) ? nullptr : itr->second;
}

// Function: insert
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline V* Dictionary<K, V, Hash, KeyEqual, Allocator>::insert(const K &key) {
  auto itr = base::find(key);
  if(itr == base::end()) {
    return base::operator[](key) = new V(key);
  }
  return itr->second;
}

// Procedure: remove
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline void Dictionary<K, V, Hash, KeyEqual, Allocator>::remove(const K &key) {
  auto itr = base::find(key);
  if(itr != base::end()) {
    delete itr->second;
    base::erase(itr);
  }
}

// Procedure: rehash
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline void Dictionary<K, V, Hash, KeyEqual, Allocator>::rehash(size_t prime) {
  base::rehash(prime);
}

// Function: rehash
// Rehash an item with a new key value. Note that it's user's responsibility to change the
// object's key appropriately.
template <class K, class V, class Hash, class KeyEqual, class Allocator>
inline V* Dictionary<K, V, Hash, KeyEqual, Allocator>::rehash(const K &old_key, const K &new_key) {
  auto itr = base::find(old_key);
  if(itr == base::end()) return insert(new_key);
  remove(new_key);
  V* v = base::operator[](new_key) = itr->second;
  base::erase(itr);
  return v;
}

//-------------------------------------------------------------------------------------------------

// Class: MurMurHash32
// MurmurHash3 was written by Austin Appleby, and is placed in the public domain. 
// The author already disclaims copyright to this source code.
struct MurMurHash32 {
  unsigned long operator() (const string &) const;
  unsigned long operator() (const int) const;
};

//-------------------------------------------------------------------------------------------------

// Class: xxHash32 
//
//   xxHash - Extremely Fast Hash algorithm
//   Header File
//   Copyright (C) 2012-2015, Yann Collet.
//   BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions are
//   met:

//       * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//       * Redistributions in binary form must reproduce the above
//   copyright notice, this list of conditions and the following disclaimer
//   in the documentation and/or other materials provided with the
//   distribution.

//   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   You can contact the author at :
//   - xxHash source repository : http://code.google.com/p/xxhash/
//   
//   The class wrapper is made by Tsung-Wei Huang.
struct xxHash32 {
  unsigned int operator() (const string &) const;
};

//-------------------------------------------------------------------------------------------------

// Class: xxHash64
//
//   xxHash - Extremely Fast Hash algorithm
//   Header File
//   Copyright (C) 2012-2015, Yann Collet.
//   BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions are
//   met:

//       * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//       * Redistributions in binary form must reproduce the above
//   copyright notice, this list of conditions and the following disclaimer
//   in the documentation and/or other materials provided with the
//   distribution.

//   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   You can contact the author at :
//   - xxHash source repository : http://code.google.com/p/xxhash/
//   
//   The class wrapper is made by Tsung-Wei Huang.
struct xxHash64 {
  size_t operator() (const string &) const;
};

};  // End of namespace OpenTimer. ----------------------------------------------------------------

#endif

