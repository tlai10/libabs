/******************************************************************************
 *                                                                            *
 * Copyright (c) 2015, Tsung-Wei Huang and Martin D. F. Wong,                 *
 * University of Illinois at Urbana-Champaign (UIUC), IL, USA.                *
 *                                                                            *
 * All Rights Reserved.                                                       *
 *                                                                            *
 * This program is free software. You can redistribute and/or modify          *
 * it in accordance with the terms of the accompanying license agreement.     *
 * See LICENSE in the top-level directory for details.                        *
 *                                                                            *
 ******************************************************************************/

#include "ot_dictionary.h"

namespace OpenTimer {

//-------------------------------------------------------------------------------------------------

// Operator: MurMurHash32
// MurmurHash3 was written by Austin Appleby, and is placed in the public domain. 
// The author already disclaims copyright to this source code.
unsigned long MurMurHash32::operator() (const string & key) const {
  
  const uint8_t * data = (const uint8_t*)(key.c_str());
  const int len = key.size();
  const int nblocks = len >> 2;

  uint32_t h1 = OT_HASH_SEED;

  const uint32_t c1 = 0xcc9e2d51;
  const uint32_t c2 = 0x1b873593;

  //----------
  // body

  const uint32_t * blocks = (const uint32_t *)(data + nblocks*4);

  for(int i = -nblocks; i; i++)
  {
    uint32_t k1 = blocks[i];

    k1 *= c1;
    k1 = ((k1 << 15) | (k1 >> 17));
    k1 *= c2;
    
    h1 ^= k1;
    h1 = ((h1 << 13) | (h1 >> 19));
    h1 = h1*5+0xe6546b64;
  }

  //----------
  // tail

  const uint8_t * tail = (const uint8_t*)(data + nblocks*4);

  uint32_t k1 = 0;

  switch(len & 3)
  {
    case 3: k1 ^= tail[2] << 16;
    case 2: k1 ^= tail[1] << 8;
    case 1: k1 ^= tail[0];
            k1 *= c1; k1 = ((k1 << 15) | (k1 >> 17)); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len;
  h1 ^= h1 >> 16;
  h1 *= 0x85ebca6b;
  h1 ^= h1 >> 13;
  h1 *= 0xc2b2ae35;
  h1 ^= h1 >> 16;

  return h1;
}				

// Operator: MurMurHash32
// MurmurHash3 was written by Austin Appleby, and is placed in the public domain. 
// The author already disclaims copyright to this source code.
unsigned long MurMurHash32::operator() (const int key) const {
  
  const uint8_t * data = (const uint8_t*)(&key);
  const int len = sizeof(key);
  const int nblocks = len >> 2;

  uint32_t h1 = 17;

  const uint32_t c1 = 0xcc9e2d51;
  const uint32_t c2 = 0x1b873593;

  //----------
  // body

  const uint32_t * blocks = (const uint32_t *)(data + nblocks*4);

  for(int i = -nblocks; i; i++)
  {
    uint32_t k1 = blocks[i];

    k1 *= c1;
    k1 = ((k1 << 15) | (k1 >> 17));
    k1 *= c2;
    
    h1 ^= k1;
    h1 = ((h1 << 13) | (h1 >> 19));
    h1 = h1*5+0xe6546b64;
  }

  //----------
  // tail

  const uint8_t * tail = (const uint8_t*)(data + nblocks*4);

  uint32_t k1 = 0;

  switch(len & 3)
  {
    case 3: k1 ^= tail[2] << 16;
    case 2: k1 ^= tail[1] << 8;
    case 1: k1 ^= tail[0];
            k1 *= c1; k1 = ((k1 << 15) | (k1 >> 17)); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len;
  h1 ^= h1 >> 16;
  h1 *= 0x85ebca6b;
  h1 ^= h1 >> 13;
  h1 *= 0xc2b2ae35;
  h1 ^= h1 >> 16;

  return h1;
}				

//-------------------------------------------------------------------------------------------------

// The following coding block defines a set of funtions that are used by xxHash class. These coding
// blocks are extracted from the source code provided by the authors of xxHash. See license in the
// corresponding .h file.

// XXH_USE_UNALIGNED_ACCESS:
// Unaligned memory access is automatically enabled for "common" CPU, such as x86.
// For others CPU, the compiler will be more cautious, and insert extra code to ensure aligned 
// access is respected. If you know your target CPU supports unaligned memory access, you want to 
// force this option manually to improve performance. You can also enable this parameter if you 
// know your input data will always be aligned (boundaries of 4, for uint32_t).
#if defined(__ARM_FEATURE_UNALIGNED) || \
    defined(__i386) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
  #define XXH_USE_UNALIGNED_ACCESS 1
#endif

// XXH_ACCEPT_nullptr_INPUT_POINTER:
// If the input pointer is a null pointer, xxHash default behavior is to trigger a memory access 
// error, since it is a bad pointer. When this option is enabled, xxHash output for null input 
// pointers will be the same as a null-length input.
// This option has a very small performance cost (only measurable on small inputs).
// By default, this option is disabled. To enable it, uncomment below define :
// #define XXH_ACCEPT_nullptr_INPUT_POINTER 1

// XXH_FORCE_NATIVE_FORMAT:
// By default, xxHash library provides endian-independant Hash values, based on little-endian 
// convention. Results are therefore identical for little-endian and big-endian CPU. This comes 
// at a performance cost for big-endian CPU, since some swapping is required to emulate 
// little-endian format. Should endian-independance be of no importance for your application, 
// you may set the #define below to 1. It will improve speed for Big-endian CPU.
// This option has no impact on Little_Endian CPU.
#define XXH_FORCE_NATIVE_FORMAT 0

#ifndef XXH_CPU_LITTLE_ENDIAN
  static const int xxh_one = 1;
  #define XXH_CPU_LITTLE_ENDIAN   (*(char*)(&xxh_one))
#endif

#define XXH_rotl32(x,r) ((x << r) | (x >> (32 - r)))
#define XXH_rotl64(x,r) ((x << r) | (x >> (64 - r)))
#define XXH_get32bits(p) XXH_readLE32_align(p, endian, align)
#define XXH_get64bits(p) XXH_readLE64_align(p, endian, align)

typedef enum { XXH_bigEndian=0, XXH_littleEndian=1 } XXH_endianess;
typedef enum { XXH_aligned, XXH_unaligned } XXH_alignment;

typedef struct _uint32_packed_t
{
    uint32_t v;
}uint32_packed_t;

typedef struct _uint64_packed_t
{
    uint64_t v;
}uint64_packed_t;

// Function: XXH_swap32
static inline uint32_t XXH_swap32 (uint32_t x) {
  return  ((x << 24) & 0xff000000 ) |
          ((x <<  8) & 0x00ff0000 ) |
          ((x >>  8) & 0x0000ff00 ) |
          ((x >> 24) & 0x000000ff );
}

// Function: XXH_swap64
static inline uint64_t XXH_swap64 (uint64_t x) {
  return  ((x << 56) & 0xff00000000000000ULL) |
          ((x << 40) & 0x00ff000000000000ULL) |
          ((x << 24) & 0x0000ff0000000000ULL) |
          ((x << 8)  & 0x000000ff00000000ULL) |
          ((x >> 8)  & 0x00000000ff000000ULL) |
          ((x >> 24) & 0x0000000000ff0000ULL) |
          ((x >> 40) & 0x000000000000ff00ULL) |
          ((x >> 56) & 0x00000000000000ffULL);
}

// Function: XXH_readLE32_align
static inline uint32_t XXH_readLE32_align(const void* ptr, XXH_endianess endian, XXH_alignment align) {
  if (align == XXH_unaligned) {
    return endian==XXH_littleEndian ? ((uint32_packed_t *)(ptr))->v : XXH_swap32(((uint32_packed_t *)(ptr))->v);
  }
  return endian==XXH_littleEndian ? *(uint32_t*)ptr : XXH_swap32(*(uint32_t*)ptr);
}

// Function: XXH_readLE32
static inline uint32_t XXH_readLE32(const void* ptr, XXH_endianess endian) {
  return XXH_readLE32_align(ptr, endian, XXH_unaligned);
}

// Function: XXH_readLE64_align
static inline uint64_t XXH_readLE64_align(const void* ptr, XXH_endianess endian, XXH_alignment align) {
  if (align==XXH_unaligned) {
    return endian==XXH_littleEndian ? ((uint64_packed_t *)(ptr))->v : XXH_swap64(((uint64_packed_t *)(ptr))->v);
  }
  return endian==XXH_littleEndian ? *(uint64_t*)ptr : XXH_swap64(*(uint64_t*)ptr);
}

// Function: XXH_readLE64
static inline uint64_t XXH_readLE64(const void* ptr, XXH_endianess endian) {
  return XXH_readLE64_align(ptr, endian, XXH_unaligned);
}

// Function: XXH32_endian_align
static inline uint32_t XXH32_endian_align(const void* input, size_t len, uint32_t seed, 
                                          XXH_endianess endian, XXH_alignment align)
{
  const uint8_t* p = (const uint8_t*)input;
  const uint8_t* bEnd = p + len;
  uint32_t h32;

  #ifdef XXH_ACCEPT_nullptr_INPUT_POINTER
  if(p==nullptr) {
    len=0;
    bEnd=p=(const uint8_t*)(size_t)16;
  }
  #endif

  if (len>=16) {
    const uint8_t* const limit = bEnd - 16;
    uint32_t v1 = seed + OT_PRIME32_1 + OT_PRIME32_2;
    uint32_t v2 = seed + OT_PRIME32_2;
    uint32_t v3 = seed + 0;
    uint32_t v4 = seed - OT_PRIME32_1;

    do {
      v1 += XXH_get32bits(p) * OT_PRIME32_2;
      v1 = XXH_rotl32(v1, 13);
      v1 *= OT_PRIME32_1;
      p+=4;
      v2 += XXH_get32bits(p) * OT_PRIME32_2;
      v2 = XXH_rotl32(v2, 13);
      v2 *= OT_PRIME32_1;
      p+=4;
      v3 += XXH_get32bits(p) * OT_PRIME32_2;
      v3 = XXH_rotl32(v3, 13);
      v3 *= OT_PRIME32_1;
      p+=4;
      v4 += XXH_get32bits(p) * OT_PRIME32_2;
      v4 = XXH_rotl32(v4, 13);
      v4 *= OT_PRIME32_1;
      p+=4;
    }
    while (p<=limit);

    h32 = XXH_rotl32(v1, 1) + XXH_rotl32(v2, 7) + XXH_rotl32(v3, 12) + XXH_rotl32(v4, 18);
  }
  else {
      h32  = seed + OT_PRIME32_5;
  }

  h32 += (uint32_t) len;

  while (p+4<=bEnd) {
    h32 += XXH_get32bits(p) * OT_PRIME32_3;
    h32  = XXH_rotl32(h32, 17) * OT_PRIME32_4 ;
    p+=4;
  }

  while (p<bEnd) {
    h32 += (*p) * OT_PRIME32_5;
    h32 = XXH_rotl32(h32, 11) * OT_PRIME32_1 ;
    p++;
  }

  h32 ^= h32 >> 15;
  h32 *= OT_PRIME32_2;
  h32 ^= h32 >> 13;
  h32 *= OT_PRIME32_3;
  h32 ^= h32 >> 16;

  return h32;
}

// Function: XXH64_endian_align
static inline uint64_t XXH64_endian_align(const void* input, size_t len, uint64_t seed, 
                                          XXH_endianess endian, XXH_alignment align) {

  const uint8_t* p = (const uint8_t*)input;
  const uint8_t* bEnd = p + len;
  uint64_t h64;


  #ifdef XXH_ACCEPT_nullptr_INPUT_POINTER
  if(p==nullptr) {
    len=0;
    bEnd=p=(const uint8_t*)(size_t)32;
  }
  #endif

  if(len>=32) {
    const uint8_t* const limit = bEnd - 32;
    uint64_t v1 = seed + OT_PRIME64_1 + OT_PRIME64_2;
    uint64_t v2 = seed + OT_PRIME64_2;
    uint64_t v3 = seed + 0;
    uint64_t v4 = seed - OT_PRIME64_1;

    do {
      v1 += XXH_get64bits(p) * OT_PRIME64_2;
      p+=8;
      v1 = XXH_rotl64(v1, 31);
      v1 *= OT_PRIME64_1;
      v2 += XXH_get64bits(p) * OT_PRIME64_2;
      p+=8;
      v2 = XXH_rotl64(v2, 31);
      v2 *= OT_PRIME64_1;
      v3 += XXH_get64bits(p) * OT_PRIME64_2;
      p+=8;
      v3 = XXH_rotl64(v3, 31);
      v3 *= OT_PRIME64_1;
      v4 += XXH_get64bits(p) * OT_PRIME64_2;
      p+=8;
      v4 = XXH_rotl64(v4, 31);
      v4 *= OT_PRIME64_1;
    }
    while (p<=limit);

    h64 = XXH_rotl64(v1, 1) + XXH_rotl64(v2, 7) + XXH_rotl64(v3, 12) + XXH_rotl64(v4, 18);

    v1 *= OT_PRIME64_2;
    v1 = XXH_rotl64(v1, 31);
    v1 *= OT_PRIME64_1;
    h64 ^= v1;
    h64 = h64 * OT_PRIME64_1 + OT_PRIME64_4;

    v2 *= OT_PRIME64_2;
    v2 = XXH_rotl64(v2, 31);
    v2 *= OT_PRIME64_1;
    h64 ^= v2;
    h64 = h64 * OT_PRIME64_1 + OT_PRIME64_4;

    v3 *= OT_PRIME64_2;
    v3 = XXH_rotl64(v3, 31);
    v3 *= OT_PRIME64_1;
    h64 ^= v3;
    h64 = h64 * OT_PRIME64_1 + OT_PRIME64_4;

    v4 *= OT_PRIME64_2;
    v4 = XXH_rotl64(v4, 31);
    v4 *= OT_PRIME64_1;
    h64 ^= v4;
    h64 = h64 * OT_PRIME64_1 + OT_PRIME64_4;
  }
  else {
    h64  = seed + OT_PRIME64_5;
  }

  h64 += (uint64_t) len;

  while (p+8<=bEnd) {
    uint64_t k1 = XXH_get64bits(p);
    k1 *= OT_PRIME64_2;
    k1 = XXH_rotl64(k1,31);
    k1 *= OT_PRIME64_1;
    h64 ^= k1;
    h64 = XXH_rotl64(h64,27) * OT_PRIME64_1 + OT_PRIME64_4;
    p+=8;
  }

  if (p+4<=bEnd)
  {
    h64 ^= (uint64_t)(XXH_get32bits(p)) * OT_PRIME64_1;
    h64 = XXH_rotl64(h64, 23) * OT_PRIME64_2 + OT_PRIME64_3;
    p+=4;
  }

  while (p<bEnd)
  {
    h64 ^= (*p) * OT_PRIME64_5;
    h64 = XXH_rotl64(h64, 11) * OT_PRIME64_1;
    p++;
  }

  h64 ^= h64 >> 33;
  h64 *= OT_PRIME64_2;
  h64 ^= h64 >> 29;
  h64 *= OT_PRIME64_3;
  h64 ^= h64 >> 32;

  return h64;
}

// Operator: xxHash32
// The main function that defines the hash function, taking the input key and return a 32-bit
// unsigned integer using the xxHash algorithm.
unsigned int xxHash32::operator() (const string & key) const {
  
  const void* input = key.c_str();
  size_t len = key.size();
    
  XXH_endianess endian_detected = (XXH_endianess) XXH_CPU_LITTLE_ENDIAN;
  
  // Input is aligned, leverage the speed advantage.
#if !defined(XXH_USE_UNALIGNED_ACCESS)
  if ((((size_t)input) & 3) == 0) {  
    if ((endian_detected==XXH_littleEndian) || XXH_FORCE_NATIVE_FORMAT) {
      return XXH32_endian_align(input, len, OT_HASH_SEED, XXH_littleEndian, XXH_aligned);
    }
    else {
      return XXH32_endian_align(input, len, OT_HASH_SEED, XXH_bigEndian, XXH_aligned);
    }
  }
#endif

  if ((endian_detected==XXH_littleEndian) || XXH_FORCE_NATIVE_FORMAT) {
    return XXH32_endian_align(input, len, OT_HASH_SEED, XXH_littleEndian, XXH_unaligned);
  }
  else {
    return XXH32_endian_align(input, len, OT_HASH_SEED, XXH_bigEndian, XXH_unaligned);
  }
}

//-------------------------------------------------------------------------------------------------

// Operator: xxHash64
// The main function that produces the hashed integer for a given string key.
size_t xxHash64::operator() (const string &key) const {

  const void* input = key.c_str();
  size_t len = key.size();

  XXH_endianess endian_detected = (XXH_endianess)XXH_CPU_LITTLE_ENDIAN;

  #if !defined(XXH_USE_UNALIGNED_ACCESS)
  if ((((size_t)input) & 7)==0) {
    if ((endian_detected==XXH_littleEndian) || XXH_FORCE_NATIVE_FORMAT)
      return XXH64_endian_align(input, len, OT_HASH_SEED, XXH_littleEndian, XXH_aligned);
    else
      return XXH64_endian_align(input, len, OT_HASH_SEED, XXH_bigEndian, XXH_aligned);
  }
  #endif

  if ((endian_detected==XXH_littleEndian) || XXH_FORCE_NATIVE_FORMAT) {
    return XXH64_endian_align(input, len, OT_HASH_SEED, XXH_littleEndian, XXH_unaligned);
  }
  else {
    return XXH64_endian_align(input, len, OT_HASH_SEED, XXH_bigEndian, XXH_unaligned);
  }
}


};
