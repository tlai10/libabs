module top (
inp1,
inp2,
iccad_clk,
out
);

// Start PIs
input inp1;
input inp2;
input iccad_clk;

// Start POs
output out;

simple u1 ( .inp1(inp1), .inp2(inp2), .iccad_clk(iccad_clk), .out(out) );

endmodule
