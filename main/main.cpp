#include "ot_abstractor.h"
// Procedure: tau2016
void tau2016(int argc, char **argv) {
  
  // Parse the tau2016 commands.
  OpenTimer::Log::init_logging(argv[0], 1);

  OpenTimer::timer_t timer;
  timer.set_num_threads(16);
  timer.exec_tau2016(argc, argv);
  OpenTimer::string_t early_lib_fpath = timer.environment_ptr()->tau2016_macro_name() + "_top_Early.lib";
  OpenTimer::string_t late_lib_fpath = timer.environment_ptr()->tau2016_macro_name() + "_top_Late.lib";

  // Perform the abstraction.
  timer.update_timing();
  OpenTimer::abstractor_t abstractor;
  abstractor.set_timer_ptr(&timer);
  abstractor.set_macro_name(timer.environment_ptr()->tau2016_macro_name());
  abstractor.abstraction();
  // Destruct the object and write results.
  abstractor.write_celllib(early_lib_fpath, late_lib_fpath);
}

//-------------------------------------------------------------------------------------------------

// Function: main
int main(int argc, char **argv) {

  tau2016(argc, argv);

  return 0;
}

