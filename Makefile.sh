# Predefined variables.
echo "# Predefine variables."
echo "OT_HOME=3rd-party/OpenTimer-1.0.4"
echo "OT_LIB=\$(OT_HOME)/lib/libOpenTimer.la"
echo "GLOG_HOME=\$(OT_HOME)/3rd-party/glog-0.3.3"
echo "GLOG_INC=\$(GLOG_HOME)/src"
echo "GLOG_LIB=\$(GLOG_HOME)/libglog.la"
echo ""

# Automake options.
echo "# Automake options."
echo "AUTOMAKE_OPTIONS = foreign"
echo ""

# Make sure that when we re-make ./configure, we get the macros we need.
echo "# Make sure that when we re-make ./configure, we get the macros we need."
echo "ACLOCAL_AMFLAGS = -I m4 --install"
echo ""

# Automake directories.
echo "# Automake directories."
echo "SUBDIRS = \$(OT_HOME) ."
echo ""

# Package-related substitution variables.
echo "# Package-related substitution variables."
echo "CXX = @CXX@"
echo "CXXFLAGS = @CXXFLAGS@"
echo "LIBS = @LIBS@"
echo "DEFS = @DEFS@"
echo ""

# Initialize variables here so we can use += operator everywhere else.
echo "# Initialize variables here so we can use += operator everywhere else."
echo "lib_LTLIBRARIES ="
echo "noinst_LTLIBRARIES ="
echo "sbin_PROGRAMS ="
echo "bin_PROGRAMS ="
echo "noinst_PROGRAMS ="
echo "pkglibexec_PROGRAMS ="
echo "pkginclude_HEADERS ="
echo "nobase_pkginclude_HEADERS ="
echo "dist_bin_SCRIPTS ="
echo "dist_pkglibexec_SCRIPTS ="
echo "nobase_dist_pkgdata_DATA ="
echo "nodist_sbin_SCRIPTS ="
echo "check_PROGRAMS ="
echo "dist_check_SCRIPTS ="
echo "check_SCRIPTS ="
echo "BUILT_SOURCES ="
echo "CLEANFILES ="
echo "EXTRA_DIST ="
echo "PHONY_TARGETS ="
echo "LDADD ="
echo ""

# libabs CXX regular flag
echo "# libabs CXX flag"
echo "LIBABS_CXXFLAGS = \$(CXXFLAGS)"
echo "LIBABS_CXXFLAGS += \$(OPENMP_CXXFLAGS)"
echo "LIBABS_CXXFLAGS += -Wall"
echo "LIBABS_CXXFLAGS += -std=c++11 -static -static-libgcc"
echo ""

# libabs include flag
echo "# libabs include flag"
echo "LIBABS_CXXFLAGS += -Isrc"
echo ""

# libabs 3rd-party OpenTimer include flag
echo "# libabs 3rd-party OpenTimer include flag"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/debug"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/dump"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/generic"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/iterator"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/liberty"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/log"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/sdc"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/shell"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/spef"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/timer"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/src/verilog"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/3rd-party/LEF"
echo "LIBABS_CXXFLAGS += -I\$(OT_HOME)/3rd-party/DEF"
echo "LIBABS_CXXFLAGS += -I\$(GLOG_INC)"
echo ""

# Program: main/abstractor
echo "# Program: main/abstractor"
echo "noinst_PROGRAMS += main/abstractor"
echo "main_abstractor_CXXFLAGS = \$(LIBABS_CXXFLAGS)"
echo "main_abstractor_LDADD = liblibabs.la"
echo "main_abstractor_LDADD += \$(OT_LIB)"
echo "main_abstractor_SOURCES = main/main.cpp"
echo ""

# Library definition: liblibabs.la
echo "# Library definition: liblibabs.la"
echo "lib_LTLIBRARIES += liblibabs.la"
echo "liblibabs_la_CXXFLAGS = \$(LIBABS_CXXFLAGS)"
echo "liblibabs_la_LIBADD = "
echo "liblibabs_la_LIBADD += \$(OT_LIB)"
echo "liblibabs_la_SOURCES = "
for f in `find src -name *.cpp -o -name *.cc`
do
  echo "liblibabs_la_SOURCES += $f"
done
echo ""

# libabs package include.
echo "# libabs Package include"
for f in `find src -name *.h`
do
  echo "pkginclude_HEADERS += $f"
done
echo ""

# Extra distribution.
echo "# Add files to the distribution list"
echo "EXTRA_DIST += Makefile.sh"
echo "EXTRA_DIST += golden"
echo ""

# Regression target.
echo "# Regression TAU 2016"
echo "regression: all-am"
echo "	@cd golden/tau2016/ && ./regression.sh"
echo "abs: all-am"
echo "  @cd golden/tau2016/ && ./abs.sh"


